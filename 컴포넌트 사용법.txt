1. Dissolve [ 디폴트값 전부 등록 완료 => 필요에 따라 수정 ]
- 컴포넌트 추가
- DoDissolve 함수 사용시 디졸브 실행
- ResetDissolve 함수로 초기화 가능
- DissolveCurve :  사용할 커브 곡선 등록 [ 디폴트로 만든 FloatCurve 지정했지만 변경 가능 ]
- DissolveColor : 사라질때 보이는 빛 색깔
- DissolveTextrue : 어떻게 사라질지 Texture로 설정 [ DissolveTexture0 , 1 , 2 참고 ]
- DissovleSize : 사라지는 크기 [ 크면 빛이 많이 보이지만 빨리 없어짐 ] , 사그라드는 정도
- DissovlePower : 빛나는 정도


2. Hover
- StaticMesh한테 컴포넌트 추가 
- 컨스트레인트로 Lock할 축 설정 
- TraceLength : 값이 적으면 바닥과 붙고, 많으면 바닥으로부터 멀리 떨어짐
- HoverForce : 뜨는 힘, 많이 줄이면 바닥에서 조금만 뜸
- LinearDamping : 이동방해값, 너무 작으면 HoverForce에 의해 점점 위로 올라가고,
                       너무 크면 점점 움직임이 줄어들다 정지한것처럼 보이는 상태가 됨
- AngularDamping : 회전 방해값 , LinearDamping과 기능은 비슷 [ 이동대신 회전 ]



3. Flicker
- 컴포넌트 추가
- FlickerMaterial에 Flicker Material 등록 [ 디폴트로 등록은 해놨음 ]
- FlickerValue : 깜빡이는 정도
- FlickerName : 만약 FlickerMaterial을 디폴트된것으로 쓰지 않을 경우,
                    사용할 Material에서 깜빡이는 정도를 나타내는 파라미터이름을 적어주면됨


4. EmissiveComponent [ 많이 테스트는 안해봤음 ]
- 컴포넌트 추가 [ Dissolve, Filcker랑은 같이쓰면 안될것 같음 ]
- EmissiveFrequencyMin/Max : 랜덤으로 Min~Max에서 값을 정해 그 주기만큼 깜빡임 [ 초단위 ]
- EmissiveOriginPower : 기본 Emissive 밝기값
- EmissiveCurve :  사용할 커브 곡선 등록 [ 디폴트로 만든 FloatCurve 지정했지만 변경 가능 ]
- IsEmissiveFlicking: 체크하면 깜빡이고, 체크안하면 안깜빡임 [ 실행 여부 ]
- EmissiveParameterName : 해당 Mesh 메테리얼의 Emissive 값을 담당하는 파라미터 이름
   => 이 값으로 Emissive 값을 조정함 [ 위의 FlickerName와 같은 기능 ]


