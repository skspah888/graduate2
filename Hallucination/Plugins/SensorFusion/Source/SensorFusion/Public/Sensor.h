// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Windows/AllowWindowsPlatformTypes.h"
#include "Windows.h"
#include "Windows/HideWindowsPlatformTypes.h"

struct FSensorInfoStruct
{	
	//센서에서 받는 정보들
	FString SensorID;

	FVector Euler;
	FVector Gyro;
	FVector Acceleration;

	int32	Battery{ -1 };
};

/**
 * 
 */
class SENSORFUSION_API Sensor
{
public:
	Sensor();
	~Sensor();

private:
	HANDLE  m_hComm;
	DCB     m_dcb;
	COMMTIMEOUTS m_CommTimeouts;
	bool    m_bPortReady;
	DWORD   m_iBytesWritten;
	DWORD   m_iBytesRead;

public:
	FSensorInfoStruct Data;

	bool OpenPort(const FString& PortName);
	void ClosePort();
	bool IsPortOpened();

	bool WriteByte(int8 bybyte);
	bool ReadByteStr(FString& buffer);

	bool SetCommunicationTimeouts(DWORD ReadIntervalTimeout,
		DWORD ReadTotalTimeoutMultiplier, DWORD ReadTotalTimeoutConstant,
		DWORD WriteTotalTimeoutMultiplier, DWORD WriteTotalTimeoutConstant);
	bool ConfigurePort(DWORD BaudRate, BYTE ByteSize, DWORD fParity,
		BYTE  Parity, BYTE StopBits);

	void Parsing(FString& buffer);

};