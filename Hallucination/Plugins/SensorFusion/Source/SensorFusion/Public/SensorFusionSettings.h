// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SensorFusionSettings.generated.h"

/**
 * 
 */


//UCLASS(config = Game, defaultconfig)
UCLASS(config = HallucinationSetting)
class SENSORFUSION_API USensorFusionSettings : public UObject
{
	GENERATED_BODY()
	
public:

	USensorFusionSettings(const FObjectInitializer& ObjectInitializer);
	
	UPROPERTY(EditAnywhere, config, Category = Custom)
	TArray<FString> Sensors;

	UPROPERTY(EditAnywhere, config)
	int32 PortNumber;
};
