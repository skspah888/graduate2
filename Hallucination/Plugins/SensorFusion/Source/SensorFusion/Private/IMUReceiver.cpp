// Fill out your copyright notice in the Description page of Project Settings.


#include "IMUReceiver.h"
#include "SensorFusionBPLibrary.h"
#include "Engine.h"
#include "Array.h"

// Sets default values for this component's properties
UIMUReceiver::UIMUReceiver()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	Rotation = FRotator::ZeroRotator;
	Acceleration = FVector::ZeroVector;
	Gyro = FVector::ZeroVector;
	Battery = -1;
}


// Called when the game starts
void UIMUReceiver::BeginPlay()
{
	Super::BeginPlay();
	TryToConnectToSensor();

	if (bEnableCSVRecording)
	{
		RecordingTimer = 0.f;
		RecordingTotalTime = 0.f;
		FString Path = FPaths::ProjectSavedDir() + RecorderFilename + FString::FromInt(IMU_Id) + TEXT(".csv");
		FileWriter = IFileManager::Get().CreateFileWriter(*Path);
	}

	if (bEnableCSVReader)
	{
		ReadingTotalTime = 0.f;
		FString Path = FPaths::ProjectSavedDir() + ReaderFilename + FString::FromInt(IMU_Id) + TEXT(".csv");
		FileReader = IFileManager::Get().CreateFileReader(*Path);
		LastReadTime = 0.f;
		if(FileReader)
			ReadFileSize = FileReader->TotalSize();
		ReadBytes = 0;
	}

	// ...

}

void UIMUReceiver::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	UActorComponent::EndPlay(EndPlayReason);

	if (USensorFusionBPLibrary::IsConnected())
		USensorFusionBPLibrary::Disconnect();

	if (FileWriter)
	{
		FileWriter->Close();
		delete FileWriter;
		FileWriter = nullptr;
	}

	if (FileReader)
	{
		FileReader->Close();
		delete FileReader;
		FileReader = nullptr;
	}
}

FString UIMUReceiver::RotatorToCSVString(const FRotator& rotation) const
{
	static int32 MinFracDigits = 1;
	return FString::SanitizeFloat(rotation.Roll, MinFracDigits) + TEXT(",")
		+ FString::SanitizeFloat(rotation.Pitch, MinFracDigits) + TEXT(",")
		+ FString::SanitizeFloat(rotation.Yaw, MinFracDigits);
}

FString UIMUReceiver::VectorToCSVString(const FVector& vector) const
{
	static int32 MinFracDigits = 1;
	return FString::SanitizeFloat(vector.X, MinFracDigits) + TEXT(",")
		+ FString::SanitizeFloat(vector.Y, MinFracDigits) + TEXT(",")
		+ FString::SanitizeFloat(vector.Z, MinFracDigits);
}

void UIMUReceiver::WriteDataToCSV(FArchive& Ar, float TimeStamp, const FRotator& rotation, const FVector& acceleration, const FVector& gyro, int32 battery)
{
	FString Data =
		FString::SanitizeFloat(TimeStamp) + TEXT(",")
		+ RotatorToCSVString(rotation) + TEXT(",")
		+ VectorToCSVString(acceleration) + TEXT(",")
		+ VectorToCSVString(gyro) + TEXT(",")
		+ FString::FromInt(battery) + TEXT("\n");

	auto Src = StringCast<ANSICHAR>(*Data, Data.Len());
	Ar.Serialize((ANSICHAR*)Src.Get(), Src.Length() * sizeof(ANSICHAR));
}

bool UIMUReceiver::ReadDataFromCSV(FArchive& Ar, float* outTime, FRotator* outRotation, FVector* outAccel, FVector* outGyro, int32* outBattery)
{
	FString Line;
	ANSICHAR TempChar = '0';
	while (TempChar != '\n' && TempChar != '\0')
	{
		Ar.Serialize(&TempChar, sizeof(ANSICHAR));
		Line += TempChar;

		ReadBytes += sizeof(ANSICHAR);
		if (ReadBytes >= ReadFileSize)
			return false;
	}
	

	TArray<FString> dataArr = {};
	Line.ParseIntoArray(dataArr, TEXT(","), false);

	// TArray<FString>::SizeType index = 0;
	int32 Index = 0;

	*outTime				= FCString::Atof(*dataArr[Index++]);

	(*outRotation).Roll		= FCString::Atof(*dataArr[Index++]);
	(*outRotation).Pitch	= FCString::Atof(*dataArr[Index++]);
	(*outRotation).Yaw		= FCString::Atof(*dataArr[Index++]);

	(*outAccel).X			= FCString::Atof(*dataArr[Index++]);
	(*outAccel).Y			= FCString::Atof(*dataArr[Index++]);
	(*outAccel).Z			= FCString::Atof(*dataArr[Index++]);

	(*outGyro).X			= FCString::Atof(*dataArr[Index++]);
	(*outGyro).Y			= FCString::Atof(*dataArr[Index++]);
	(*outGyro).Z			= FCString::Atof(*dataArr[Index++]);

	*outBattery				= FCString::Atoi(*dataArr[Index++]);

	return true;
}

// Called every frame
void UIMUReceiver::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/*FRotator Rotation(0.f);
	FVector Acceleration(0.f);
	FVector Gyro(0.f);
	int Battery = -1;*/

	bool bReadyToBroadcast = false;

	if (bEnableCSVReader && FileReader)
	{
		ReadingTotalTime += DeltaTime;
		if (ReadingTotalTime > LastReadTime)
		{
			while (ReadingTotalTime > LastReadTime)
			{
				if (ReadDataFromCSV(*FileReader, &LastReadTime, &Rotation, &Acceleration, &Gyro, &Battery))
					bReadyToBroadcast = true;
				else
					break;
			}
		}

	}
	else
	{
		if (USensorFusionBPLibrary::IsConnected())
		{
			USensorFusionBPLibrary::GetSensorRotation(IMU_Id, Rotation, bUseRawData);
			USensorFusionBPLibrary::GetSensorAcceleration(IMU_Id, Acceleration, bUseRawData);
			USensorFusionBPLibrary::GetSensorGyro(IMU_Id, Gyro, bUseRawData);
			USensorFusionBPLibrary::GetSensorBattery(IMU_Id, Battery, bUseRawData);

			bReadyToBroadcast = true;

			if (!USensorFusionBPLibrary::IsRotationBinded(IMU_Id))
			{
				USensorFusionBPLibrary::BindRotation(IMU_Id);
//				UE_LOG(LogTemp, Warning, TEXT("Bind Error!"));
			}

			if (bEnableCSVRecording && FileWriter)
			{
				RecordingTimer += DeltaTime;
				RecordingTotalTime += DeltaTime;

				while (RecordingTimer > RecordingRate)
				{
					RecordingTimer -= RecordingRate;
					WriteDataToCSV(*FileWriter, RecordingTotalTime, Rotation, Acceleration, Gyro, Battery);
				}

			}
		}
		else
			TryToConnectToSensor();
	}
	
	if (bReadyToBroadcast)
	{
		RotationReceived.Broadcast(Rotation);
		AccelerationReceived.Broadcast(Acceleration);
		GyroReceived.Broadcast(Gyro);
		BatteryReceived.Broadcast(Battery);

		int MessageId = IMU_Id * 100;
#define DebugId(Id) MessageId + Id

		switch (DebugLevel)
		{
		case 1:
		{
			GEngine->AddOnScreenDebugMessage(DebugId(0), 1.f, FColor::Orange, FString::Printf(TEXT("Battery		[%d] >>> %d"), IMU_Id, Battery));
			GEngine->AddOnScreenDebugMessage(DebugId(1), 1.f, FColor::Yellow, FString::Printf(TEXT("Rotation		[%d] >>> %s"), IMU_Id, *Rotation.ToString()));
			GEngine->AddOnScreenDebugMessage(DebugId(2), 1.f, FColor::Green, FString::Printf(TEXT("Gyro			[%d] >>> %s"), IMU_Id, *Gyro.ToString()));
			GEngine->AddOnScreenDebugMessage(DebugId(3), 1.f, FColor::Cyan, FString::Printf(TEXT("Acceleration	[%d] >>> X = %f"), IMU_Id, Acceleration.X));
			GEngine->AddOnScreenDebugMessage(DebugId(4), 1.f, FColor::Cyan, FString::Printf(TEXT("Acceleration	[%d] >>> Y = %f"), IMU_Id, Acceleration.Y));
			GEngine->AddOnScreenDebugMessage(DebugId(5), 1.f, FColor::Cyan, FString::Printf(TEXT("Acceleration	[%d] >>> Z = %f"), IMU_Id, Acceleration.Z));
			break;
		}
		case 2:
		{
			float tolerance = 1.25f;
			if (FMath::Abs(Acceleration.X) > tolerance)
				GEngine->AddOnScreenDebugMessage(DebugId(0), 1.f, FColor::Red, (Acceleration.X < 0.f) ? TEXT("Forward") : TEXT("Backward"));
			if (FMath::Abs(Acceleration.Y) > tolerance)
				//Accelerometer RHS 쓰기 때문에 Y축만 반대로
				GEngine->AddOnScreenDebugMessage(DebugId(1), 1.f, FColor::Green, (Acceleration.Y < 0.f) ? TEXT("Left") : TEXT("Right"));
			if (FMath::Abs(Acceleration.Z) > tolerance)
				GEngine->AddOnScreenDebugMessage(DebugId(2), 1.f, FColor::Blue, (Acceleration.Z < 0.f) ? TEXT("Up") : TEXT("Down"));
			break;
		}
		}
	}
	
	// ...
}

void UIMUReceiver::TryToConnectToSensor()
{
	if (!USensorFusionBPLibrary::IsConnected())
	{
		if (!USensorFusionBPLibrary::Connect())
			UE_LOG(LogTemp, Warning, TEXT("Connect Error!"));
	}
	//if (!USensorFusionBPLibrary::BindRotation(IMU_Id))
		//UE_LOG(LogTemp, Warning, TEXT("Bind Rotation!"))

	USensorFusionBPLibrary::BindRotation(IMU_Id);
}


