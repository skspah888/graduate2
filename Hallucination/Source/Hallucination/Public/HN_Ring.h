// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "HN_Item.h"
#include "HN_Ring.generated.h"

UCLASS()
class HALLUCINATION_API AHN_Ring : public AHN_Item
{
	GENERATED_BODY()
	
public:
	UPROPERTY(VisibleAnywhere, Category = Ring)
	UAudioComponent* Speaker;

	UPROPERTY(VisibleAnywhere, Category = Ring)
	class USoundBase*		RingSound;

public:
	// Sets default values for this actor's properties
	AHN_Ring();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void GetItem_Implementation(AHN_Player* _player) override;
};
