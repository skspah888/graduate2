// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Engine/TriggerVolume.h"
#include "HN_3F_TriggerVolume.generated.h"
/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_3F_TriggerVolume : public ATriggerVolume
{
	GENERATED_BODY()
	
private:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Room, Meta = (AllowPrivateAccess = true))
	class ATargetPoint*		TargetPoint;
	
protected:
	virtual void BeginPlay() override;

public:
	AHN_3F_TriggerVolume();

	UFUNCTION(Server, Reliable, WithValidation)
	void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	UFUNCTION()
	void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);
	
};
