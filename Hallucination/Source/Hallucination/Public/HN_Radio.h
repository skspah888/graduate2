// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "HN_Object.h"
#include "GameFramework/Actor.h"
#include "HN_Radio.generated.h"

UCLASS()
class HALLUCINATION_API AHN_Radio : public AHN_Object
{
	GENERATED_BODY()
	
public:
	UPROPERTY(VisibleAnywhere, Category = Radio)
	UTexture2D* EmissiveTex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrentTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrentDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Switching;

public:	
	// Sets default values for this actor's properties
	AHN_Radio();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	virtual void ActiveOn_Implementation() override;
	virtual void ActiveOff_Implementation() override;
};