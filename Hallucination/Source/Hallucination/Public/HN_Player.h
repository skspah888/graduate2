// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Character.h"
#include "GenericTeamAgentInterface.h"
#include "HN_Player.generated.h"

class AHN_Item;

UENUM()
enum class HAND_TYPE : uint8 { RIGHT, LEFT, END };

UENUM()
enum class PLAYER_STATE : uint8 { CLAM, SCARY, FEAR, TERRIBLE, BLOODY, PANIC, END };
//	각 50~40, 40~30, 30~20, 20~10 ,10~0, 0

DECLARE_DELEGATE_OneParam(FDele_Single_OneParam, FVector);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDele_Single);

UCLASS()
class HALLUCINATION_API AHN_Player : public ACharacter
{
	GENERATED_BODY()

	public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
	UCameraComponent* VRCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
	UCameraComponent* PCCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	class USceneComponent* VROrigin;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	class USceneComponent* PCOrigin;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	UAudioComponent*		Speaker;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	UAudioComponent*		EffectSpeaker;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	UAudioComponent*		DamageSpeaker;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio)
	class USoundCue*		PlayerWalkSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio)
	class USoundCue*		FearSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio)
	class USoundCue*		ScarySoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio)
	class USoundCue*		TerribleSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio)
	class USoundCue*		DamageSoundCue;


	UPROPERTY(EditAnywhere)
	TSubclassOf<UCameraShake> MyShake;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	bool		IsVRMode = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player")
	bool		IsInteractObject = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player")
	bool		IsPlayerController = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	float		InvincibilityTime;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player")
	bool		IsInvincibility;

	float		InvincibilityCurrentTime;

	AHN_Item* Inventory[(int)HAND_TYPE::END];

	FDele_Single_OneParam Fuc_DeleSingle_OneParam;

	UPROPERTY(BlueprintAssignable, Category = "Player")
	FDele_Single Fuc_DeleSingle;

protected:
	/*UPROPERTY()
	class AHN_PlayerController* HNPlayerController;*/

	FGenericTeamId TeamId;
	float fRotate;

	//	공포 저항 수치
	int iMaxHp;
	int iHp;
	bool	bChange;		//	false면 상태변화 중, true면 상태변화가 끝남 -> 상태가 변하면 false로 바꿔줘야함
	PLAYER_STATE eState;
	float fHealCoolTime;

	//	플레이어 HP 이펙트 변수
	float fGamma;

	//	플레이어 HP 이펙트 Post Process Material
	UMaterial* pBlurMtrl;

	//	나이트 비전
	bool					IsNightVision;
	FPostProcessSettings*	pNightVision;
	FPostProcessSettings*	pNormalVision;

public:
	AHN_Player();

protected:
	UFUNCTION(BlueprintCallable)
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	UFUNCTION(BlueprintCallable)
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void	SetEnablePlayerController(bool bCheck);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void	SetPlayerInteract(bool bCheck);

	FGenericTeamId	GetGenericTeamId() const;
	void			InitPlayer();

public:
	UFUNCTION(BlueprintCallable)
	void Scamp_Shake();
	UFUNCTION(BlueprintCallable)
	void Scamp_ShakeEnd();
	UFUNCTION(BlueprintCallable)
	void PlayWalkSound(float fValue);



public:
	UFUNCTION(BlueprintCallable, Category = "PlayerInput")
	void		UpDown(float NewAxisValue);
	UFUNCTION(BlueprintCallable, Category = "PlayerInput")
	void		LeftRight(float NewAxisValue);
	UFUNCTION(BlueprintCallable, Category = "PlayerInput")
	void		LookUp(float NewAxisValue);
	UFUNCTION(BlueprintCallable, Category = "PlayerInput")
	void		Turn(float NewAxisValue);

public:			// Delegate Function
	UFUNCTION()
	void DeleFunc_SetPosition(FVector originPos);

public:
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void		RotationController(const FRotator& Rotator);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void		RotationCharacter(const FRotator& Rotator);

	UFUNCTION(Server, Reliable, WithValidation)
	void SetActorRotationServer(FRotator bRotation);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetActorRotationMulticast(FRotator bRotation);
	UFUNCTION(BlueprintCallable)
	void		Interact();

	UFUNCTION(BlueprintCallable)
	void		SetCameraActivate(bool bCheck);

	void SetItem(HAND_TYPE _hand, AHN_Item* _item);
	bool UseItem(FName _name);
	bool CheckItem(FName _name);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void Damage(int _damage);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void Heal(int _heal);

	void HpEffect();
	void BloodyEffect();
	void RemoveBloodyEffect();

	void PlayScarySound();
	void PlayFearSound();
	void PlayTerribleSound();

	void PlayDamageSound();

	UFUNCTION(BlueprintCallable)
	void ChangeNightVision();

	UFUNCTION(BlueprintCallable)
	void CallDeadDelegateFunc();	
};
