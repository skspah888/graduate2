// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Components/SceneComponent.h"
#include "HN_FlickerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HALLUCINATION_API UHN_FlickerComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHN_FlickerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	void SetFlickerMaterial();
	void SetFlickerValue();

public:
	TArray<UMaterialInstanceDynamic*>	FlickerMaterialInstances;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Flicker")
	UMaterialInterface*				FlickerMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Flicker")
	float							FlickerValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Flicker")
	FName							FlickerName;							// Default : "Flicker Value"
};
