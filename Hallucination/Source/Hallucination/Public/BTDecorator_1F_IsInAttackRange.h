// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_1F_IsInAttackRange.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTDecorator_1F_IsInAttackRange : public UBTDecorator
{
	GENERATED_BODY()
	
public:
	UBTDecorator_1F_IsInAttackRange();

	// 원하는 조건이 달성됐는지 파악, const함수여서 데코레이터 클래스의 멤버 변수값은 변경할 수 없음
protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
