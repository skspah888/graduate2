// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Actor.h"
#include "HN_Object.generated.h"

UCLASS()
class HALLUCINATION_API AHN_Object : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly)
	bool bActive;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;

public:	
	// Sets default values for this actor's properties
	AHN_Object();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ActiveOn();
	virtual void ActiveOn_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ActiveOff();
	virtual void ActiveOff_Implementation();

	UFUNCTION(BlueprintCallable)
	void SetRenderDepth(bool bValue);
};
