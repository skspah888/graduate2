// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Animation/AnimInstance.h"
#include "HN_AnimInst_Flower.generated.h"

UENUM(BlueprintType)
enum class EFlowerState : uint8
{
	FRONT			UMETA(DisplayName = "Front"),
	BLEND			UMETA(DisplayName = "Blend"),
	BACK			UMETA(DisplayName = "Back"),
	STATE_END		UMETA(DisplayName = "State_End")
};

UCLASS()
class HALLUCINATION_API UHN_AnimInst_Flower : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UHN_AnimInst_Flower();

public:
	UFUNCTION(BlueprintCallable)
	void		SetState(EFlowerState NextState);
	uint32		GetState();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FlowerGhost, Meta = (AllowPrivateAccess = true))
	EFlowerState eState;
	
};
