// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Actor.h"
#include "HN_4F_Room.generated.h"

UCLASS()
class HALLUCINATION_API AHN_4F_Room : public AActor
{
	GENERATED_BODY()
	
private:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Room, Meta = (AllowPrivateAccess = true))
	TArray<class ATargetPoint*>		TargetArr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Room, Meta = (AllowPrivateAccess = true))
	class AHN_DragGhostDoor*		Door;

public:
	//TSubclassOf<class AHN_Ghost_Player> PlayerBlueprintType;
	class AHN_Ghost_Player*				GhostPlayer;

private:
	int32							CurrentIndex;
	int32							MaxIndex;
	bool							IsPlayer;
	
public:	
	AHN_4F_Room();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

public:
	bool	Get_TargetLocation(FVector& TargetLocation);
	bool	Get_TargetRotator(FRotator& TargetRotator);
	bool	Set_ReTargetLocation();

public:
	void	SetPlayer(bool bCheck);
	void	Activate_Door(bool IsOpen);
	void	SetActorIgnore(class AHN_Player* targetPlayer, bool isIgnore);
	void	SetGhost(class AHN_Ghost_Drag* _Ghost);
	void	SetTargetDoor(bool bCheck);
	void	SpawnGhost();
	void	PenaltyGhostPlayer();

};
