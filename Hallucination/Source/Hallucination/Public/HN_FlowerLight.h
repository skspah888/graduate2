// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Actor.h"
#include "HN_FlowerLight.generated.h"

UCLASS()
class HALLUCINATION_API AHN_FlowerLight : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	UPointLightComponent*		PointLight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	FLinearColor				DefaultLightColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	FLinearColor				ChangeLightColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	float LightIntensity;

public:	
	AHN_FlowerLight();

protected:
	virtual void BeginPlay() override;
	//virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintCallable, Category = "Custom Light")
	void	SetCustomLightColor(bool IsChange);

	UFUNCTION(BlueprintCallable, Category = "Custom Light")
	void	SetCustomLightIntensity(float _intensity);

	void	SetCustomLightVisibility();

};
