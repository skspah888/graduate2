// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Animation/AnimInstance.h"
#include "HN_AnimInst_Patient.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UHN_AnimInst_Patient : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UHN_AnimInst_Patient();
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	void SetIsRing();

private:
	UPROPERTY(BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsRing;
};
