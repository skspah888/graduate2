// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "Interactable.h"
#include "HN_Ghost_Patient.generated.h"

UENUM()
enum class PATIENT_STATE : uint8 { CRY, RING, CLEAR, STATE_END };

UCLASS()
class HALLUCINATION_API AHN_Ghost_Patient : public AHN_Ghost, public IInteractable
{
	GENERATED_BODY()
	
public:
	AHN_Ghost_Patient();
	
protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void PostInitializeComponents() override;

public:
	UFUNCTION(BlueprintCallable)
	void Acquired_Ring();
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void ChangeAnim_Patient();// 박병주 추가, 환자 애니메이션 변경
	bool IsRing();	
	void SetRing(bool bCheck);

public:
	UFUNCTION(BlueprintCallable, Category = "Patient")
	virtual bool CanInteract_Implementation() override;
	UFUNCTION(BlueprintCallable, Category = "Patient")
	virtual void PerformInteract_Implementation() override;
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Patient")
	void PlayPatientSound(bool isPlay);
	UFUNCTION(BlueprintCallable, Category = "Patient")
	void SetPatientSound(class USoundCue* _SoundCue, bool isPlay = true);


public:
	UFUNCTION(NetMulticast, Reliable)
	void SetState(PATIENT_STATE eState);
	void DestoryPatient();
	void ClearPatient();
	

private:
	FTimerHandle		TimerHandle;
	PATIENT_STATE		CurrentState;
	PATIENT_STATE		NextState;

private:
	UPROPERTY()
	class UHN_AnimInst_Patient* AnimInst;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	class USoundCue*		CrySoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	class USoundCue*		RingSoundCue;

};
