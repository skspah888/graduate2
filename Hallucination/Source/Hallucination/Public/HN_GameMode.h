// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/GameModeBase.h"
#include "HN_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_GameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AHN_GameMode();

	virtual void PostInitializeComponents() override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	

protected:
	virtual void BeginPlay() override;
	
};
