// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Actor.h"
#include "HN_Gate.generated.h"

UCLASS()
class HALLUCINATION_API AHN_Gate : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(VisibleAnywhere, Category = Gate)
	UStaticMeshComponent* Door;

	UPROPERTY(VisibleAnywhere, Category = Gate)
	UAudioComponent* Speaker;

	bool	bOpen;

	float	fValue;

public:
	// Sets default values for this actor's properties
	AHN_Gate();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame

	virtual void Tick(float DeltaTime) override;

	void Open();
	void Stop();
};
