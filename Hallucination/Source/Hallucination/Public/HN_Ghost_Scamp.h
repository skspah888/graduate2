// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "HN_Ghost_Scamp.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_Ghost_Scamp : public AHN_Ghost
{
	GENERATED_BODY()
	
private:
	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = State, Meta = (AllowPrivateAccess = true))
	bool bIsComplete;	//	이 귀신을 해결했나 true:해결 false:미해결

	UPROPERTY()
	class AHN_AI_Scamp* AIController;

	UPROPERTY(VisibleAnywhere, Category = Trigger)
	UBoxComponent* StartTrigger;			//	플레이어 수 감지하는 트리거

	int32 iPlayerCount;		//	플레이어가 몇 명 트리거와 닿았는지 세는 변수

	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = Door, Meta = (AllowPrivateAccess = true))
	class AHN_Door* RoomDoor;

	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = Ghost, Meta = (AllowPrivateAccess = true))
	class AHN_Ghost_Patient* Patient;

	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = Ghost, Meta = (AllowPrivateAccess = true))
	float fOpacity;

	UPROPERTY(VisibleAnywhere, Category = Sound)
	class USoundBase*		DieSound;

	UPROPERTY(VisibleAnywhere, Category = Sound)
	class USoundBase*		StartSound;

	UPROPERTY(VisibleAnywhere, Category = Sound)
	class USoundBase*		ShakeSound;

	UPROPERTY(VisibleAnywhere, Category = Sound)
	class USoundBase*		NotShakeSound;

	UPROPERTY(VisibleAnywhere, Category = Sound)
	class USoundBase*		BGMSound;
	
	UPROPERTY(VisibleAnywhere, Category = Sound)
	class USoundBase*		BGM4FSound;

	bool	bCheat;

public:
	AHN_Ghost_Scamp();

private:
	UFUNCTION(BlueprintCallable)
	void OnCharacterOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable)
	void OnCharacterOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable)
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void PostInitializeComponents() override;

	void DisableActor();
	UFUNCTION(BlueprintCallable)
	void ActiveActor();

	void SetComplete(bool _complete) { bIsComplete = _complete; }

	//UFUNCTION(BlueprintCallable)
	//void StartAI();
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void ChangeAnim_ScampDoorClose();
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void ChangeAnim_ScampDoorOpen();

	//	소리 부분
	UFUNCTION(NetMulticast, Reliable)
	void PlayDieSound();

	UFUNCTION(NetMulticast, Reliable)
	void PlayStartSound();

	UFUNCTION(NetMulticast, Reliable)
	void PlayShakeSound();

	UFUNCTION(NetMulticast, Reliable)
	void PlayNotShakeSound();

	//	타이머를 사용한 Destroy 
	UFUNCTION()
	void DestroyActorFunction();
};
