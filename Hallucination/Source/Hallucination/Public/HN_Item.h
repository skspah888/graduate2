// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Actor.h"
#include "HN_Item.generated.h"

class AHN_Player;

UENUM()
enum class ITEM_TYPE : uint8 { RING, KEY, END };

UCLASS()
class HALLUCINATION_API AHN_Item : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly)
	bool bActive;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;

	ITEM_TYPE eType;
	
public:	
	AHN_Item();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void GetItem(AHN_Player* _player);
	virtual void GetItem_Implementation(AHN_Player* _player);

	UFUNCTION(BlueprintCallable)
	void SetRenderDepth(bool bValue);
};
