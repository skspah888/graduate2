// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "HN_Item.h"
#include "HN_Key.generated.h"

/**
 *
 */
UCLASS()
class HALLUCINATION_API AHN_Key : public AHN_Item
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, Category = Key)
	UAudioComponent* Speaker;

	UPROPERTY(VisibleAnywhere, Category = Key)
	class USoundBase*		KeySound;

public:
	// Sets default values for this actor's properties
	AHN_Key();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void GetItem_Implementation(AHN_Player* _player) override;
};
