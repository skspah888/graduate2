// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Components/SceneComponent.h"
#include "HN_EmissiveComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HALLUCINATION_API UHN_EmissiveComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHN_EmissiveComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable, Category = CustomEmissive)
	void GetStaticMeshes();

	UFUNCTION(BlueprintCallable, Category = CustomEmissive)
	void GetSkeletalMeshes();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void SetEmissivePower();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void UpdateEmissivePower(float _EmissivePower);


public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = CustomEmissive, Meta = (AllowPrivateAccess = true))
	TArray<class UMaterialInstanceDynamic*>		EmissiveMaterials;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = CustomEmissive, Meta = (AllowPrivateAccess = true))
	float										EmissiveFrequency;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = CustomEmissive, Meta = (AllowPrivateAccess = true))
	float										EmissiveOriginPower;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = CustomEmissive, Meta = (AllowPrivateAccess = true))
	FName										EmissiveParameterName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = CustomEmissive, Meta = (AllowPrivateAccess = true))
	bool										IsEmissiveFlicking;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = CustomEmissive, Meta = (AllowPrivateAccess = true))
	class UCurveFloat*							EmissiveCurve;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = CustomEmissive, Meta = (AllowPrivateAccess = true))
	float										EmissiveFrequencyMin;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = CustomEmissive, Meta = (AllowPrivateAccess = true))
	float										EmissiveFrequencyMax;

	float										AccumulatedTime;
	float										EmissiveCurrentPower;
};
