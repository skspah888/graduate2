// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "HN_Door.generated.h"

UCLASS()
class HALLUCINATION_API AHN_Door : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:

	UPROPERTY(VisibleAnywhere, Category = Door)
	UBoxComponent* BoxCollision;

	UPROPERTY(VisibleAnywhere, Category = Door)
	UBoxComponent* Trigger;

	UPROPERTY(VisibleAnywhere, Category = Door)
	UStaticMeshComponent* Frame;

	UPROPERTY(VisibleAnywhere, Category = Door)
	UStaticMeshComponent* Door;

	UPROPERTY(VisibleAnywhere, Category = Door)
	USceneComponent* Scene;

	UPROPERTY(VisibleAnywhere, Category = Door)
	UAudioComponent* Speaker;

	UPROPERTY(VisibleAnywhere, Category = Door)
	class USoundBase*		OpenSound;

	UPROPERTY(VisibleAnywhere, Category = Door)
	class USoundBase*		CloseSound;


	DOOR_STATE eState;

	UPROPERTY(EditAnywhere, Category = Door)
	bool	bOpen;		//	현재 열려있는지 닫혀있는지
	UPROPERTY(EditAnywhere, Category = Door)
	bool	bLock;		//	잠겨있는지 아닌지

	float	CurRotAngle;	//	현재 문 각도
	float	MaxRotAngle;	//	최대 문 각도

public:	
	// Sets default values for this actor's properties
	AHN_Door();

protected:
	void Idle();
	void Open();
	void Close();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

public:	
	// Called every frame
	
	virtual void Tick(float DeltaTime) override;
	virtual void ChangeState(DOOR_STATE _eNewState);
	UFUNCTION(BlueprintCallable)
	void SetLock(bool _Lock) { bLock = _Lock; }
	UFUNCTION(BlueprintCallable)
	void SetRenderDepth(bool bValue);
	
	DOOR_STATE GetState() { return eState; }

	bool IsOpen() { return bOpen; }

public:
	UFUNCTION(BlueprintCallable)
	virtual bool CanInteract_Implementation() override;
	UFUNCTION(BlueprintCallable)
	virtual void PerformInteract_Implementation() override;
};
