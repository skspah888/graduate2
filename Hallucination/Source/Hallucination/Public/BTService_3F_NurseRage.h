// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTService.h"
#include "BTService_3F_NurseRage.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTService_3F_NurseRage : public UBTService
{
	GENERATED_BODY()
	
public:
	UBTService_3F_NurseRage();

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
