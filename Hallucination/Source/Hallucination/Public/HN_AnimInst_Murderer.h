// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Animation/AnimInstance.h"
#include "HN_AnimInst_Murderer.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UHN_AnimInst_Murderer : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UHN_AnimInst_Murderer();
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	
public:
	void			PlayAttackMontage();

	UFUNCTION()
	void			AnimNotify_AttackHitCheck();

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ghost, Meta = (AllowPrivateAccess = true))
	float				CurrentPawnSpeed;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Ghost, Meta = (AllowPrivateAccess = true))
	UAnimMontage*		AttackMontage;
};
