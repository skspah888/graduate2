// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTService.h"
#include "BTService_1F_IsAttack.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTService_1F_IsAttack : public UBTService
{
	GENERATED_BODY()
	
public:
	UBTService_1F_IsAttack();

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
