// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_1F_MurdererTurnToTarget.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_1F_MurdererTurnToTarget : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_1F_MurdererTurnToTarget();
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
