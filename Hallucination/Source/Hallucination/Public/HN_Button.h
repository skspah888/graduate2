// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "HN_Object.h"
#include "GameFramework/Actor.h"
#include "HN_Button.generated.h"

UCLASS()
class HALLUCINATION_API AHN_Button : public AHN_Object
{
	GENERATED_BODY()
	
public:
	UPROPERTY(VisibleAnywhere, Category = Button)
	UStaticMeshComponent* ButtonMesh;

	UPROPERTY(VisibleAnywhere, Category = Button)
	UAudioComponent* Speaker;

	UPROPERTY(VisibleAnywhere, Category = Button)
	class USoundBase*		ButtonOnSound;

	UPROPERTY(VisibleAnywhere, Category = Button)
	class USoundBase*		ButtonOffSound;

	//UPROPERTY(VisibleAnywhere, Category = Button)
	//class USoundAttenuat*		ATT;

public:	
	// Sets default values for this actor's properties
	AHN_Button();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void ActiveOn_Implementation() override;

	virtual void ActiveOff_Implementation() override;
};
