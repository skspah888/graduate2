#pragma once
#include "Interactable.generated.h"

/**  */
UINTERFACE()
class HALLUCINATION_API UInteractable : public UInterface
{
	GENERATED_BODY()
};

/**  */
class HALLUCINATION_API IInteractable
{
	GENERATED_BODY()

	public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Interactable)
	bool CanInteract();
	virtual bool CanInteract_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Interactable)
	void PerformInteract();
	virtual void PerformInteract_Implementation();

};

