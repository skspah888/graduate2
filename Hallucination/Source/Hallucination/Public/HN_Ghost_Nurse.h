// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "HN_AnimInst_Nurse.h"
#include "HN_Ghost_Nurse.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnAttackEndDelegate);

UCLASS()
class HALLUCINATION_API AHN_Ghost_Nurse : public AHN_Ghost
{
	GENERATED_BODY()
	
public:
	AHN_Ghost_Nurse();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;

public:
	void		SetGoalPoint(bool bCheck);

public:
	float		GetAttackRange();
	bool		GetGoalPoint();
	void		NurseAttack();
	FOnAttackEndDelegate						OnAttackEnd;
	FTimerHandle								TimerHandle;

public:
	UFUNCTION(NetMulticast, Reliable)
	void				StartAttackSound();
	UFUNCTION(NetMulticast, Reliable)
	void				StartDeadSound();
	UFUNCTION(NetMulticast, Reliable)
	void				StartWhisperSound();
	UFUNCTION(NetMulticast, Reliable)
	void				StartLaughSound();
	UFUNCTION(NetMulticast, Reliable)
	void				StartPlayerDeadSound();

public:
	UFUNCTION()
	void						SetNurseState(ENurseState eState);
	UFUNCTION(NetMulticast, Reliable)
	void						NurseAttackAnim();
	UFUNCTION()
	void						OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted);

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
	void		NurseGhostDie();
	UFUNCTION(NetMulticast, Reliable)
	void		NurseGhostDieSound();
	void		DestoryGhost();

	UFUNCTION()
	class AHN_Player* GetTarget();

public:
	bool		IsGoal;
	float		AttackRange;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	bool		IsAttacking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AHN_WayPoint*			NextWayPoint;

	UPROPERTY()
	UHN_AnimInst_Nurse*			NurseAnimInst;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Ghost)
	class UHN_DissolveComponent*		DissolveComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AActor*				RagePoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					AttackSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					DeadSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					WhisperSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					LaughSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					PlayerDeadSoundCue;
};
