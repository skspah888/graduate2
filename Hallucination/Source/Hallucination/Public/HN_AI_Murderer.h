// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_AIController.h"
#include "HN_AI_Murderer.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_AI_Murderer : public AHN_AIController
{
	GENERATED_BODY()
	
public:
	static const FName		TargetKey;
	static const FName		IsAttackKey;
	
public:
	AHN_AI_Murderer();

public:
	virtual void Tick(float DeltaTime) override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

public:
	UFUNCTION()
	class AHN_Player* GetTarget();

public:
	UFUNCTION(BlueprintCallable)
	void ExcuteBehaviorTree();
	
};
