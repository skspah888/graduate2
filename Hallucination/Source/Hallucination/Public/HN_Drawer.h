// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "HN_Object.h"
#include "HN_Drawer.generated.h"

/**
 *
 */
UCLASS()
class HALLUCINATION_API AHN_Drawer : public AHN_Object
{
	GENERATED_BODY()

	public:
	UPROPERTY(VisibleAnywhere, Category = Drawer)
	UStaticMeshComponent* DrawerMesh;

	UPROPERTY(VisibleAnywhere, Category = Drawer)
	UAudioComponent* Speaker;

	UPROPERTY(VisibleAnywhere, Category = Drawer)
	class USoundBase* OpenSound;

	UPROPERTY(VisibleAnywhere, Category = Drawer)
	class USoundBase* CloseSound;

	bool bWait;		//	true일 때 움직이게

	float fYLoc;

public:
	// Sets default values for this actor's properties
	AHN_Drawer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void ActiveOn_Implementation() override;

	virtual void ActiveOff_Implementation() override;
};
