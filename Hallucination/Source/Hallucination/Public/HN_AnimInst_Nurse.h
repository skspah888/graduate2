// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Animation/AnimInstance.h"
#include "HN_AnimInst_Nurse.generated.h"

UENUM(BlueprintType)
enum class ENurseState : uint8
{
	IDLE			UMETA(DisplayName = "Idle"),
	RUN				UMETA(DisplayName = "Run"),
	ATTACK			UMETA(DisplayName = "Attack"),
	STATE_END		UMETA(DisplayName = "State_End")
};

UCLASS()
class HALLUCINATION_API UHN_AnimInst_Nurse : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UHN_AnimInst_Nurse();
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

public:
	UFUNCTION(BlueprintCallable)
	void		SetState(ENurseState NextState);
	uint32		GetState();

public:
	void			PlayAttackMontage();

	UFUNCTION(Server, Reliable, WithValidation)
	void			AnimNotify_AttackHitCheck();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NurseGhost, Meta = (AllowPrivateAccess = true))
	ENurseState			eState;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NurseGhost, Meta = (AllowPrivateAccess = true))
	float				CurrentPawnSpeed;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = NurseGhost, Meta = (AllowPrivateAccess = true))
	UAnimMontage*		AttackMontage;
};
