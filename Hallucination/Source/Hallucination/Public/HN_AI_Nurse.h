// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_AIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "HN_AI_Nurse.generated.h"

UCLASS()
class HALLUCINATION_API AHN_AI_Nurse : public AHN_AIController
{
	GENERATED_BODY()
	
public:
	static const FName		IsRageKey;
	static const FName		IsAttackKey;
	static const FName		TargetKey;
	static const FName		HomePosKey;
	static const FName		PatrolPosKey;
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	float AISightRadius = 500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	float AISightAge = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	float AILoseSightRadius = AISightRadius + 50.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	float AIFieldOfView = 60.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	class UAISenseConfig_Sight* SightConfig;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	//bool IsTargetDetected = false;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	//float DistanceToTarget = 0.f;
	
public:
	AHN_AI_Nurse();

public:
	virtual void Tick(float DeltaTime) override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

public:
	void Initialize_SightSense();
	
	UFUNCTION()
	class AHN_Player* GetTarget();

	UFUNCTION()
	bool	GetRage();

	UFUNCTION()
	void OnTargetUpdate(AActor* Actor, FAIStimulus Stimulus);

	UFUNCTION()
	void OnPerceptionUpdate(const TArray<AActor*>& DetectedPawns);

	UFUNCTION(BlueprintCallable)
	void TestNurseRageMode();

public:
	UFUNCTION()
	void PatrolNurseGhost();

	UFUNCTION()
	void ChaseTarget();

	UFUNCTION()
	FVector GetNextLocation();

	UFUNCTION(BlueprintCallable)
	void ExcuteBehaviorTree();

public:
	bool		IsRage;
};
