// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "HN_Ghost_Drag.generated.h"


UENUM()
enum class ROOM_TYPE : uint8 { ROOMA, ROOMB, STATE_END };
/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_Ghost_Drag : public AHN_Ghost
{
	GENERATED_BODY()

private:
	class AHN_4F_Room*			Room_A;
	class AHN_4F_Room*			Room_B;
	class ATargetPoint*			PlayerLookPoint;
	FTimerHandle				TimerHandle;
	ROOM_TYPE					TargetState;

public:
	AHN_Ghost_Drag();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override; 
	virtual void PossessedBy(AController* NewController) override;

public:
	class AHN_4F_Room*			Get_Room(int32 RoomNumber);
	class ATargetPoint*			Get_LookPoint();
	FVector						Get_TargetLocation();
	FRotator					Get_TargetRotator();
	bool						Set_ReTargetLocation();

public:
	void						Setting_TargetRoom(int32 RandomNumber);
	void						Setting_Door(bool IsOpen);
	void						SetDragBlackBoard(FString BoardName, bool bCheck);
	void						SetActorIgnore(class AHN_Player* targetplayer, bool isIgnore);
	
public:
	void						DestroyDragGhost();
	void						ClearDrag();

public:
	void						SpawnPlayerGhost();			// 플레이어 귀신 생성 [ ArriveRoom에서 실행 ]
	void						PenaltyPlayerGhost();		// 플레이어 귀신 상호작용
};
