// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_AIController.h"
#include "HN_AI_Flower.generated.h"

UCLASS()
class HALLUCINATION_API AHN_AI_Flower : public AHN_AIController
{
	GENERATED_BODY() 
	
public:
	static const FName				IsSoundKey;
	static const FName				DelayTimeKey;
	static const FName				CurrentTimeKey;
	
public:
	AHN_AI_Flower();

public:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

public:
	UFUNCTION(Server, Reliable, WithValidation)
	void SoundCheck(bool bCheck);
	void SetFlowerTime();
};
