// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Door.h"
#include "HN_DragGhostDoor.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_DragGhostDoor : public AHN_Door
{
	GENERATED_BODY()
	
public:
	AHN_DragGhostDoor();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

public:
	void Idle();
	UFUNCTION(NetMulticast, Reliable)
	void Open();
	UFUNCTION(NetMulticast, Reliable)
	void Close();
	void SetGhost(class AHN_Ghost_Drag* _Ghost);

public:
	UFUNCTION(BlueprintCallable, Category = GhostDoor)
	void SetDragBlackBoard(FString BoardName, bool bCheck);

	UFUNCTION(BlueprintCallable, Category = GhostDoor)
	void SetGhostMode(bool bCheck);

	UFUNCTION(BlueprintCallable, Category = GhostDoor)
	void SetTargetDoor(bool bCheck);


public:
	class AHN_Ghost_Drag*			DragGhost;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Door)
	bool				IsDragDoor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Door)
	bool				IsTargetDoor;
};
