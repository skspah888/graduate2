// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "HN_Ghost_Flower.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_Ghost_Flower : public AHN_Ghost
{
	GENERATED_BODY()
	
private:
	/*TArray<class AHN_Player*>			PlayerArray;*/
	TArray<class AHN_Player*>					PlayerArray;
	TArray<FVector>								OriginPositionArray;
	TArray<class AHN_FlowerLight*>				LightArray;
	FTimerHandle								TimerHandle;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	class USoundCue*		FlowerSoundCue;

public:
	AHN_Ghost_Flower();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;

public:
	UFUNCTION(Server, Reliable, WithValidation)
	void		OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:
	void		DestoryGhost();
	void		SetOriginPosition(FVector originPos);

public:
	UFUNCTION(NetMulticast, Reliable)
	void FlowerClearMotion();
	UFUNCTION(Server, Reliable, WithValidation)
	void OnAudioEnd();

	UFUNCTION(Server, Reliable, WithValidation)
	void StartSound();

	//UFUNCTION(NetMulticast, Reliable)
	void CheckPlayer();

	UFUNCTION(NetMulticast, Reliable)
	void ChangeLight(bool IsChange);
	UFUNCTION(NetMulticast, Reliable)
	void PlaySoundFlower();
	UFUNCTION(NetMulticast, Reliable)
	void ChangeBackAnim();
	UFUNCTION(NetMulticast, Reliable)
	void ChangeFrontAnim();



public:
	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
	class UCapsuleComponent* TriggerCapsule;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ghost")
	class UHN_DissolveComponent*		DissolveComponent;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ghost")
	UAudioComponent*					FlowerSpeaker;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					DeadSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					LaughSoundCue;


};
