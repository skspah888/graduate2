// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Camera/CameraShake.h"
#include "HN_CameraShake.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UHN_CameraShake : public UCameraShake
{
	GENERATED_BODY()
	
public:
	UHN_CameraShake();

};
