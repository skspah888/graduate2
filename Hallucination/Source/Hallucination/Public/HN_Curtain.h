// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "HN_Object.h"
#include "HN_Curtain.generated.h"

/**
 *
 */
UCLASS()
class HALLUCINATION_API AHN_Curtain : public AHN_Object
{
	GENERATED_BODY()

	public:
	UPROPERTY(VisibleAnywhere, Category = Curtain)
	UStaticMeshComponent* PoleMesh;

	UPROPERTY(VisibleAnywhere, Category = Curtain)
	UAudioComponent* Speaker;

	UPROPERTY(VisibleAnywhere, Category = Curtain)
	class USoundBase*		CurtainSound;

	bool bChange;
	float fCurtainScale;

public:
	// Sets default values for this actor's properties
	AHN_Curtain();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void ActiveOn_Implementation() override;

	virtual void ActiveOff_Implementation() override;

};
