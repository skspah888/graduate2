// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_AIController.h"
#include "HN_AI_Patient.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_AI_Patient : public AHN_AIController
{
	GENERATED_BODY()
	
public:
	static const FName				IsContactKey;
	static const FName				TargetKey;
	static const FName				IsRingKey;
	static const FName				IsClearKey;
	
public:
	AHN_AI_Patient();

public:
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

public:
	UFUNCTION(BlueprintCallable)
	void Acquired_Ring();

	UFUNCTION(BlueprintCallable)
	bool IsRing();

	UFUNCTION(BlueprintCallable)
	void SetClear(bool IsClear);

	UFUNCTION(BlueprintCallable)
	void ExcuteBehaviorTree();

	UFUNCTION(BlueprintCallable)
	void SetPatientClear();

	UFUNCTION(BlueprintCallable)
	void TestPatient();

};
