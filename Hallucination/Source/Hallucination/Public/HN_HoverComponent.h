// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Components/SceneComponent.h"
#include "HN_HoverComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HALLUCINATION_API UHN_HoverComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHN_HoverComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
public:
	class UPrimitiveComponent*					PrimitiveComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Custom, Meta = (AllowPrivateAccess = true))
	float										TraceLength;		// 트레이스 거리를 줄이면 바닥과 붙는다

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Custom, Meta = (AllowPrivateAccess = true))
	float										HoverForce;		// 뜨는 힘 , 많이 줄이면 바닥에서 조금만 뜸

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Custom, Meta = (AllowPrivateAccess = true))
	float										LinearDamping;		// 이동방해 값, 크면 움직임이 저하

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Custom, Meta = (AllowPrivateAccess = true))
	float										AngularDamping;		// 회전방해 값, 크면 움직임이 저하
};
