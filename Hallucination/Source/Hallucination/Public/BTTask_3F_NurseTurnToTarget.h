// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_3F_NurseTurnToTarget.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_3F_NurseTurnToTarget : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_3F_NurseTurnToTarget();
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
