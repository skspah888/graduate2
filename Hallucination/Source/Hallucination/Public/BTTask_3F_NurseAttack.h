// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_3F_NurseAttack.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_3F_NurseAttack : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTask_3F_NurseAttack();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

private:
	bool			IsAttacking = false;
};
