// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "Components/ActorComponent.h"
#include "HN_DissolveComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HALLUCINATION_API UHN_DissolveComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHN_DissolveComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void GetStaticMeshes();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void GetSkeletalMeshes();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void SetColor();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void SetTexture();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void SetDissolveSize();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void SetDissolvePower();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void ResetDissolve();

	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void DoDissolve();
		

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	TArray<class UMaterialInstanceDynamic*>		DissolveMaterials;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	bool										isDoingDissolve;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	float										CurrentDissolveAmount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	class UCurveFloat*							DissolveCurve;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	float										AccumulatedTime;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	FLinearColor								DissolveColor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	class UTexture2D*							DissolveTexture;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	float										DissolveSize;		// 사그라드는 정도 [ 디폴트 10 ]

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Dissolve, Meta = (AllowPrivateAccess = true))
	float										DissolvePower;		// 빛나는 정도 [ 디폴트 50 ]

	bool										FinishDissolve;
};
