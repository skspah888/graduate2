// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "GameFramework/Actor.h"
#include "HN_SpotLight.generated.h"

UCLASS()
class HALLUCINATION_API AHN_SpotLight : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHN_SpotLight();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintCallable)
	void LightExplosion();

	UFUNCTION(BlueprintCallable)
	void OnParticleEnd(UParticleSystemComponent* PSystem);
	void GetStaticMesh();

public:
	TArray<UMaterialInstanceDynamic*>		MaterialInstances;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	UStaticMeshComponent*			LightMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	UParticleSystemComponent*		LightParticle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	UAudioComponent*				LightSpeaker;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	USpotLightComponent*			SpotLight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	class USoundCue*				ExplosionSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	FName							EmissiveName;							// Default : Emissive

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Light")
	bool bIsBreak;
};
