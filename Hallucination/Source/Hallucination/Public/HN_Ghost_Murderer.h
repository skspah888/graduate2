// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_Ghost.h"
#include "HN_AnimInst_Murderer.h"
#include "HN_Ghost_Murderer.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnAttackEndDelegate);

UCLASS()
class HALLUCINATION_API AHN_Ghost_Murderer : public AHN_Ghost
{
	GENERATED_BODY()
	
public:
	AHN_Ghost_Murderer();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual void PossessedBy(AController* NewController) override;

public:
	void		MurdererAttack();
	float		GetAttackRange();

	UFUNCTION(NetMulticast, Reliable)
	void		MurdererAttackAnim();

	UFUNCTION()
	void		OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted);

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
	void		MurdererGhostDie();
	UFUNCTION(NetMulticast, Reliable)
	void		MurdererGhostDieSound();
	void		DestoryGhost();

	UFUNCTION()
	class AHN_Player* GetTarget();


	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void			StartAppearSound();

	UFUNCTION(NetMulticast, Reliable)
	void			StartAttackSound();

	UFUNCTION(NetMulticast, Reliable)
	void			StartBreathSound();

	UFUNCTION(NetMulticast, Reliable)
	void			StartLaughSound();

	UFUNCTION(NetMulticast, Reliable)
	void			StartDeadSound();

	UFUNCTION()
	void			OnAudioEnd();

public:
	FOnAttackEndDelegate						OnAttackEnd;
	FTimerHandle								TimerHandle;
	float		AttackRange;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	bool		IsAttacking;

public:
	UPROPERTY()
	UHN_AnimInst_Murderer*				MurdererAnimInst;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Ghost)
	class UHN_DissolveComponent*		DissolveComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					AppearSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					AttackSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					BreathSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					DeadSoundCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ghost, meta = (AllowPrivateAccess = "true"))
	class USoundCue*					LaughSoundCue;
};
