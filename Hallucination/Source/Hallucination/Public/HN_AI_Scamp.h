// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HN_AIController.h"
#include "HN_AI_Scamp.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API AHN_AI_Scamp : public AHN_AIController
{
	GENERATED_BODY()
	
public:
	static const FName NextPosKey;
	static const FName IsClearKey;
	
public:
	AHN_AI_Scamp();
	UFUNCTION(BlueprintCallable)
	virtual void RunAI() override;

public:
	void SetClear();
};
