// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hallucination.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_1F_MurdererAttack.generated.h"

/**
 * 
 */
UCLASS()
class HALLUCINATION_API UBTTask_1F_MurdererAttack : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_1F_MurdererAttack();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

private:
	bool			IsAttacking = false;
};
