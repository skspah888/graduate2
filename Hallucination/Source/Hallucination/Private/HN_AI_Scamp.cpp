// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AI_Scamp.h"

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

const FName AHN_AI_Scamp::NextPosKey(TEXT("NextPos"));
const FName AHN_AI_Scamp::IsClearKey(TEXT("IsClear"));

AHN_AI_Scamp::AHN_AI_Scamp()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT("BlackboardData'/Game/AI/4F/BB_4F.BB_4F'"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT("BehaviorTree'/Game/AI/4F/BT_4F_Scamp.BT_4F_Scamp'"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;
	}
}

void AHN_AI_Scamp::RunAI()
{
	if (UseBlackboard(BBAsset, Blackboard))
	{
		if (!RunBehaviorTree(BTAsset))
		{
			//LOG_TEXT(Error, TEXT("Scamp AIController couldn't run behavior tree!"));
		}
	}
}

void AHN_AI_Scamp::SetClear()
{
	GetBlackboardComponent()->SetValueAsBool(AHN_AI_Scamp::IsClearKey, true);
}
