// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Drawer.h"

#define MIN_Y 9.f
#define MAX_Y 39.f

AHN_Drawer::AHN_Drawer()
{
	DrawerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DRAWER"));
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_TABLE(TEXT("StaticMesh'/Game/Asset/Hospital/Meshes/Interior_Elements/Furniture/SM_Table_on_Wheels_01.SM_Table_on_Wheels_01'"));

	if (SM_TABLE.Succeeded())
	{
		Mesh->SetStaticMesh(SM_TABLE.Object);
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_DRAWER(TEXT("StaticMesh'/Game/Asset/Hospital/Meshes/Interior_Elements/Furniture/SM_Drawer_Table_on_Wheels_01.SM_Drawer_Table_on_Wheels_01'"));

	if (SM_DRAWER.Succeeded())
	{
		DrawerMesh->SetStaticMesh(SM_DRAWER.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_OPEN(TEXT("SoundCue'/Game/Asset/Hospital/Audio/Door_Closing_Cue.Door_Closing_Cue'"));

	if (SOUND_OPEN.Succeeded())
	{
		OpenSound = SOUND_OPEN.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_CLOSE(TEXT("SoundCue'/Game/Asset/Hospital/Audio/Opening_the_Sliding_Door_Cue.Opening_the_Sliding_Door_Cue'"));

	if (SOUND_CLOSE.Succeeded())
	{
		CloseSound = SOUND_CLOSE.Object;
	}

	DrawerMesh->SetupAttachment(RootComponent);
	Speaker->SetupAttachment(RootComponent);

	DrawerMesh->SetRelativeLocation(FVector(0.f, 9.f, 0.f));
	Speaker->SetRelativeLocation(FVector(0.f, 0.f, 0.f));

	DrawerMesh->ComponentTags.Add(TEXT("outline"));

	bWait = true;

	fYLoc = MIN_Y;
}

void AHN_Drawer::BeginPlay()
{
	Super::BeginPlay();
}

void AHN_Drawer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!bWait)
	{
		if (bActive)
		{
			//	Open
			if (fYLoc >= MAX_Y)
				bWait = true;

			fYLoc += 0.5f;
			DrawerMesh->SetRelativeLocation(FVector(0.f, fYLoc, 0.f));

		}
		else
		{
			//	Close
			if (fYLoc <= MIN_Y)
				bWait = true;

			fYLoc -= 0.5f;
			DrawerMesh->SetRelativeLocation(FVector(0.f, fYLoc, 0.f));
		}
	}
}

void AHN_Drawer::ActiveOn_Implementation()
{
	bActive = true;

	if (bWait)
	{
		Speaker->Stop();
		Speaker->SetSound(OpenSound);
		Speaker->Play();

		bWait = false;
	}
}

void AHN_Drawer::ActiveOff_Implementation()
{
	bActive = false;

	if (bWait)
	{
		Speaker->Stop();
		Speaker->SetSound(CloseSound);
		Speaker->Play();

		bWait = false;
	}
}
