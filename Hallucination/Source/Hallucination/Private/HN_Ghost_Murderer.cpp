// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Murderer.h"
#include "Sound/SoundCue.h"
#include "HN_DissolveComponent.h"
#include "HN_AI_Murderer.h"

AHN_Ghost_Murderer::AHN_Ghost_Murderer()
{
	PrimaryActorTick.bCanEverTick = false;

	// AI 설정
	AIControllerClass = AHN_AI_Murderer::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	GetCapsuleComponent()->InitCapsuleSize(90.f, 150.f);

	// 스켈레탈 메시 : 테스트용
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_NURSE(TEXT(
		"SkeletalMesh'/Game/Characters/Murderer/murderer_mesh.murderer_mesh'"));
	if (SK_NURSE.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_NURSE.Object);
	}

	// 애니메이션 설정
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> ANIM(TEXT(
		"AnimBlueprint'/Game/Characters/Murderer/AnimBP_Murderer.AnimBP_Murderer_C'"));
	if (ANIM.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(ANIM.Class);
	}

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 360.f, 0.f);
	GetCharacterMovement()->MaxWalkSpeed = 200.f;
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, 10.f), FRotator(0.f, -90.f, 0.f));

	// 캐릭터 조정
	GetMesh()->SetRelativeScale3D(FVector(2.f, 2.f, 2.f));

	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("FlowerAudioComponent"));
	Speaker->SetupAttachment(RootComponent);

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundCue> AppearObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Murderer/MurdererAppear_Cue.MurdererAppear_Cue'"));
	if (AppearObject.Succeeded())
	{
		AppearSoundCue = AppearObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Murderer/MurdererAttack_Cue.MurdererAttack_Cue'"));
	if (AttackObject.Succeeded())
	{
		AttackSoundCue = AttackObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> BreathObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Murderer/MurdererBreath_Cue.MurdererBreath_Cue'"));
	if (BreathObject.Succeeded())
	{
		BreathSoundCue = BreathObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> DeadObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Murderer/MurdererDead_Cue.MurdererDead_Cue'"));
	if (DeadObject.Succeeded())
	{
		DeadSoundCue = DeadObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> LaughObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Murderer/MurdererLaugh_Cue.MurdererLaugh_Cue'"));
	if (LaughObject.Succeeded())
	{
		LaughSoundCue = LaughObject.Object;
	}

	// 디졸브
	DissolveComponent = CreateDefaultSubobject<UHN_DissolveComponent>(TEXT("DissolveComponent"));
	DissolveComponent->DissolveColor = FLinearColor::Green;

	AttackRange = 300.f;
	IsAttacking = false;
}

void AHN_Ghost_Murderer::BeginPlay()
{
	Super::BeginPlay();

	MurdererAnimInst = Cast<UHN_AnimInst_Murderer>(GetMesh()->GetAnimInstance());
	MurdererAnimInst->OnMontageEnded.AddDynamic(this, &AHN_Ghost_Murderer::OnAttackMontageEnded);
	AttackRange = 300.f;
}

void AHN_Ghost_Murderer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ghost_Murderer::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);
}

void AHN_Ghost_Murderer::MurdererAttack()
{
	if (IsAttacking || !MurdererAnimInst)
		return;

	StartAttackSound();
	IsAttacking = true;
	MurdererAttackAnim();
}

float AHN_Ghost_Murderer::GetAttackRange()
{
	return AttackRange;
}

void AHN_Ghost_Murderer::MurdererAttackAnim_Implementation()
{
	MurdererAnimInst->PlayAttackMontage();
}

void AHN_Ghost_Murderer::OnAttackMontageEnded(UAnimMontage * Montage, bool bInterrupted)
{
	IsAttacking = false;
	OnAttackEnd.Broadcast();
	StartLaughSound();
}

void AHN_Ghost_Murderer::MurdererGhostDieSound_Implementation() {
	StartDeadSound();
	DissolveComponent->DoDissolve();
}

bool AHN_Ghost_Murderer::MurdererGhostDie_Validate()
{
	return true;
}


void AHN_Ghost_Murderer::MurdererGhostDie_Implementation()
{
	// Ghost가 죽을때 불리는 함수
	//Speaker->SetSound(GhostSoundCue);
	//Speaker->Play();
	//DissolveComponent->DoDissolve();
	MurdererGhostDieSound();
	Cast<AHN_AI_Murderer>(GetController())->UnPossess();
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AHN_Ghost_Murderer::DestoryGhost, 8.f, false);
}

void AHN_Ghost_Murderer::DestoryGhost()
{
	Destroy();
}

AHN_Player * AHN_Ghost_Murderer::GetTarget()
{
	if (HasAuthority() == true)
		return Cast<AHN_AI_Murderer>(GetController())->GetTarget();
	else
		return NULL;
}

void AHN_Ghost_Murderer::StartAppearSound_Implementation()
{
	if (Speaker)
	{
		Speaker->OnAudioFinished.Clear();
		Speaker->SetSound(AppearSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Murderer::StartAttackSound_Implementation()
{
	if (Speaker)
	{
		Speaker->OnAudioFinished.Clear();
		Speaker->SetSound(AttackSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Murderer::StartBreathSound_Implementation()
{
	if (Speaker)
	{
		if (Speaker->IsPlaying())
			return;
		Speaker->OnAudioFinished.AddDynamic(this, &AHN_Ghost_Murderer::OnAudioEnd);
		Speaker->SetSound(BreathSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Murderer::StartLaughSound_Implementation()
{
	if (Speaker)
	{
		Speaker->OnAudioFinished.Clear();
		Speaker->SetSound(LaughSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Murderer::StartDeadSound_Implementation()
{
	if (Speaker)
	{
		Speaker->OnAudioFinished.Clear();
		Speaker->SetSound(DeadSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Murderer::OnAudioEnd()
{
	if (Speaker)
	{
		Speaker->OnAudioFinished.Clear();
		StartBreathSound();
	}
}
