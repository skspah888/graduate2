// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_3F_NurseAttack.h"
#include "HN_Ghost_Nurse.h"
#include "HN_AI_Nurse.h"
#include "HN_AnimInst_Nurse.h"

UBTTask_3F_NurseAttack::UBTTask_3F_NurseAttack()
{
	// Tick 기능 활성화
	bNotifyTick = true;
	IsAttacking = false;
}

EBTNodeResult::Type UBTTask_3F_NurseAttack::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto ControllingPawn = Cast<AHN_Ghost_Nurse>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;

	// 공격 명령 실행
	ControllingPawn->NurseAttack();
	IsAttacking = true;

	// 캐릭터 공격모션이 끝나면 호출될 함수 등록
	ControllingPawn->OnAttackEnd.AddLambda([this]()->void
	{
		IsAttacking = false;
	});
	return EBTNodeResult::InProgress;
}

void UBTTask_3F_NurseAttack::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);
	if (!IsAttacking)
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
