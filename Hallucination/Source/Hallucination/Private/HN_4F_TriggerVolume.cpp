// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_4F_TriggerVolume.h"
#include "DrawDebugHelpers.h"
#include "HN_Player.h"
#include "HN_Ghost_Drag.h"

AHN_4F_TriggerVolume::AHN_4F_TriggerVolume()
{
	OnActorBeginOverlap.AddDynamic(this, &AHN_4F_TriggerVolume::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AHN_4F_TriggerVolume::OnOverlapEnd);
	PlayerCount = 0;
}

void AHN_4F_TriggerVolume::BeginPlay()
{
	DrawDebugBox(GetWorld(), GetActorLocation(), GetActorScale() * 100, FColor::Cyan, true, -1, 0, 5);
}

void AHN_4F_TriggerVolume::OnOverlapBegin(AActor * OverlappedActor, AActor * OtherActor)
{
	if (OtherActor && (OtherActor != this)) {

		if (Cast<AHN_Player>(OtherActor))
		{
			++PlayerCount;
			if (PlayerCount == 2)
			{
				// Drag Ghost Spawn
				FTransform SpawnLocation;
				SpawnLocation.SetLocation(OtherActor->GetActorLocation());
				GetWorld()->SpawnActor<AHN_Ghost_Drag>(AHN_Ghost_Drag::StaticClass(), SpawnLocation);

				// Volume Destroy
				Destroy();
			}
		}
	}
}

void AHN_4F_TriggerVolume::OnOverlapEnd(AActor * OverlappedActor, AActor * OtherActor)
{
	if (OtherActor && (OtherActor != this)) {
		if (Cast<AHN_Player>(OtherActor))
		{
			--PlayerCount;
		}
	}
}
