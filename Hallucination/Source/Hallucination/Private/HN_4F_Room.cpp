// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_4F_Room.h"
#include "Engine/TargetPoint.h"
#include "HN_DragGhostDoor.h"
#include "HN_Player.h"
#include "HN_Ghost_Player.h"

AHN_4F_Room::AHN_4F_Room()
{
	PrimaryActorTick.bCanEverTick = false;
	IsPlayer = false;

	//static ConstructorHelpers::FObjectFinder<UBlueprint> TriggerBlueprint(TEXT(
	//	"Blueprint'/Game/Blueprints/BP_Ghost_Player'")); //.BP_Ghost_Player'"));
	//if (TriggerBlueprint.Object) {
	//	PlayerBlueprintType = (UClass*)TriggerBlueprint.Object->GeneratedClass;
	//}

	//static ConstructorHelpers::FObjectFinder<UClass> TriggerBlueprint(TEXT(
	//	"Blueprint'/Game/Blueprints/BP_Ghost_Player.BP_Ghost_Player_C'")); //.BP_Ghost_Player'"));
	//if (TriggerBlueprint.Object) {
	//	PlayerBlueprintType = (UClass*)TriggerBlueprint.Object;
	//}

}

void AHN_4F_Room::BeginPlay()
{
	Super::BeginPlay();

	MaxIndex = TargetArr.Num();
	CurrentIndex = 0;
	TargetArr.Last();
}

void AHN_4F_Room::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AHN_4F_Room::Get_TargetLocation(FVector & TargetLocation)
{
	if(!IsPlayer)
		return false;

	TargetLocation = TargetArr[CurrentIndex]->GetActorLocation();

	return true;
}

bool AHN_4F_Room::Get_TargetRotator(FRotator & TargetRotator)
{
	if (!IsPlayer)
		return false;

	TargetRotator = TargetArr[CurrentIndex]->GetActorRotation();

	return true;
}

bool AHN_4F_Room::Set_ReTargetLocation()
{
	if (CurrentIndex + 1 == MaxIndex)
	{
		CurrentIndex = 0;
		return true;
	}

	++CurrentIndex;

	return false;
}

void AHN_4F_Room::SetPlayer(bool bCheck)
{
	IsPlayer = bCheck;
}

void AHN_4F_Room::Activate_Door(bool IsOpen)
{
	if (IsOpen)	// ���� ������
	{
		Door->ChangeState(DOOR_STATE::OPEN);
		Door->BoxCollision->SetCollisionProfileName(TEXT("NoCollision"));
	}
	else		// ���� ������ [ ArriveRoom ]
	{
		Door->ChangeState(DOOR_STATE::CLOSE);
		Door->BoxCollision->SetCollisionProfileName(TEXT("BlockAllDynamic"));
		Door->SetGhostMode(true);
	}
}

void AHN_4F_Room::SetActorIgnore(AHN_Player * targetPlayer, bool isIgnore)
{
	if (isIgnore)
	{
		targetPlayer->MoveIgnoreActorAdd(Door);
	}
	else
	{
		targetPlayer->MoveIgnoreActorRemove(Door);
	}
}

void AHN_4F_Room::SetGhost(AHN_Ghost_Drag * _Ghost)
{
	if (Door)
		Door->SetGhost(_Ghost);
}

void AHN_4F_Room::SetTargetDoor(bool bCheck)
{
	if (Door)
	{
		Door->SetTargetDoor(bCheck);

		if (!bCheck)
		{
			Door->SetGhostMode(false);
		}
	}
}

void AHN_4F_Room::SpawnGhost()
{
	FTransform SpawnLocation = TargetArr.Last()->GetTransform();
	FVector SpawnPosition = TargetArr.Last()->GetActorLocation();
	SpawnPosition.Z = 200.f;
	SpawnLocation.SetLocation(SpawnPosition);
	SpawnLocation.SetScale3D(FVector(1.5f, 1.5f, 1.5f));
	GhostPlayer = GetWorld()->SpawnActor<AHN_Ghost_Player>(AHN_Ghost_Player::StaticClass(), SpawnLocation);
	GhostPlayer->SpawnDefaultController();
}

void AHN_4F_Room::PenaltyGhostPlayer()
{
	GhostPlayer->DissolveGhost();
}

