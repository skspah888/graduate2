// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_NotShake.h"
#include "HN_Player.h"

UBTTask_4F_NotShake::UBTTask_4F_NotShake()
{
	NodeName = TEXT("NotShake");
}

EBTNodeResult::Type UBTTask_4F_NotShake::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto Player = Cast<AHN_Player>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (Player == nullptr)
		//LOG_TEXT(Warning, TEXT("No Player"));

	Player->Scamp_ShakeEnd();

	return EBTNodeResult::Succeeded;
}
