// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_3F_NurseChaseTarget.h"
#include "HN_Ghost_Nurse.h"
#include "HN_AI_Nurse.h"
#include "HN_AnimInst_Nurse.h"

UBTTask_3F_NurseChaseTarget::UBTTask_3F_NurseChaseTarget()
{
	bNotifyTick = true;
	NodeName = TEXT("Chase Target");
}

EBTNodeResult::Type UBTTask_3F_NurseChaseTarget::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto ControllingPawn = Cast<AHN_Ghost_Nurse>(OwnerComp.GetAIOwner()->GetPawn());
	ControllingPawn->SetNurseState(ENurseState::RUN);

	return EBTNodeResult::InProgress;
}

void UBTTask_3F_NurseChaseTarget::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);
	Cast<AHN_AI_Nurse>(OwnerComp.GetAIOwner())->ChaseTarget();
	FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
}
