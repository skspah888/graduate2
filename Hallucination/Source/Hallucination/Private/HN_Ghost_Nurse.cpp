// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Nurse.h"
#include "HN_AI_Nurse.h"
#include "Sound/SoundCue.h"
#include "HN_DissolveComponent.h"
#include "HN_Player.h"

AHN_Ghost_Nurse::AHN_Ghost_Nurse()
{
	PrimaryActorTick.bCanEverTick = false;

	// AI 설정
	AIControllerClass = AHN_AI_Nurse::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	GetCapsuleComponent()->InitCapsuleSize(90.f, 150.f);

	// 스켈레탈 메시 : 테스트용
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_NURSE(TEXT(
		"SkeletalMesh'/Game/Characters/Nurse/nurse.nurse'"));
	if (SK_NURSE.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_NURSE.Object);
	}

	// 애니메이션 설정
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> ANIM(TEXT(
		"AnimBlueprint'/Game/Characters/Nurse/AnimBP_Nurse.AnimBP_Nurse_C'"));
	if (ANIM.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(ANIM.Class);
	}

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 360.f, 0.f);
	GetCharacterMovement()->MaxWalkSpeed = 300.f;
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, 50.f), FRotator(0.f, -90.f, 0.f));

	// 캐릭터 조정
	GetMesh()->SetRelativeScale3D(FVector(1.5f, 1.5f, 1.5f));

	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("FlowerAudioComponent"));
	Speaker->SetupAttachment(RootComponent);

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundCue> AttackObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Nurse/NurseAttack_Cue.NurseAttack_Cue'"));
	if (AttackObject.Succeeded())
	{
		AttackSoundCue = AttackObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> DeadObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Nurse/NurseDead_Cue.NurseDead_Cue'"));
	if (DeadObject.Succeeded())
	{
		DeadSoundCue = DeadObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> WhisperObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Nurse/NurseWhisper_Cue.NurseWhisper_Cue'"));
	if (WhisperObject.Succeeded())
	{
		WhisperSoundCue = WhisperObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> LaughObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Nurse/NurseLaugh_Cue.NurseLaugh_Cue'"));
	if (LaughObject.Succeeded())
	{
		LaughSoundCue = LaughObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> PlayerDeadObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Nurse/NursePlayerDead_Cue.NursePlayerDead_Cue'"));
	if (PlayerDeadObject.Succeeded())
	{
		PlayerDeadSoundCue = PlayerDeadObject.Object;
	}

	// 디졸브
	DissolveComponent = CreateDefaultSubobject<UHN_DissolveComponent>(TEXT("DissolveComponent"));
	DissolveComponent->DissolveColor = FLinearColor::Blue;
	
	AttackRange = 300.f;
	IsGoal = false;
	IsAttacking = false;
}


void AHN_Ghost_Nurse::BeginPlay()
{
	Super::BeginPlay();

	NurseAnimInst = Cast<UHN_AnimInst_Nurse>(GetMesh()->GetAnimInstance());
	NurseAnimInst->OnMontageEnded.AddDynamic(this, &AHN_Ghost_Nurse::OnAttackMontageEnded);
	AttackRange = 400.f;
	StartWhisperSound();
}

void AHN_Ghost_Nurse::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ghost_Nurse::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);
}

void AHN_Ghost_Nurse::SetGoalPoint(bool bCheck)
{
	IsGoal = bCheck;
}

float AHN_Ghost_Nurse::GetAttackRange()
{
	return AttackRange;
}

bool AHN_Ghost_Nurse::GetGoalPoint()
{
	return IsGoal;
}
void AHN_Ghost_Nurse::NurseAttackAnim_Implementation() {
	NurseAnimInst->PlayAttackMontage();
}
void AHN_Ghost_Nurse::NurseAttack()
{
	if (IsAttacking || !NurseAnimInst)
		return;

	StartAttackSound();
	IsAttacking = true;
	//NurseAnimInst->PlayAttackMontage();
	NurseAttackAnim();
}

void AHN_Ghost_Nurse::StartAttackSound_Implementation()
{
	if (Speaker)
	{
		Speaker->SetSound(AttackSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Nurse::StartDeadSound_Implementation()
{
	if (Speaker)
	{
		Speaker->SetSound(DeadSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Nurse::StartWhisperSound_Implementation()
{
	if (Speaker)
	{
		Speaker->SetSound(WhisperSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Nurse::StartLaughSound_Implementation()
{
	if (Speaker)
	{
		Speaker->SetSound(LaughSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Nurse::StartPlayerDeadSound_Implementation()
{
	if (Speaker)
	{
		Speaker->SetSound(PlayerDeadSoundCue);
		Speaker->Play();
	}
}

void AHN_Ghost_Nurse::SetNurseState(ENurseState eState)
{
	//if (IsAttacking)
	//{
	//	ABCHECK(FMath::IsWithinInclusive<int32>(CurrentCombo, 1, MaxCombo));
	//	if (CanNextCombo)
	//	{
	//		IsComboInputOn = true;
	//	}
	//}
	//else
	//{
	//	ABCHECK(CurrentCombo == 0);
	//	AttackStartComboState();
	//	ABAnim->ABAnim();
	//	ABAnim->JumpToAttackMontageSection(CurrentCombo);
	//	IsAttacking = true;
	//}

	NurseAnimInst->SetState(eState);
}

void AHN_Ghost_Nurse::OnAttackMontageEnded(UAnimMontage * Montage, bool bInterrupted)
{
	IsAttacking = false;
	OnAttackEnd.Broadcast();
	StartPlayerDeadSound();
}

void AHN_Ghost_Nurse::NurseGhostDieSound_Implementation() {
	StartDeadSound();
	DissolveComponent->DoDissolve();
}

bool AHN_Ghost_Nurse::NurseGhostDie_Validate()
{
	return true;
}

void AHN_Ghost_Nurse::NurseGhostDie_Implementation()
{
	// Ghost가 죽을때 불리는 함수
	//Speaker->SetSound(GhostSoundCue);
	//Speaker->Play();
	//DissolveComponent->DoDissolve();
	NurseGhostDieSound();
	Cast<AHN_AI_Nurse>(GetController())->UnPossess();
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AHN_Ghost_Nurse::DestoryGhost, 3.f, false);
}

void AHN_Ghost_Nurse::DestoryGhost()
{
	Destroy();
}

AHN_Player * AHN_Ghost_Nurse::GetTarget()
{
	if (HasAuthority() == true)
		return Cast<AHN_AI_Nurse>(GetController())->GetTarget();
	else
		return NULL;
}
