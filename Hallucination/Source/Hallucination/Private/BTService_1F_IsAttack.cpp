// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_1F_IsAttack.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Player.h"
#include "HN_Ghost_Murderer.h"
#include "HN_AI_Murderer.h"

UBTService_1F_IsAttack::UBTService_1F_IsAttack()
{
	NodeName = TEXT("Attack Check");
	Interval = 0.1f;
}

void UBTService_1F_IsAttack::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto ControllingPawn = Cast<AHN_Ghost_Murderer>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
	{
		//print("Not ControllingPawn");
		return;
	}

	auto Target = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Murderer::TargetKey));
	if (nullptr == Target)
	{
		//print("Not Target");
		return;
	}

	FVector TargetLocation = Target->GetActorLocation();
	FVector PawnLocation = ControllingPawn->GetActorLocation();

	float fDist = FVector::DistXY(TargetLocation, PawnLocation);
	bool bResult = (fDist <= ControllingPawn->GetAttackRange());

	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Murderer::IsAttackKey, bResult);

}

