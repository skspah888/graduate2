// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_HN_StopSound.h"
#include "AIController.h"

UBTTask_HN_StopSound::UBTTask_HN_StopSound()
{
	NodeName = TEXT("HN_StopSound");
}

EBTNodeResult::Type UBTTask_HN_StopSound::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	APawn* Owner = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == Owner) return EBTNodeResult::Succeeded;

	auto Speaker = Cast<UAudioComponent>(Owner->GetComponentByClass(UAudioComponent::StaticClass()));

	if (nullptr == Speaker)
	{
		//LOG_TEXT(Warning, TEXT("Audio Component is null"));
		return EBTNodeResult::Succeeded;
	}

	//	플레이 스탑
	Speaker->Stop();

	return EBTNodeResult::Succeeded;
}
