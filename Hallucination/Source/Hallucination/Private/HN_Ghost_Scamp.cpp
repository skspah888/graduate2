// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Scamp.h"
#include "HN_AI_Scamp.h"
#include "HN_Player.h"
#include "HN_Door.h"
#include "HN_Ghost_Patient.h"
#include "HN_Ring.h"

AHN_Ghost_Scamp::AHN_Ghost_Scamp()
{
	PrimaryActorTick.bCanEverTick = true;

	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));
	Speaker->SetupAttachment(RootComponent);

	// 스켈레탈 메시
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_DOLL(TEXT("SkeletalMesh'/Game/Characters/Scamp/scampani_idle.scampani_idle'"));
	if (SK_DOLL.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_DOLL.Object);
	}

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -88.f), FRotator(0.f, -90.f, 0.f));
	GetMesh()->SetWorldScale3D(FVector(0.5f, 0.5f, 0.5f));
	//	변수 초기화

	bIsComplete = false;

	StartTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("TRRIGER"));
	StartTrigger->SetupAttachment(RootComponent);
	StartTrigger->SetWorldLocation(FVector(-310.f, -570.f, 0.f));
	StartTrigger->SetBoxExtent(FVector(550.f, 550.f, 30.f));
	StartTrigger->SetCollisionProfileName(TEXT("HNTrigger"));

	iPlayerCount = 0;

	GetCapsuleComponent()->SetActive(false);

	//	AI 설정
	AIControllerClass = AHN_AI_Scamp::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;


	fOpacity = 0.f;

	//	애니메이션 블루프린트 설정
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);

	static ConstructorHelpers::FClassFinder<UAnimInstance> ANIM(TEXT("/Game/Characters/Scamp/Scamp_AnimBP.Scamp_AnimBP_C"));
	if (ANIM.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(ANIM.Class);
	}

	//	사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundBase> DIE_SOUND(TEXT("SoundCue'/Game/Audio/SoundCue/scamp_dead_Cue.scamp_dead_Cue'"));

	if (DIE_SOUND.Succeeded())
	{
		DieSound = DIE_SOUND.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> START_SOUND(TEXT("SoundCue'/Game/Audio/SoundCue/Scamp_Start_Cue.Scamp_Start_Cue'"));

	if (START_SOUND.Succeeded())
	{
		StartSound = START_SOUND.Object;

	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SHAKE_SOUND(TEXT("SoundCue'/Game/Audio/SoundCue/Find_Me.Find_Me'"));

	if (SHAKE_SOUND.Succeeded())
	{
		ShakeSound = SHAKE_SOUND.Object;

	}

	static ConstructorHelpers::FObjectFinder<USoundBase> NOTSHAKE_SOUND(TEXT("SoundCue'/Game/Audio/SoundCue/scamp_laugh_Cue.scamp_laugh_Cue'"));

	if (NOTSHAKE_SOUND.Succeeded())
	{
		NotShakeSound = NOTSHAKE_SOUND.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> BGM_SOUND(TEXT("SoundCue'/Game/Audio/SoundCue/scamp_bgm_Cue.scamp_bgm_Cue'"));

	if (BGM_SOUND.Succeeded())
	{
		BGMSound = BGM_SOUND.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> BGM4F_SOUND(TEXT("SoundCue'/Game/Audio/SoundCue/Bgm_Cue.Bgm_Cue'"));

	if (BGM4F_SOUND.Succeeded())
	{
		BGM4FSound = BGM4F_SOUND.Object;
	}
}

void AHN_Ghost_Scamp::OnCharacterOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this)) {
		auto Player = Cast<AHN_Player>(OtherActor);

		if (nullptr != Player)
		{
			++iPlayerCount;
			//print("In Player");
			//LOG_TEXT(Warning, TEXT("In Player"));

			if (iPlayerCount == 2)
			{
				//	플레이어가 두 명 다 들어옴
				//print("In Player 2, AI Start");
				//LOG_TEXT(Warning, TEXT("In Player 2, AI Start"));
				if (GetController() == nullptr)
					return;
				//ActiveActor();
				GetCapsuleComponent()->SetActive(true);
				AIController = Cast<AHN_AI_Scamp>(GetController());
				//AIController->RunAI();
				if (AIController != nullptr)
					AIController->RunAI();
				StartTrigger->SetActive(false);

				//환자귀신 소리 멈추기
				Patient->PlayPatientSound(false);

				//UGameplayStatics::PlaySound2D(GetWorld(), BGMSound);

				PlayStartSound();
				ChangeAnim_ScampDoorClose();// 리플리케이트를 위해 따로 함수생성
			}
		}
	}
}
void AHN_Ghost_Scamp::ChangeAnim_ScampDoorClose_Implementation() {
	if (iPlayerCount == 2) {
		if (RoomDoor != nullptr)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), BGMSound);
			RoomDoor->ChangeState(DOOR_STATE::CLOSE);
			RoomDoor->SetLock(true);
		}
	}
}

void AHN_Ghost_Scamp::OnCharacterOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this)) {
		auto Player = Cast<AHN_Player>(OtherActor);

		if (nullptr != Player)
		{
			//print("Out Player");
			//LOG_TEXT(Warning, TEXT("Out Player"));
			--iPlayerCount;
		}
	}
}

void AHN_Ghost_Scamp::BeginPlay()
{
	Super::BeginPlay();

	DisableActor();
}

void AHN_Ghost_Scamp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsComplete)
	{
		if (RoomDoor != nullptr)
		{
			ChangeAnim_ScampDoorOpen();

			//Patient->Acquired_Ring();

			//	반지 생성
			FActorSpawnParameters SpawnInfo;
			FVector fLocation = GetActorLocation();
			fLocation.Z = fLocation.Z + 80.f;
			//GetWorld()->SpawnActor<AHN_Ring>(AHN_Ring::StaticClass(), fLocation, GetActorRotation(), SpawnInfo);

			FName Path = TEXT("Blueprint'/Game/Blueprints/BP_Ring.BP_Ring_C'");
			UClass* GeneratedBP = Cast<UClass>(StaticLoadObject(UClass::StaticClass(), NULL, *Path.ToString()));
			GetWorld()->SpawnActor<AHN_Ring>(GeneratedBP, fLocation, FRotator::ZeroRotator);

			// 클리어
			Cast<AHN_AI_Scamp>(GetController())->SetClear();

			Patient->PlayPatientSound(true);

			DisableActor();
			PlayDieSound();

			UGameplayStatics::PlaySound2D(GetWorld(), BGM4FSound);

			FTimerHandle Timer;
			GetWorldTimerManager().SetTimer(Timer, this, &AHN_Ghost_Scamp::DestroyActorFunction, 3);
		}
	}
}
void AHN_Ghost_Scamp::ChangeAnim_ScampDoorOpen_Implementation()
{
	if (RoomDoor != nullptr)
	{
		RoomDoor->SetLock(false);
		RoomDoor->ChangeState(DOOR_STATE::OPEN);
	}
}

void AHN_Ghost_Scamp::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
}

void AHN_Ghost_Scamp::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	StartTrigger->OnComponentBeginOverlap.AddDynamic(this, &AHN_Ghost_Scamp::OnCharacterOverlapBegin);
	StartTrigger->OnComponentEndOverlap.AddDynamic(this, &AHN_Ghost_Scamp::OnCharacterOverlapEnd);
}

void AHN_Ghost_Scamp::DisableActor()
{
	SetActorHiddenInGame(true);
	//SetActorEnableCollision(false);
	SetActorTickEnabled(false);
}

void AHN_Ghost_Scamp::ActiveActor()
{
	SetActorHiddenInGame(false);
	//SetActorEnableCollision(true);
	SetActorTickEnabled(true);
}

void AHN_Ghost_Scamp::PlayDieSound_Implementation()
{
	Speaker->Stop();
	Speaker->SetSound(DieSound);
	Speaker->Play();
}

void AHN_Ghost_Scamp::PlayStartSound_Implementation()
{
	Speaker->Stop();
	Speaker->SetSound(StartSound);
	Speaker->Play();
}

void AHN_Ghost_Scamp::PlayShakeSound_Implementation()
{
	Speaker->SetSound(ShakeSound);
	Speaker->Play();
}

void AHN_Ghost_Scamp::PlayNotShakeSound_Implementation()
{
	//Speaker->Stop();
	Speaker->SetSound(NotShakeSound);
	Speaker->Play();
}

void AHN_Ghost_Scamp::DestroyActorFunction()
{
	Destroy();
}
