// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_DragGhostDoor.h"
#include "HN_Ghost_Drag.h"

AHN_DragGhostDoor::AHN_DragGhostDoor()
{
	//	틀 초기화
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_FRAME(TEXT("StaticMesh'/Game/StarterContent/Props/SM_DoorFrame.SM_DoorFrame'"));

	if (SM_FRAME.Succeeded())
	{
		Frame->SetStaticMesh(SM_FRAME.Object);
	}

	//	문 초기화
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_DOOR(TEXT("StaticMesh'/Game/StarterContent/Props/SM_Door.SM_Door'"));

	if (SM_DOOR.Succeeded())
	{
		Door->SetStaticMesh(SM_DOOR.Object);
	}

	CurRotAngle = 45;
	MaxRotAngle = 120;
	Door->SetRelativeLocation(FVector(0.f, 45.f, 0.f));

	Door->ComponentTags.Add(TEXT("outline"));

	IsDragDoor = false;
	IsTargetDoor = false;
}

void AHN_DragGhostDoor::BeginPlay()
{
	Super::BeginPlay();
	BoxCollision->SetCollisionProfileName(TEXT("BlockAllDynamic"));
}

// Called every frame
void AHN_DragGhostDoor::Tick(float DeltaTime)
{
	switch (eState)
	{
	case DOOR_STATE::IDLE:
		Idle();
		break;

	case DOOR_STATE::OPEN:
		Open();
		break;

	case DOOR_STATE::CLOSE:
		Close();
		break;
	default:
		break;
	}
}

void AHN_DragGhostDoor::Idle()
{
	//SetActorTickEnabled(false);
}

void AHN_DragGhostDoor::Open_Implementation()
{
	if (bLock)
	{
		eState = DOOR_STATE::IDLE;
		return;
	}
	else if (abs(CurRotAngle) == MaxRotAngle)
	{
		eState = DOOR_STATE::IDLE;
		bOpen = true;
		return;
	}

	//LOG_TEXT(Warning, TEXT("Open"));
	++CurRotAngle;

	Door->SetRelativeLocation(FVector(0.0f, CurRotAngle, 0.0f));
}

void AHN_DragGhostDoor::Close_Implementation()
{
	if (CurRotAngle == 45)
	{
		eState = DOOR_STATE::IDLE;
		bOpen = false;
		return;
	}

	//LOG_TEXT(Warning, TEXT("Close"));

	--CurRotAngle;

	Door->SetRelativeLocation(FVector(0.0f, CurRotAngle, 0.0f));
}

void AHN_DragGhostDoor::SetGhost(AHN_Ghost_Drag * _Ghost)
{
	if (_Ghost)
		DragGhost = _Ghost;
}

void AHN_DragGhostDoor::SetDragBlackBoard(FString BoardName, bool bCheck)
{
	DragGhost->SetDragBlackBoard(BoardName, bCheck);
}

void AHN_DragGhostDoor::SetGhostMode(bool bCheck)
{
	IsDragDoor = bCheck;
}

void AHN_DragGhostDoor::SetTargetDoor(bool bCheck)
{
	IsTargetDoor = bCheck;
}
