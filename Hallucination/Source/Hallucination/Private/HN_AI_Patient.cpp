// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AI_Patient.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

// 키 이름이 절대 변하지 않는다는 가정
	// 다른 코드에서 관련값 참조가 편하지만, 하드코딩으로 값 변경을 해야하는 단점이 있음
const FName AHN_AI_Patient::IsContactKey(TEXT("IsContact"));
const FName AHN_AI_Patient::TargetKey(TEXT("Target"));
const FName AHN_AI_Patient::IsRingKey(TEXT("IsRing"));
const FName AHN_AI_Patient::IsClearKey(TEXT("IsClear"));

AHN_AI_Patient::AHN_AI_Patient()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT(
		"BlackboardData'/Game/AI/4F/BB_4F.BB_4F'"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;		// 블랙보드 셋팅
	}

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT(
		"BehaviorTree'/Game/AI/4F/BT_4F_Patient.BT_4F_Patient'"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;		// 비헤이비어트리 셋팅
	}
}

void AHN_AI_Patient::BeginPlay()
{
	Super::BeginPlay();
}

void AHN_AI_Patient::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	//// 키 값 등록하는 법
	//Blackboard->SetValueAsBool(IsContactKey, true);

	// 블랙보드 사용
	UseBlackboard(BBAsset, Blackboard);

	// 반지 기본 값 셋팅
	Blackboard->SetValueAsBool(IsRingKey, false);
}

void AHN_AI_Patient::OnUnPossess()
{
	Super::OnUnPossess();
}

void AHN_AI_Patient::Acquired_Ring()
{
	// 반지 획득
	Blackboard->SetValueAsBool(IsRingKey, true);
}

bool AHN_AI_Patient::IsRing()
{
	bool bCheck = Blackboard->GetValueAsBool(IsRingKey);

	if (bCheck)
	{
		//LOG_TEXT(Warning, TEXT("TRUE"));
	}
	else
	{
		//LOG_TEXT(Warning, TEXT("FALSE"));
	}
	return true;
}

void AHN_AI_Patient::SetClear(bool IsClear)
{
	Blackboard->SetValueAsBool(IsClearKey, IsClear);
}

void AHN_AI_Patient::ExcuteBehaviorTree()
{
	// 비헤이비어 트리 사용
	RunBehaviorTree(BTAsset);
}

void AHN_AI_Patient::SetPatientClear()
{
	Blackboard->SetValueAsBool(IsRingKey, false);
	Blackboard->SetValueAsBool(IsClearKey, false);
}

void AHN_AI_Patient::TestPatient()
{
	Blackboard->SetValueAsBool(IsRingKey, true);
	Blackboard->SetValueAsBool(IsClearKey, true);
}
