// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_3F_TriggerVolume.h"
#include "DrawDebugHelpers.h"
#include "HN_Ghost_Flower.h"
#include "Engine/TargetPoint.h"
#include "EngineUtils.h"
#include "HN_Player.h"

AHN_3F_TriggerVolume::AHN_3F_TriggerVolume()
{
	OnActorBeginOverlap.AddDynamic(this, &AHN_3F_TriggerVolume::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AHN_3F_TriggerVolume::OnOverlapEnd);
}

void AHN_3F_TriggerVolume::BeginPlay()
{
	//DrawDebugBox(GetWorld(), GetActorLocation(), GetActorScale() * 100, FColor::Cyan, true, -1, 0, 5);
}

bool AHN_3F_TriggerVolume::OnOverlapBegin_Validate(AActor* OverlappedActor, AActor* OtherActor)
{
	return true;
}

void AHN_3F_TriggerVolume::OnOverlapBegin_Implementation(AActor * OverlappedActor, AActor * OtherActor)
{
	if (OtherActor && (OtherActor != this)) {
		//print("Overlap Begin");
		//printFString("Other Actor = %s", *OtherActor->GetName());

		if (TargetPoint == nullptr)
			return;
		
		if (Cast<AHN_Player>(OtherActor))
		{
			/*FActorSpawnParameters SpawnInfo;
			AHN_Ghost_Flower* Ghost = GetWorld()->SpawnActor<AHN_Ghost_Flower>(AHN_Ghost_Flower::StaticClass(), TargetPoint->GetActorLocation(), OtherActor->GetActorRotation(), SpawnInfo);
			Ghost->SetOriginPosition(GetActorLocation());*/

			FTransform SpawnLocation = TargetPoint->GetTransform();
			//FVector SpawnPosition = TargetPoint->GetActorLocation();
			//SpawnLocation.SetLocation(SpawnPosition);
			//SpawnLocation.SetScale3D(FVector(0.01f, 0.01f, 0.01f));
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AHN_Ghost_Flower* Ghost = GetWorld()->SpawnActor<AHN_Ghost_Flower>(AHN_Ghost_Flower::StaticClass(), SpawnLocation, SpawnInfo);
			Ghost->SetOriginPosition(GetActorLocation());
			Destroy();
		}
	}
}

void AHN_3F_TriggerVolume::OnOverlapEnd(AActor * OverlappedActor, AActor * OtherActor)
{
	if (OtherActor && (OtherActor != this)) {
		//print("Overlap Ended");
		//printFString("%s has left the Trigger Volume", *OtherActor->GetName());
	}
}