// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_4F_Contact.h"
#include "HN_AI_Scamp.h"
#include "HN_Ghost_Scamp.h"
#include "HN_Player.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "DrawDebugHelpers.h"


UBTService_4F_Contact::UBTService_4F_Contact()
{
	NodeName = TEXT("Contact");
	Interval = 0.5f;
}

void UBTService_4F_Contact::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSecond)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSecond);

	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == ControllingPawn) return;

	UWorld* World = ControllingPawn->GetWorld();
	FVector Center = ControllingPawn->GetActorLocation();
	float DetectRadius = 100.0f;

	auto Scamp = Cast<AHN_Ghost_Scamp>(ControllingPawn);

	if (nullptr == Scamp) return;
	if (nullptr == World) return;

	TArray<FOverlapResult> OverlapResults;
	FCollisionQueryParams CollisionQueryParam(NAME_None, false, ControllingPawn);
	bool bResult = World->OverlapMultiByChannel(OverlapResults, Center, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel12, FCollisionShape::MakeSphere(DetectRadius), CollisionQueryParam);

	if (bResult)
	{
		//	무언가 충돌했다
		for (auto OverlapResult : OverlapResults)
		{
			//	겹쳐진 오브젝트들 돌면서
			AHN_Player* Player = Cast<AHN_Player>(OverlapResult.GetActor());
			//	플레이어가 맞으면
			if (Player && Player->GetController()->IsPlayerController())
			{
				
				//OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Patient::IsRingKey, true);
				Scamp->SetComplete(true);

				//DrawDebugSphere(World, Center, DetectRadius, 16, FColor::Green, false, 0.2f);
			//	DrawDebugPoint(World, Player->GetActorLocation(), 10.0f, FColor::Blue, false, 0.2f);
				//DrawDebugLine(World, ControllingPawn->GetActorLocation(), Player->GetActorLocation(), FColor::Blue, false, 0.2f);
				return;
			}
		}
	}

	//DrawDebugSphere(World, Center, DetectRadius, 16, FColor::Red, false, 0.2f);
}
