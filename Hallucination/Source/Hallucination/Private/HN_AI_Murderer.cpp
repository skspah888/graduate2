// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AI_Murderer.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "EngineUtils.h"
#include "HN_Player.h"
#include "HN_Ghost_Murderer.h"

const FName AHN_AI_Murderer::TargetKey(TEXT("TargetObject"));
const FName AHN_AI_Murderer::IsAttackKey(TEXT("IsAttack"));

AHN_AI_Murderer::AHN_AI_Murderer()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT(
		"BlackboardData'/Game/AI/1F/BB_1F.BB_1F'"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;		// 블랙보드 셋팅
	}

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT(
		"BehaviorTree'/Game/AI/1F/BT_1F_Murderer.BT_1F_Murderer'"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;		// 비헤이비어트리 셋팅
	}
}

void AHN_AI_Murderer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_AI_Murderer::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	UseBlackboard(BBAsset, Blackboard);
	// 블랙보드 사용
	//GetBlackboardComponent()->SetValueAsFloat(DelayTimeKey, RandTime);
}

void AHN_AI_Murderer::OnUnPossess()
{
	Super::OnUnPossess();
}

AHN_Player * AHN_AI_Murderer::GetTarget()
{
	return Cast<AHN_Player>(GetBlackboardComponent()->GetValueAsObject(TargetKey));
}

void AHN_AI_Murderer::ExcuteBehaviorTree()
{
	// 비헤이비어 트리 사용
	RunBehaviorTree(BTAsset);

	//// 사운드 재생
	//AHN_Ghost_Murderer* ControllerPawn = Cast<AHN_Ghost_Murderer>(GetPawn());
	//if (ControllerPawn)
	//{
	//	ControllerPawn->StartBreathSound();
	//}
}
