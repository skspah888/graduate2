// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AnimInst_Patient.h"
#include "HN_AI_Patient.h"

UHN_AnimInst_Patient::UHN_AnimInst_Patient()
{
	IsRing = false;
}

void UHN_AnimInst_Patient::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	//auto Pawn = TryGetPawnOwner();eState

	//if (!::IsValid(Pawn)) return;

	//IsRing = Cast<AHN_AI_Patient>(Pawn->GetController())->GetIsRing();
}

void UHN_AnimInst_Patient::SetIsRing()
{
	IsRing = true;
}
