// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_4F_DetectPlayer.h"
#include "HN_AI_Patient.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "DrawDebugHelpers.h"
#include "HN_Player.h"

UBTService_4F_DetectPlayer::UBTService_4F_DetectPlayer()
{
	NodeName = TEXT("Detect");
	Interval = 0.f;
}

void UBTService_4F_DetectPlayer::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == ControllingPawn)
		return;

	UWorld* World = ControllingPawn->GetWorld();
	FVector Center = ControllingPawn->GetActorLocation();
	float DetectRadius = 300.f;

	if (nullptr == World)
		return;

	TArray<FOverlapResult> OverlapResults;
	FCollisionQueryParams CollisionQueryParam(NAME_None, false, ControllingPawn);
	bool bResult = World->OverlapMultiByChannel(OverlapResults, Center, FQuat::Identity,
		ECollisionChannel::ECC_GameTraceChannel2,
		FCollisionShape::MakeSphere(DetectRadius),
		CollisionQueryParam);

	// 플레이어 탐지됨.
	if (bResult)
	{
		for (auto OverlapResult : OverlapResults)
		{
			AHN_Player* Player = Cast<AHN_Player>(OverlapResult.GetActor());
			if (Player && Player->GetController()->IsPlayerController())
			{
				// 블랙보드 Target 값을 플레이어로 지정
				OwnerComp.GetBlackboardComponent()->SetValueAsObject(AHN_AI_Patient::TargetKey, Player);
				OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Patient::IsContactKey, true);
				// 구체색 변경
				//DrawDebugSphere(World, Center, DetectRadius, 16, FColor::Green, false, 1.f);
				// NPC와 연결된 선 추가로 그림
				//DrawDebugPoint(World, Player->GetActorLocation(), 10.f, FColor::Blue, false, 0.2f);
				//DrawDebugLine(World, ControllingPawn->GetActorLocation(), Player->GetActorLocation(), FColor::Blue, false, 0.1f);
				return;
			}
		}
	}
	OwnerComp.GetBlackboardComponent()->SetValueAsObject(AHN_AI_Patient::TargetKey, nullptr);
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Patient::IsContactKey, false);
	//DrawDebugSphere(World, Center, DetectRadius, 16, FColor::Red, false, 0.1f);
}
