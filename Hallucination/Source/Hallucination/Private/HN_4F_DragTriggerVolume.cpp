// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_4F_DragTriggerVolume.h"
#include "HN_Player.h"
#include "HN_Ghost_Drag.h"

// Sets default values
AHN_4F_DragTriggerVolume::AHN_4F_DragTriggerVolume()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TriggerZone = CreateDefaultSubobject<UBoxComponent>("TriggerZone");
	TriggerZone->SetBoxExtent(FVector(500, 400, 100));
	RootComponent = TriggerZone;
	PlayerCount = 0;
}

// Called when the game starts or when spawned
void AHN_4F_DragTriggerVolume::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AHN_4F_DragTriggerVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_4F_DragTriggerVolume::NotifyActorBeginOverlap(AActor * OtherActor)
{
	if (Cast<AHN_Player>(OtherActor))
	{
		++PlayerCount;
		//print("Begin Player");
		if (PlayerCount == 2)
		{
			// Drag Ghost Spawn
			FTransform SpawnLocation;
			SpawnLocation.SetLocation(OtherActor->GetActorLocation());
			GetWorld()->SpawnActor<AHN_Ghost_Drag>(AHN_Ghost_Drag::StaticClass(), SpawnLocation);

			// Volume Destroy
			Destroy();
		}
	}
}

void AHN_4F_DragTriggerVolume::NotifyActorEndOverlap(AActor * OtherActor)
{
	if (Cast<AHN_Player>(OtherActor))
	{
		//print("End Player");
		--PlayerCount;
	}
}

void AHN_4F_DragTriggerVolume::TestSpawn()
{
	// Drag Ghost Spawn
	FTransform SpawnLocation;
	SpawnLocation.SetLocation(FVector(100.f,100.f,100.f));
	GetWorld()->SpawnActor<AHN_Ghost_Drag>(AHN_Ghost_Drag::StaticClass(), SpawnLocation);

	// Volume Destroy
	Destroy();
}

