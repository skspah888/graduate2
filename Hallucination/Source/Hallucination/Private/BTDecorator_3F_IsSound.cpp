// Fill out your copyright notice in the Description page of Project Settings.


#include "BTDecorator_3F_IsSound.h"
#include "HN_AI_Flower.h"
#include "HN_Ghost_Flower.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTDecorator_3F_IsSound::UBTDecorator_3F_IsSound()
{
	NodeName = TEXT("IsSound");
}

bool UBTDecorator_3F_IsSound::CalculateRawConditionValue(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) const
{
	// 원하는 조건이 달성됐는지 파악
	bool bResult = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	auto ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == ControllingPawn)
		return false;

	bool IsSound = OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Flower::IsSoundKey);

	return IsSound;
}
