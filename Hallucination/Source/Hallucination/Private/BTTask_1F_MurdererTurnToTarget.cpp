// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_1F_MurdererTurnToTarget.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Ghost_Murderer.h"
#include "HN_Player.h"
#include "HN_AI_Murderer.h"

UBTTask_1F_MurdererTurnToTarget::UBTTask_1F_MurdererTurnToTarget()
{
	NodeName = TEXT("Turn To Target");
}

EBTNodeResult::Type UBTTask_1F_MurdererTurnToTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto ControllingPawn = Cast<AHN_Ghost_Murderer>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;

	auto Target = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Murderer::TargetKey));
	if (nullptr == Target)
		return EBTNodeResult::Failed;

	// 내가 추적한 대상을 바라보는 벡터 구하기
	FVector LookVector = Target->GetActorLocation() - ControllingPawn->GetActorLocation();
	LookVector.Z = 0.f;
	FRotator TargetRot = FRotationMatrix::MakeFromX(LookVector).Rotator();

	// 지정한 속도만큼 회전
	ControllingPawn->SetActorRotation(FMath::RInterpTo(ControllingPawn->GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 90.f));

	return EBTNodeResult::Succeeded;
}

