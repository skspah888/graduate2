// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Player.h"
#include "HN_PlayerController.h"
#include "EngineUtils.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "HN_CameraShake.h"
#include "DrawDebugHelpers.h"
#include "Interactable.h"
#include "HN_Item.h"
#include "Sound/SoundCue.h"
#include <Kismet/KismetMathLibrary.h>

AHN_Player::AHN_Player()
{
	PrimaryActorTick.bCanEverTick = true;

	InitPlayer();
}

void AHN_Player::InitPlayer()
{
	// 컴포넌트 초기화
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("HNCharacter"));
	VROrigin = CreateDefaultSubobject<USceneComponent>(TEXT("VROrigin"));
	PCOrigin = CreateDefaultSubobject<USceneComponent>(TEXT("PCOrigin"));
	PCCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PC_Camera"));
	VRCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("VR_Camera"));
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("Speaker"));
	EffectSpeaker = CreateDefaultSubobject<UAudioComponent>(TEXT("EffectSpeaker"));
	DamageSpeaker = CreateDefaultSubobject<UAudioComponent>(TEXT("DamageSpeaker"));

	// 스켈레탈 메시
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_PLAYER(TEXT(
		"SkeletalMesh'/Game/Asset/NewAsset/4F_Model/Player/Player1/Player1.Player1'"));
	if (SK_PLAYER.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_PLAYER.Object);
	}

	// 상속 관계 설정
	RootComponent = GetCapsuleComponent();
	VROrigin->SetupAttachment(RootComponent);
	PCOrigin->SetupAttachment(RootComponent);
	VRCamera->SetupAttachment(VROrigin);
	PCCamera->SetupAttachment(PCOrigin);
	Speaker->SetupAttachment(GetMesh());
	EffectSpeaker->SetupAttachment(GetMesh());
	DamageSpeaker->SetupAttachment(GetMesh());

	// 카메라 설정
	PCCamera->bUsePawnControlRotation = true;
	PCCamera->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	PCCamera->SetRelativeLocation(FVector(20.f, 0.f, 40.f));

	VRCamera->bUsePawnControlRotation = true;
	VRCamera->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	VRCamera->SetRelativeLocation(FVector(20.f, 0.f, 40.f));

	// 캐릭터 무브먼트 설정
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 360.f, 0.f);

	// 캐릭터 조정
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -97.f), FRotator(0.f, 0.f, 0.f));
	GetCharacterMovement()->MaxWalkSpeed = 500.f;

	VROrigin->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	VRCamera->SetRelativeLocation(FVector(0.f, 0.f, GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + 50.f));
	PCCamera->SetRelativeLocation(FVector(0.f, 0.f, GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + 50.f));

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundCue> PlayerSoundCueObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/wheelchair_Cue.wheelchair_Cue'"));
	if (PlayerSoundCueObject.Succeeded())
	{
		PlayerWalkSoundCue = PlayerSoundCueObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> SOUND_SCARY(TEXT(
		"SoundCue'/Game/Audio/SoundCue/smooth_breath_Cue.smooth_breath_Cue'"));
	if (SOUND_SCARY.Succeeded())
	{
		ScarySoundCue = SOUND_SCARY.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> SOUND_FEAR(TEXT(
		"SoundCue'/Game/Audio/SoundCue/rough_breath_Cue.rough_breath_Cue'"));
	if (SOUND_FEAR.Succeeded())
	{
		FearSoundCue = SOUND_FEAR.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> SOUND_TERRIBLE(TEXT(
		"SoundCue'/Game/Audio/SoundCue/breathe_heartbeat_Cue.breathe_heartbeat_Cue'"));
	if (SOUND_TERRIBLE.Succeeded())
	{
		TerribleSoundCue = SOUND_TERRIBLE.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> SOUND_DAMAGE(TEXT(
		"SoundCue'/Game/Audio/SoundCue/player_damage_Cue.player_damage_Cue'"));
	if (SOUND_DAMAGE.Succeeded())
	{
		DamageSoundCue = SOUND_DAMAGE.Object;
	}

	//	변수 초기화
	fRotate = 0.f;
	MyShake = UHN_CameraShake::StaticClass();

	TeamId = FGenericTeamId(0);

	for (int i = 0; i < (int)HAND_TYPE::END; ++i)
	{
		Inventory[i] = NULL;
	}

	//	플레이어 상태
	bChange = true;
	iMaxHp = 50;
	iHp = iMaxHp;
	eState = PLAYER_STATE::CLAM;
	fGamma = 1.0f;
	IsNightVision = false;
	fHealCoolTime = 0.f;

	static ConstructorHelpers::FObjectFinder<UMaterial> MTRL_BLUR(TEXT(
		"Material'/Game/Materials/M_RadialBlur_PP.M_RadialBlur_PP'"));
	if (MTRL_BLUR.Succeeded())
	{
		pBlurMtrl = MTRL_BLUR.Object;
	}

	//PCCamera->PostProcessSettings.AddBlendable(pBlurMtrl, 1);

	//	나이트 비전 세팅
	pNightVision = new FPostProcessSettings();
	static ConstructorHelpers::FObjectFinder<UTexture> TEX_NIGHTVISION(TEXT(
		"Texture2D'/Game/Textures/ColorLUT_NightVision.ColorLUT_NightVision'"));
	if (TEX_NIGHTVISION.Succeeded())
	{
		pNightVision->ColorGradingLUT = TEX_NIGHTVISION.Object;
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> MTRL_NIGHTVISION(TEXT(
		"Material'/Game/Materials/PP_NightVision.PP_NightVision'"));
	if (MTRL_NIGHTVISION.Succeeded())
	{
		pNightVision->AddBlendable(MTRL_NIGHTVISION.Object, 1);
	}

	pNormalVision = new FPostProcessSettings();

	static ConstructorHelpers::FObjectFinder<UMaterial> MTRL_OUTLINE(TEXT(
		"Material'/Game/Materials/Shader/Outline/PP_Outliner_M.PP_Outliner_M'"));
	if (MTRL_OUTLINE.Succeeded())
	{
		pNormalVision->AddBlendable(MTRL_OUTLINE.Object, 1);
	}

	IsInteractObject = true;
	IsPlayerController = true;
	IsVRMode = false;

	InvincibilityTime = 1.f;
	IsInvincibility = false;
	InvincibilityCurrentTime = 0.f;
}

void AHN_Player::BeginPlay()
{
	Super::BeginPlay();

	SetCameraActivate(IsVRMode);

	//// 컨트롤러 셋팅
	//if (IsPlayerControlled())
	//{
	//	HNPlayerController = Cast<AHN_PlayerController>(GetController());
	//	ERROR_CHECK(nullptr == HNPlayerController);
	//	EnableInput(HNPlayerController);
	//}


	// 사운드 셋팅
	if (PlayerWalkSoundCue && Speaker)
	{
		Speaker->SetSound(PlayerWalkSoundCue);
	}

	// 델리게이트 셋팅
	Fuc_DeleSingle_OneParam.BindUFunction(this, FName("DeleFunc_SetPosition"));


	//	이펙트 설정
	pNormalVision->bOverride_ColorGamma = 1;
	pNormalVision->bOverride_VignetteIntensity = 1;

	//	나이트 비전 세팅

	pNightVision->bOverride_WhiteTemp = 1;
	pNightVision->WhiteTemp = 4957.f;
	pNightVision->bOverride_WhiteTint = 1;
	pNightVision->WhiteTint = 0.43f;
	pNightVision->bOverride_ColorSaturation = 1;
	pNightVision->ColorSaturation = FVector4(0.56f, 0.56f, 0.56f, 1.f);
	pNightVision->bOverride_ColorContrast = 1;
	pNightVision->ColorContrast = FVector4(1.34f, 1.34f, 1.34f, 1.f);
	pNightVision->bOverride_ColorGamma = 1;
	pNightVision->ColorGamma = FVector4(1.5f, 1.5f, 1.5f, 1.f);
	pNightVision->bOverride_VignetteIntensity = 1;
	pNightVision->VignetteIntensity = 1.3f;
	pNightVision->bOverride_GrainIntensity = 1;
	pNightVision->GrainIntensity = 0.05;
	pNightVision->bOverride_ColorGradingLUT = 1;


	if (IsVRMode)
	{
		VRCamera->PostProcessSettings = *pNormalVision;
	}
	else
	{
		PCCamera->PostProcessSettings = *pNormalVision;
	}
}

void AHN_Player::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	Fuc_DeleSingle_OneParam.Unbind();
}

void AHN_Player::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// 캐릭터 휠체어 소리
	FVector vecVelocity = GetVelocity();
	vecVelocity.Z = 0.f;
	float Length = vecVelocity.Size();
	PlayWalkSound(Length);

	//	플레이어 지속 힐
	fHealCoolTime += DeltaTime;

	if (FMath::IsNearlyEqual(fHealCoolTime, 60.f, 0.01f))
	{
		//print("Heal");
		fHealCoolTime = 0.f;
		Heal(2);
	}

	// 캐릭터 공포도(체력)에 따른 캐릭터 효과
	if (!bChange)
	{
		if (eState == PLAYER_STATE::TERRIBLE)
			RemoveBloodyEffect();
		else if (eState == PLAYER_STATE::BLOODY)
			BloodyEffect();
	}

	// 무적시간
	if (IsInvincibility)
	{
		InvincibilityCurrentTime += DeltaTime;
		if (InvincibilityCurrentTime >= InvincibilityTime)
		{
			InvincibilityCurrentTime = 0.f;
			IsInvincibility = false;
		}
	}
}

void AHN_Player::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
}

void AHN_Player::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// 키 입력 바인딩 부분
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("UpDown"), this, &AHN_Player::UpDown);
	PlayerInputComponent->BindAxis(TEXT("LeftRight"), this, &AHN_Player::LeftRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &AHN_Player::LookUp);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &AHN_Player::Turn);
	PlayerInputComponent->BindAction(TEXT("Interact"), EInputEvent::IE_Pressed, this, &AHN_Player::Interact);
}

void AHN_Player::Scamp_Shake()
{
	auto PlayerController = Cast<AHN_PlayerController>(Controller);

	DisableInput(PlayerController);

	//	화면이 6초동안 흔들리며 
	if (MyShake != nullptr)
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(MyShake, 1.0f);

	////플레이어A의 위치가 (플레이어A의 의지와 무관하게) 플레이어B에게로 다가간다.
	//UWorld* CurrentWorld = GetWorld();

	//for (TActorIterator<AHN_Player> Iter(CurrentWorld); Iter; ++Iter)
	//{
	//	//	***최적화 필요***
	//	//	다른 플레이어를 찾아서
	//	if (GetName() != (*Iter)->GetName())
	//	{
	//		auto otherPlayer = Cast<AHN_Player>(*Iter);
	//		ERROR_CHECK(otherPlayer == nullptr);

	//		//	이동
	//		UAIBlueprintHelperLibrary::SimpleMoveToLocation(GetController(), otherPlayer->GetActorLocation());
	//	}
	//}
}

void AHN_Player::Scamp_ShakeEnd()
{
	auto PlayerController = Cast<AHN_PlayerController>(Controller);

	EnableInput(PlayerController);
}

void AHN_Player::PlayWalkSound(float fValue)
{
	if (fValue != 0.f)
	{
		if (!Speaker->IsPlaying())
		{
			Speaker->SetPitchMultiplier(30.f);
			Speaker->Play();
		}
	}
	else
	{
		Speaker->Stop();
	}
}

void AHN_Player::SetEnablePlayerController_Implementation(bool bCheck)
{
	if (bCheck)
	{
		EnableInput(Cast<APlayerController>(this));
		IsPlayerController = true;
		//EnableInput(HNPlayerController);
	}
	else
	{
		DisableInput(Cast<APlayerController>(this));
		IsPlayerController = false;
		//DisableInput(HNPlayerController);
	}
}

void AHN_Player::SetPlayerInteract_Implementation(bool bCheck)
{
	IsInteractObject = bCheck;
}

FGenericTeamId AHN_Player::GetGenericTeamId() const
{
	return TeamId;
}

void AHN_Player::UpDown(float NewAxisValue)
{
	// 컨트롤 회전값으로부터 회전 행렬을 생성 후, 원하는 방향 축을 대입하여 캐릭터가 움직일 방향값을 가져온다.
	if (IsVRMode)
	{
		if (IsPlayerController)
		{
			if (FMath::IsNearlyEqual(NewAxisValue, 0.f))
				return;

			AddMovementInput(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::X), NewAxisValue);

			/*FRotator TargetRot = FRotationMatrix::MakeFromX(VRCamera->GetForwardVector()).Rotator();
			SetActorRotation(TargetRot);*/
			//FRotator Rotator = FMath::RInterpTo(GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 90.f);

			FRotator PlayerRotator = GetActorRotation();
			PlayerRotator.Yaw = VRCamera->GetComponentRotation().Yaw;
			SetActorRotation(PlayerRotator);			
				
			///*FRotator PlayerRotator = GetActorRotation();
			//PlayerRotator.Yaw = Rotator.Yaw;
			//SetActorRotation(PlayerRotator);*/
		}
		//AddMovementInput(Camera->GetForwardVector(), NewAxisValue);
	}
	else
	{
		if (IsPlayerController)
		{
			FRotator TargetRot = FRotationMatrix::MakeFromX(VRCamera->GetForwardVector()).Rotator();
			FRotator Rotator = FMath::RInterpTo(GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 2.f);
			AddMovementInput(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::X), NewAxisValue);
		}
	}


	//// 움직인 만큼 X방향 변경
	//DirectionToMove.X = NewAxisValue;
}

void AHN_Player::LeftRight(float NewAxisValue)
{
	// 컨트롤 회전값으로부터 회전 행렬을 생성 후, 원하는 방향 축을 대입하여 캐릭터가 움직일 방향값을 가져온다.
	if (IsPlayerController)
		AddMovementInput(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::Y), NewAxisValue);
	//AddMovementInput(Camera->GetRightVector(), NewAxisValue);
}

void AHN_Player::LookUp(float NewAxisValue)
{
	if (IsPlayerController)
		AddControllerPitchInput(NewAxisValue); // 상하 ( 마우스 Y )
}

bool AHN_Player::SetActorRotationServer_Validate(FRotator bRotation)
{
	return true;
}


void AHN_Player::SetActorRotationServer_Implementation(FRotator bRotation) {
	SetActorRotation(bRotation);
}

void AHN_Player::SetActorRotationMulticast_Implementation(FRotator bRotation) {
	SetActorRotation(bRotation);
}

void AHN_Player::Turn(float NewAxisValue)
{
	if (IsPlayerController)
	{
		if (IsVRMode)
		{
			if (FMath::IsNearlyEqual(NewAxisValue, 0.f))
				return;

			AddControllerYawInput(NewAxisValue);  // 좌우 ( 마우스 X )

			FRotator TargetRot = FRotationMatrix::MakeFromX(VRCamera->GetForwardVector()).Rotator();
			TargetRot.Pitch = GetActorRotation().Pitch;
			TargetRot.Pitch = GetActorRotation().Pitch;

			FRotator Rotator = FMath::RInterpTo(GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 90.f);
			//SetActorRotation(Rotator);
			SetActorRotationServer(Rotator);
			if(HasAuthority() == false)
				SetActorRotationMulticast(Rotator);
			//SetActorRotationMulticast(Rotator);
		/*	FRotator TargetRotator = UKismetMathLibrary::FindLookAtRotation(VRCamera->GetComponentLocation(), GetActorLocation());
			float fPitch, fYaw, fRoll;
			UKismetMathLibrary::BreakRotator(TargetRotator, fPitch, fYaw, fRoll);
			FRotator TargetBreakRotator = FRotator(0.f, fYaw, 0.f);

			FRotator Rotator = FMath::RInterpTo(GetActorRotation(), TargetBreakRotator, GetWorld()->GetDeltaSeconds(), 5.f);
			SetActorRotation(Rotator);*/
			
		}
		else
		{
			AddControllerYawInput(NewAxisValue);  // 좌우 ( 마우스 X )
		}
		
	}
}

void AHN_Player::DeleFunc_SetPosition(FVector originPos)
{
	SetActorLocation(originPos);
}

void AHN_Player::RotationController_Implementation(const FRotator& Rotator)
{
	
	SetActorRotation(Rotator);
	//SetActorRotationMulticast(Rotator);//이걸 굳이해야할까?
}

void AHN_Player::RotationCharacter_Implementation(const FRotator& Rotator)
{
	if (GetController()) 
		GetController()->SetControlRotation(Rotator);
}
void AHN_Player::Interact()
{
	if (!IsInteractObject)
		return;

	// 엑터의 충돌이 탐지된 경우 충돌된 엑터에 관련된 정보를 얻기위한 구조체
	FHitResult HitResult;

	// 탐색 방법 설정, 자신은 감지되지 않도록 this 무시
	FCollisionQueryParams Params(FName("Interaction"), false, this);

	float fDistance = 80.f;	// 탐지 End 범위
	float fRadius = 20.f;	// 탐지 도형 반지름
	FVector vPos = GetActorLocation();
	vPos.Z = vPos.Z + 20.f;

	bool bResult = GetWorld()->SweepSingleByObjectType(
		HitResult, vPos, vPos + GetActorForwardVector() * fDistance,
		FQuat::Identity, FCollisionObjectQueryParams(FCollisionObjectQueryParams::AllObjects),
		FCollisionShape::MakeSphere(fRadius), Params);

	if (!bResult)
		return;

	// 상호작용할 Actor가 존재하는지 검사
	if (HitResult.Actor.IsValid())
	{
		// 해당 Actor가 Interface를 가지고 있는지 검사
		if (HitResult.Actor->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
		{
			if (IInteractable::Execute_CanInteract(HitResult.Actor.Get()))
			{
				IInteractable::Execute_PerformInteract(HitResult.Actor.Get());
			}
		}
	}
	/*
	#if ENABLE_DRAW_DEBUG
		FVector TraceVec = GetActorForwardVector() * fDistance;					// 탐지 길이
		FVector Center = vPos + TraceVec * 0.5f;					// 길이의 중점
		float HalfHeight = fDistance * 0.5f + fRadius;
		FQuat CapsuleRot = FRotationMatrix::MakeFromZ(TraceVec).ToQuat();		// 캡슐을 캐릭터 시선 방향으로 눕는다는 것
		FColor DrawColor = bResult ? FColor::Green : FColor::Red;				// 부딪혔으면 그린, 안부딪혔으면 레드
		float DebugLifeTime = 5.f;

		// 캡슐 모양을 그리는 기능
		DrawDebugCapsule(GetWorld(), Center, HalfHeight, fRadius, CapsuleRot, DrawColor, false, DebugLifeTime);

	#endif
	*/
}

void AHN_Player::SetCameraActivate(bool bCheck)
{
	IsVRMode = bCheck;

	if (IsVRMode)
	{
		VRCamera->SetActive(true);
		PCCamera->SetActive(false);
		// 캐릭터 무브먼트 설정
		bUseControllerRotationYaw = false;
		GetCharacterMovement()->bOrientRotationToMovement = false;
		GetCharacterMovement()->bUseControllerDesiredRotation = false;
	}
	else
	{
		PCCamera->SetActive(true);
		VRCamera->SetActive(false);
		// 캐릭터 무브먼트 설정
		bUseControllerRotationYaw = true;
		GetCharacterMovement()->bOrientRotationToMovement = true;
		GetCharacterMovement()->bUseControllerDesiredRotation = true;
	}
}

void AHN_Player::SetItem(HAND_TYPE _hand, AHN_Item* _item)
{
	//	 손에 이미 아이템이 있으면 못줍게
	if (Inventory[(int)_hand] != NULL)
	{
		return;
	}

	if (HAND_TYPE::RIGHT == _hand)
	{
		_item->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("RHandSocket"));
	}
	else
	{
		_item->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("LHandSocket"));
	}

	Inventory[(int)_hand] = _item;
}

bool AHN_Player::UseItem(FName _name)
{
	for (int i = 0; i < (int)HAND_TYPE::END; ++i)
	{
		if (_name == TEXT("Ring"))
		{
			if ((Inventory[i] != nullptr) && Inventory[i]->eType == ITEM_TYPE::RING)
			{
				Inventory[i]->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
				Inventory[i]->Destroy();
				Inventory[i] = nullptr;
				return true;
			}
		}
		else if (_name == TEXT("Key"))
		{
			if ((Inventory[i] != nullptr) && Inventory[i]->eType == ITEM_TYPE::KEY)
			{
				Inventory[i]->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
				Inventory[i]->Destroy();
				Inventory[i] = nullptr;
				return true;
			}
		}
	}
	return false;
}

bool AHN_Player::CheckItem(FName _name)
{
	for (int i = 0; i < (int)HAND_TYPE::END; ++i)
	{
		if (_name == TEXT("Ring"))
		{
			if ((Inventory[i] != nullptr) && (Inventory[i]->eType == ITEM_TYPE::RING))
			{
				return true;
			}
		}
		else if (_name == TEXT("Key"))
		{
			if ((Inventory[i] != nullptr) && (Inventory[i]->eType == ITEM_TYPE::KEY))
			{
				return true;
			}
		}
	}
	return false;
}

void AHN_Player::Damage_Implementation(int _damage)
{
	if (IsInvincibility)
		return;

	IsInvincibility = true;
	iHp -= _damage;

	//print("_damage");

	PlayDamageSound();

	HpEffect();
}

void AHN_Player::Heal_Implementation(int _heal)
{
	if (iHp == iMaxHp)
		return;

	if ((iHp + _heal) >= iMaxHp)
		iHp = iMaxHp;
	else
		iHp += _heal;

	HpEffect();
}

void AHN_Player::HpEffect()
{
	//	효과를 다시 세팅할건지 체크하는 부분
	PLAYER_STATE NewState = PLAYER_STATE::END;

	//LOG_TEXT(Warning, TEXT("%d"), iHp);

	if (40 < iHp)
	{
		NewState = PLAYER_STATE::CLAM;
	}
	else if (30 < iHp)
	{
		NewState = PLAYER_STATE::SCARY;
	}
	else if (20 < iHp)
	{
		NewState = PLAYER_STATE::FEAR;
	}
	else if (10 < iHp)
	{
		NewState = PLAYER_STATE::TERRIBLE;
	}
	else if (0 < iHp)
	{
		NewState = PLAYER_STATE::BLOODY;
	}
	else
	{
		NewState = PLAYER_STATE::PANIC;
		CallDeadDelegateFunc();
	}

	//	이전 상태와 같으면 리턴
	if (NewState == eState)
		return;

	if (eState == PLAYER_STATE::BLOODY && NewState == PLAYER_STATE::TERRIBLE)
	{
		//	피 효과 지워야함
		bChange = false;
	}

	//	새로우면 상태를 바꿈
	eState = NewState;

	if (PLAYER_STATE::CLAM == eState)
	{
		EffectSpeaker->Stop();
	}
	else if (PLAYER_STATE::SCARY == eState)
	{
		PlayScarySound();
	}
	else if (PLAYER_STATE::FEAR == eState)
	{
		PlayFearSound();
	}
	else if (PLAYER_STATE::TERRIBLE == eState)
	{
		PlayTerribleSound();
	}
	else if (PLAYER_STATE::BLOODY == eState)
	{
		bChange = false;
	}
	else if (PLAYER_STATE::PANIC == eState)
	{
		bChange = false;
	}
}

void AHN_Player::BloodyEffect()
{
	if (IsNightVision)
		return;

	if (FMath::IsNearlyEqual(fGamma, 0.f, 0.000001f))
	{
		//pNormalVision->AddBlendable(pBlurMtrl, 1);
		if (IsVRMode)
			VRCamera->PostProcessSettings.AddBlendable(pBlurMtrl, 1);
		else
			PCCamera->PostProcessSettings.AddBlendable(pBlurMtrl, 1);

		bChange = true;
		return;
	}

	//	서서히 빨개짐
	//fGamma -= UGameplayStatics::GetWorldDeltaSeconds(GetWorld());
	fGamma -= 0.01f;
//	print("Bloody");

	//pNormalVision->ColorGamma = FVector(1.f, fGamma, fGamma);
	if (IsVRMode)
		VRCamera->PostProcessSettings.ColorGamma = FVector(1.f, fGamma, fGamma);
	else
		PCCamera->PostProcessSettings.ColorGamma = FVector(1.f, fGamma, fGamma);
}

void AHN_Player::RemoveBloodyEffect()
{
	if (IsNightVision)
		return;

	if (FMath::IsNearlyEqual(fGamma, 1.f, 0.000001f))
	{
		//pNormalVision->AddBlendable(pBlurMtrl, 0);
		if (IsVRMode)
			VRCamera->PostProcessSettings.AddBlendable(pBlurMtrl, 0);
		else
			PCCamera->PostProcessSettings.AddBlendable(pBlurMtrl, 0);

		bChange = true;
		return;
	}

	//	서서히 되돌아옴
	//fGamma += UGameplayStatics::GetWorldDeltaSeconds(GetWorld());
//	print("Remove");
	fGamma += 0.01f;
	//pNormalVision->ColorGamma = FVector(1.f, fGamma, fGamma);
	if (IsVRMode)
		VRCamera->PostProcessSettings.ColorGamma = FVector(1.f, fGamma, fGamma);
	else
		PCCamera->PostProcessSettings.ColorGamma = FVector(1.f, fGamma, fGamma);
}

void AHN_Player::PlayScarySound()
{
	EffectSpeaker->Stop();
	EffectSpeaker->SetSound(ScarySoundCue);
	EffectSpeaker->Play();
}

void AHN_Player::PlayFearSound()
{
	EffectSpeaker->Stop();
	EffectSpeaker->SetSound(FearSoundCue);
	EffectSpeaker->Play();
}

void AHN_Player::PlayTerribleSound()
{
	EffectSpeaker->Stop();
	EffectSpeaker->SetSound(TerribleSoundCue);
	EffectSpeaker->Play();
}

void AHN_Player::PlayDamageSound()
{
	DamageSpeaker->Stop();
	DamageSpeaker->SetSound(DamageSoundCue);
	DamageSpeaker->Play();
}

void AHN_Player::ChangeNightVision()
{
	if (IsNightVision)
	{
		//	나이트비전 끄기
		if (IsVRMode)
			VRCamera->PostProcessSettings = *pNormalVision;
		else
			PCCamera->PostProcessSettings = *pNormalVision;
		IsNightVision = false;
	}
	else
	{
		//	나이트비전 켜기
		//	카메라가 가지고 있던 값 저장
		if (IsVRMode)
		{
			*pNormalVision = VRCamera->PostProcessSettings;
			VRCamera->PostProcessSettings = *pNightVision;	//	카메라에 나이트비전 세팅
		}
		else
		{
			*pNormalVision = PCCamera->PostProcessSettings;
			PCCamera->PostProcessSettings = *pNightVision;	//	카메라에 나이트비전 세팅
		}

		IsNightVision = true;
	}
}

void AHN_Player::CallDeadDelegateFunc()
{
	Fuc_DeleSingle.Broadcast();
	Fuc_DeleSingle.Clear();
}
