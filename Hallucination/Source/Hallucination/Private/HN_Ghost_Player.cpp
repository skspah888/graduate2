// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Player.h"
#include "HN_DissolveComponent.h"
#include "Sound/SoundCue.h"

// Sets default values
AHN_Ghost_Player::AHN_Ghost_Player()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// AI 설정
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	// 컴포넌트 초기화
	GhostRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	DissolveComponent = CreateDefaultSubobject<UHN_DissolveComponent>(TEXT("DissolveComponent"));


	// 스켈레탈 메시
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_PLAYER(TEXT(
		"SkeletalMesh'/Game/Asset/NewAsset/4F_Model/Player/Player1/Player1.Player1'"));
	if (SK_PLAYER.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_PLAYER.Object);
	}

	RootComponent = GetCapsuleComponent();
	GetMesh()->SetupAttachment(RootComponent);

	// 캡슐 컴포넌트
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("HNCharacter"));
	GetMesh()->SetRelativeScale3D(FVector(1.5f, 1.5f, 1.5f));
	GetMesh()->SetRelativeLocationAndRotation(FVector(0, 0, -90.f), FRotator(0.f, -90.f, 0.f));

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundCue> SoundCueObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/PlayerGhost/PlayerGhost_Cue.PlayerGhost_Cue'"));
	if (SoundCueObject.Succeeded())
	{
		GhostSoundCue = SoundCueObject.Object;
		Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("GhostAudioComponent"));
		Speaker->SetupAttachment(RootComponent);

		// 쉐이더 효과 넣고 싶으면 이쪽으로?
		//Speaker->OnAudioFinished.AddDynamic(this, &AHN_Ghost_Flower::OnAudioEnd);
	}

	IsDissolve = false;
	IsDestroy = false;
}

// Called when the game starts or when spawned
void AHN_Ghost_Player::BeginPlay()
{
	Super::BeginPlay();

	// 사운드 셋팅
	if (GhostSoundCue&& Speaker)
	{
		Speaker->SetSound(GhostSoundCue);
	}
}

// Called every frame
void AHN_Ghost_Player::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	IsDissolve = DissolveComponent->FinishDissolve;

	if (IsDissolve && !IsDestroy)
	{
		Destroy(1.f);
		IsDestroy = true;
	}
}

void AHN_Ghost_Player::DissolveGhost_Implementation()
{
	Speaker->Play();
	DissolveComponent->DoDissolve();
}

