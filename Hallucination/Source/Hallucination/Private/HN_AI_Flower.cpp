// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AI_Flower.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

const FName AHN_AI_Flower::IsSoundKey(TEXT("IsSound"));
const FName AHN_AI_Flower::DelayTimeKey(TEXT("DelayTime"));
const FName AHN_AI_Flower::CurrentTimeKey(TEXT("CurrentTime"));

AHN_AI_Flower::AHN_AI_Flower()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT(
		"BlackboardData'/Game/AI/3F/BB_3F.BB_3F'"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;		// 블랙보드 셋팅
	}

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT(
		"BehaviorTree'/Game/AI/3F/BT_3F_Flower.BT_3F_Flower'"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;		// 비헤이비어트리 셋팅
	}
}

void AHN_AI_Flower::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	// 블랙보드 사용
	UseBlackboard(BBAsset, Blackboard);
	// 비헤이비어 트리 사용
	RunBehaviorTree(BTAsset);
}

void AHN_AI_Flower::OnUnPossess()
{
	Super::OnUnPossess();
}

bool AHN_AI_Flower::SoundCheck_Validate(bool bCheck)
{
	return true;
}


void AHN_AI_Flower::SoundCheck_Implementation(bool bCheck)
{
	GetBlackboardComponent()->SetValueAsBool(IsSoundKey, bCheck);
}

void AHN_AI_Flower::SetFlowerTime()
{
	float RandTime = FMath::RandRange(3.f, 5.f);
	GetBlackboardComponent()->SetValueAsFloat(DelayTimeKey, RandTime);
	GetBlackboardComponent()->SetValueAsFloat(CurrentTimeKey, 0.f);
}

