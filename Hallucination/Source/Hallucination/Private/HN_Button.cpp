// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Button.h"

// Sets default values
AHN_Button::AHN_Button()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	ButtonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BUTTON"));
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_BUTTONBASE(TEXT("StaticMesh'/Game/HorrorEngine/Meshes/ButtonBase.ButtonBase'"));

	if (SM_BUTTONBASE.Succeeded())
	{
		Mesh->SetStaticMesh(SM_BUTTONBASE.Object);
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_BUTTON(TEXT("StaticMesh'/Game/HorrorEngine/Meshes/Button.Button'"));

	if (SM_BUTTON.Succeeded())
	{
		ButtonMesh->SetStaticMesh(SM_BUTTON.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_ON(TEXT("SoundCue'/Game/HorrorEngine/Audio/Button/ButtonOn_Cue.ButtonOn_Cue'"));

	if (SOUND_ON.Succeeded())
	{
		ButtonOnSound = SOUND_ON.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_OFF(TEXT("SoundCue'/Game/HorrorEngine/Audio/Button/ButtonOff_Cue.ButtonOff_Cue'"));

	if (SOUND_OFF.Succeeded())
	{
		ButtonOffSound = SOUND_OFF.Object;
	}

	//static ConstructorHelpers::FObjectFinder<USoundAttenuat> SOUND_ATT(TEXT("SoundAttenuation'/Game/HorrorEngine/Audio/_ATT_Profiles/ATT_General.ATT_General'"));

	//if (SOUND_ATT.Succeeded())
	//{
	//	ATT = SOUND_ATT.Object;
	//}

	ButtonMesh->SetupAttachment(RootComponent);
	ButtonMesh->SetRelativeLocation(FVector(0.f, 0.8f, 5.0f));

	ButtonMesh->ComponentTags.Add(TEXT("outline"));

	SetActorTickEnabled(false);
}

// Called when the game starts or when spawned
void AHN_Button::BeginPlay()
{
	Super::BeginPlay();

	UMaterialInstanceDynamic* pInstance = ButtonMesh->CreateDynamicMaterialInstance(0, ButtonMesh->GetMaterial(0));
	pInstance->SetScalarParameterValue(TEXT("Emissive"), 1.f);
}

// Called every frame
void AHN_Button::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHN_Button::ActiveOn_Implementation()
{
//	Super::On_Implementation();

	bActive = true;

	UMaterialInstanceDynamic* pInstance = ButtonMesh->CreateDynamicMaterialInstance(0, ButtonMesh->GetMaterial(0));
	pInstance->SetScalarParameterValue(TEXT("Emissive"), 0.f);

	ButtonMesh->SetRelativeLocation(FVector(0.f, 0.5f, 5.0f));

	Speaker->Stop();
	Speaker->SetSound(ButtonOnSound);
	Speaker->Play();

	return;
}

void AHN_Button::ActiveOff_Implementation()
{
//	Super::Off_Implementation();

	bActive = false;


	UMaterialInstanceDynamic* pInstance = ButtonMesh->CreateDynamicMaterialInstance(0, ButtonMesh->GetMaterial(0));
	pInstance->SetScalarParameterValue(TEXT("Emissive"), 1.f);

	ButtonMesh->SetRelativeLocation(FVector(0.f, 0.8f, 5.0f));

	Speaker->Stop();
	Speaker->SetSound(ButtonOffSound);
	Speaker->Play();

	return;
}
