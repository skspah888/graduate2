// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AnimInst_Flower.h"


UHN_AnimInst_Flower::UHN_AnimInst_Flower()
{
	eState = EFlowerState::BACK;
}

void UHN_AnimInst_Flower::SetState(EFlowerState NextState)
{
	eState = NextState;
}

uint32 UHN_AnimInst_Flower::GetState()
{
	return (uint32)eState;
}
