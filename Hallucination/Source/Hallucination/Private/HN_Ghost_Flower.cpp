// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Flower.h"
#include "Engine/TargetPoint.h"
#include "EngineUtils.h"
#include "HN_Player.h"
#include "HN_AI_Flower.h"
#include "Sound/SoundCue.h"
#include "HN_FlowerLight.h"
#include "HN_AnimInst_Flower.h"
#include "HN_DissolveComponent.h"


AHN_Ghost_Flower::AHN_Ghost_Flower()
{
	PrimaryActorTick.bCanEverTick = false;

	// AI 설정
	AIControllerClass = AHN_AI_Flower::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	// 메시 설정
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_GHOST(TEXT(
		"SkeletalMesh'/Game/Characters/Flower/GhostFlower.GhostFlower'"));
	if (SK_GHOST.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_GHOST.Object);
	}

	// 애니메이션 설정
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> ANIM(TEXT(
		"AnimBlueprint'/Game/Characters/Flower/Flower_AnimBP.Flower_AnimBP_C'"));

	
	if (ANIM.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(ANIM.Class);
	}


	// 캐릭터 조정
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -97.f), FRotator(0.f, 90.f, 0.f));
	GetMesh()->SetRelativeScale3D(FVector(0.02f, 0.02f, 0.02f));

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundAttenuation> SoundAttenuation(TEXT(
		"SoundAttenuation'/Game/Audio/SoundAttenuation/ATT_FlowerGhost.ATT_FlowerGhost'"));

	static ConstructorHelpers::FObjectFinder<USoundCue> FlowerSoundCueObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Flower_Cue.Flower_Cue'"));

	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("FlowerAudioComponent"));
	Speaker->SetupAttachment(RootComponent);

	FlowerSpeaker = CreateDefaultSubobject<UAudioComponent>(TEXT("FlowerAudioComponent2"));
	FlowerSpeaker->SetupAttachment(RootComponent);

	if (SoundAttenuation.Succeeded())
	{
		Speaker->AttenuationSettings = SoundAttenuation.Object;
		FlowerSpeaker->AttenuationSettings = SoundAttenuation.Object;
	}

	if (FlowerSoundCueObject.Succeeded())
	{
		FlowerSoundCue = FlowerSoundCueObject.Object;
		Speaker->OnAudioFinished.AddDynamic(this, &AHN_Ghost_Flower::OnAudioEnd);
	}

	// 트리거 캡슐
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(120.f, 180.f);
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);
	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &AHN_Ghost_Flower::OnOverlapBegin);

	// 디졸브
	DissolveComponent = CreateDefaultSubobject<UHN_DissolveComponent>(TEXT("DissolveComponent"));
	DissolveComponent->DissolveColor = FLinearColor::Red;

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundCue> DeadObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Flower/FlowerDead_Cue.FlowerDead_Cue'"));
	if (DeadObject.Succeeded())
	{
		DeadSoundCue = DeadObject.Object;
	}

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundCue> LaughObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Flower/FlowerLaugh_Cue.FlowerLaugh_Cue'"));
	if (LaughObject.Succeeded())
	{
		LaughSoundCue = LaughObject.Object;
	}
}

bool AHN_Ghost_Flower::OnOverlapBegin_Validate(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	return true;
}

void AHN_Ghost_Flower::FlowerClearMotion_Implementation(){
	Speaker->OnAudioFinished.Clear();
	DissolveComponent->DoDissolve();
	Speaker->SetSound(DeadSoundCue);
}

void AHN_Ghost_Flower::OnOverlapBegin_Implementation(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		if (Cast<AHN_Player>(OtherActor))
		{
			// 디졸브효과, 사운드 체인지, 컨트롤러 끊기, 오디오 끝나면 Destroy
			//Speaker->OnAudioFinished.Clear();
			//DissolveComponent->DoDissolve();
			//Speaker->SetSound(GhostSoundCue);
			FlowerClearMotion(); //위의 3줄 멀티캐스트 처리
			Cast<AHN_AI_Flower>(GetController())->UnPossess();
			TriggerCapsule->SetCollisionProfileName(TEXT("NoCollision"));
			//TriggerCapsule->FinishDestroy();
			GetWorldTimerManager().SetTimer(TimerHandle, this, &AHN_Ghost_Flower::DestoryGhost, 3.f, false);
		}
	}
}

void AHN_Ghost_Flower::BeginPlay()
{
	Super::BeginPlay();

	// 플레이어 셋팅
	UWorld* CurrentWorld = GetWorld();
	for (TActorIterator<AHN_Player> Iter(CurrentWorld); Iter; ++Iter)
	{
		//print(*Iter->GetName());
		PlayerArray.Add(*Iter);
		OriginPositionArray.Add((*Iter)->GetActorLocation());
	}

	// 라이트 셋팅
	for (TActorIterator<AHN_FlowerLight> Iter(CurrentWorld); Iter; ++Iter)
	{
		LightArray.Add(*Iter);
		(*Iter)->SetCustomLightVisibility();
	}

	// 사운드 셋팅
	if (FlowerSoundCue && Speaker)
	{
		Speaker->SetSound(FlowerSoundCue);
		ChangeFrontAnim();
		StartSound();
		FlowerSpeaker->SetSound(LaughSoundCue);
	}
}

void AHN_Ghost_Flower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ghost_Flower::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);
}

void AHN_Ghost_Flower::SetOriginPosition(FVector originPos)
{
	//// OriginPos 셋팅
	//int dir = 1;
	//print("asd2");
	//for (int i = 0; i < OriginPositionArray.Num(); ++i)
	//{
	//	print("asd3");
	//	FVector PlayerPosition = OriginPositionArray[i];
	//	PlayerPosition.X = originPos.X;
	//	PlayerPosition.Y = originPos.Y;

	//	float fRandom = FMath::RandRange(30.f, 100.f);
	//	PlayerPosition.Y = PlayerPosition.Y + dir * fRandom;
	//	dir = -dir;

	//	OriginPositionArray[i] = PlayerPosition;
	//}
	
}

void AHN_Ghost_Flower::PlaySoundFlower_Implementation()
{
	Speaker->SetPitchMultiplier(FMath::RandRange(1.f, 3.f));
	Speaker->Play();
}

bool AHN_Ghost_Flower::StartSound_Validate()
{
	return true;
}


void AHN_Ghost_Flower::StartSound_Implementation()
{
	ChangeFrontAnim();
	// 사운드 재생 후 IsSound true 변경
	//Speaker->SetPitchMultiplier(FMath::RandRange(1.f, 3.f));
	//Speaker->Play();
	PlaySoundFlower();
	auto FlowerController = Cast<AHN_AI_Flower>(Controller);
	FlowerController->SoundCheck(true);
}

bool AHN_Ghost_Flower::OnAudioEnd_Validate()
{
	return true;
}


void AHN_Ghost_Flower::OnAudioEnd_Implementation()
{
	// 사운드가 끝났음 => IsSound  false 변경하기
	auto FlowerController = Cast<AHN_AI_Flower>(Controller);
	FlowerController->SoundCheck(false);
	ChangeFrontAnim();
	/*
	// 사운드가 끝났으니 뒤->앞으로 돌아가야함
	auto GhostAnim = Cast<UHN_AnimInst_Flower>(GetMesh()->GetAnimInstance());
	if (GhostAnim)
	{
		GhostAnim->SetState(EFlowerState::BLEND);
	}
	*/
}
void AHN_Ghost_Flower::ChangeBackAnim_Implementation() {
	
}

void AHN_Ghost_Flower::ChangeFrontAnim_Implementation() {
	// 사운드가 끝났으니 뒤->앞으로 돌아가야함
	auto GhostAnim = Cast<UHN_AnimInst_Flower>(GetMesh()->GetAnimInstance());
	if (GhostAnim)
	{
		GhostAnim->SetState(EFlowerState::BLEND);
	}
}

void AHN_Ghost_Flower::CheckPlayer()//_Implementation()
{
	// 플레이어 움직임 검사
	for (int i = 0; i < PlayerArray.Num(); ++i)
	{
		FVector vecVelocity = PlayerArray[i]->GetVelocity();
		vecVelocity.Z = 0.f;
		float Length = vecVelocity.Size();

		if (Length != 0.f)
		{
			// 플레이어에 지정된 이벤트 실행하기
			// 파라미터로는 i번째에 있는 originpositon
			PlayerArray[i]->Fuc_DeleSingle_OneParam.Execute(OriginPositionArray[i]);
			PlayerArray[i]->Damage(10);
			//print("Player Damage 10");

			if(!FlowerSpeaker->IsPlaying())
				FlowerSpeaker->Play();
		}
	}
}

void AHN_Ghost_Flower::ChangeLight_Implementation(bool IsChange)
{
	for (auto light : LightArray)
	{
		light->SetCustomLightColor(IsChange);
	}
}

void AHN_Ghost_Flower::DestoryGhost()
{
	Destroy();
}
