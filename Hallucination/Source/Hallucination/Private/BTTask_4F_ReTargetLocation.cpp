// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_ReTargetLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_AI_Drag.h"
#include "HN_Ghost_Drag.h"

UBTTask_4F_ReTargetLocation::UBTTask_4F_ReTargetLocation()
{
	NodeName = TEXT("Re Target Location");

}

EBTNodeResult::Type UBTTask_4F_ReTargetLocation::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	// BlackBoard에 Player가 이동할 다음 목표지점 Set
	auto GhostDrag = Cast<AHN_Ghost_Drag>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == GhostDrag)
		return EBTNodeResult::Failed;

	bool IsEnd = GhostDrag->Set_ReTargetLocation();

	if (IsEnd)
	{
		// 이미 방에 도착 -> IsMove를 True로 변경
		OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Drag::IsMoveKey, true);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Succeeded;
}
