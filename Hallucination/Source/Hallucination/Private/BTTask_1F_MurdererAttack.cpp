// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_1F_MurdererAttack.h"
#include "HN_Ghost_Murderer.h"
#include "HN_AI_Murderer.h"
#include "HN_AnimInst_Murderer.h"

UBTTask_1F_MurdererAttack::UBTTask_1F_MurdererAttack()
{
	// Tick 기능 활성화
	bNotifyTick = true;
	IsAttacking = false;
}

EBTNodeResult::Type UBTTask_1F_MurdererAttack::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto ControllingPawn = Cast<AHN_Ghost_Murderer>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;

	// 공격 명령 실행
	ControllingPawn->MurdererAttack();
	IsAttacking = true;

	// 캐릭터 공격모션이 끝나면 호출될 함수 등록
	ControllingPawn->OnAttackEnd.AddLambda([this]()->void
	{
		IsAttacking = false;
	});

	return EBTNodeResult::InProgress;
}

void UBTTask_1F_MurdererAttack::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);
	if (!IsAttacking)
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
