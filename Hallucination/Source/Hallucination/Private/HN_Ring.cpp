// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ring.h"
#include "HN_Player.h"

AHN_Ring::AHN_Ring()
{
	//Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));
	//Speaker->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_RING(TEXT("StaticMesh'/Game/Asset/NewAsset/Ring.Ring'"));

	if (SM_RING.Succeeded())
	{
		Mesh->SetStaticMesh(SM_RING.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> RING_SOUND(TEXT("SoundCue'/Game/Audio/SoundCue/Key_Sound_Cue.Key_Sound_Cue'"));

	if (RING_SOUND.Succeeded())
	{
		RingSound = RING_SOUND.Object;
	}

	eType = ITEM_TYPE::RING;

	Mesh->ComponentTags.Add(TEXT("outline"));
	Mesh->SetWorldScale3D(FVector(0.5f, 0.5f, 0.5f));

	SetActorTickEnabled(false);
}

void AHN_Ring::BeginPlay()
{
	Super::BeginPlay();
}

void AHN_Ring::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ring::GetItem_Implementation(AHN_Player* _player)
{
	//	잡은 손 방향에 따라 타입을 다르게 주면 됨
	_player->SetItem(HAND_TYPE::LEFT, this);
//
//	//	오브젝트 비활성화
//	SetActorHiddenInGame(true);
//	SetActorEnableCollision(false);
//	SetActorTickEnabled(false);
	Speaker->SetSound(RingSound);
	Speaker->Play();

}
