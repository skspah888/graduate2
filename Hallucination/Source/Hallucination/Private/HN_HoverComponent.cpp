// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_HoverComponent.h"

// Sets default values for this component's properties
UHN_HoverComponent::UHN_HoverComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	TraceLength = 150.f;
	HoverForce = 500000.f;
	LinearDamping = 0.6f;
	AngularDamping = 0.6f;
}


// Called when the game starts
void UHN_HoverComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	TArray<UActorComponent*> PrimitiveComponents = GetOwner()->GetComponentsByClass(UPrimitiveComponent::StaticClass());
	if (PrimitiveComponents.Num() <= 0)
	{
		PrimitiveComponent = nullptr;
		return;
	}

	PrimitiveComponent = Cast<UPrimitiveComponent>(PrimitiveComponents[0]);
	PrimitiveComponent->SetSimulatePhysics(true);
}


// Called every frame
void UHN_HoverComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PrimitiveComponent)
		return;

	FVector StartLocation =  GetComponentLocation();
	FVector EndLocation = StartLocation - FVector(0.f,0.f,TraceLength);

	FHitResult Hit;
	FCollisionQueryParams TraceParams;		// 탐색 방법 설정, 자신은 감지되지 않도록 this 무시
	TraceParams.AddIgnoredActor(GetOwner());

	bool bResult = GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECC_Visibility, TraceParams);

	if (!bResult)
		return;

	float vecLength = (Hit.Location - GetComponentLocation()).Size();
	vecLength /= TraceLength;

	float LerpValue = FMath::Lerp(HoverForce, 0.f, vecLength);

	FVector vecForce = Hit.ImpactNormal * LerpValue;

	PrimitiveComponent->AddForce(vecForce);
	PrimitiveComponent->SetLinearDamping(LinearDamping);
	PrimitiveComponent->SetAngularDamping(AngularDamping);
}

