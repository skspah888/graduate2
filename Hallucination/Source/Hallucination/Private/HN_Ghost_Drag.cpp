// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Drag.h"
#include "Engine/TargetPoint.h"
#include "HN_4F_Room.h"
#include "HN_AI_Drag.h"
#include "EngineUtils.h"

AHN_Ghost_Drag::AHN_Ghost_Drag()
{
	PrimaryActorTick.bCanEverTick = true;

	//// ���̷�Ż �޽�
	//static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_WARRIOR(TEXT(
	//	"SkeletalMesh'/Game/TestAssets/SK_CharM_Warrior.SK_CharM_Warrior'"));
	//if (SK_WARRIOR.Succeeded())
	//{
	//	GetMesh()->SetSkeletalMesh(SK_WARRIOR.Object);
	//}

	// AI ����
	AIControllerClass = AHN_AI_Drag::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	TargetState = ROOM_TYPE::STATE_END;
}

void AHN_Ghost_Drag::BeginPlay()
{
	Super::BeginPlay();

	// Room Setting
	for (TActorIterator<AHN_4F_Room> Iter(GetWorld()); Iter; ++Iter)
	{
		if ((*Iter)->GetName() == FString(TEXT("DragRoom_A")))
		{
			Room_A = (*Iter);
			Room_A->SetGhost(this);
		}

		if ((*Iter)->GetName() == FString(TEXT("DragRoom_B")))
		{
			Room_B = (*Iter);
			Room_B->SetGhost(this);
		}
	}

	// TargetPoint Setting
	for (TActorIterator<ATargetPoint> Iter(GetWorld()); Iter; ++Iter)
	{
		if ((*Iter)->GetName() == FString(TEXT("PlayerLookPoint")))
		{
			PlayerLookPoint = (*Iter);
			break;
		}
	}
}

void AHN_Ghost_Drag::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ghost_Drag::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);
}

AHN_4F_Room * AHN_Ghost_Drag::Get_Room(int32 RoomNumber)
{
	if (RoomNumber == 0)
	{
		return Room_A;
	}
	else
	{
		return Room_B;
	}
}

ATargetPoint * AHN_Ghost_Drag::Get_LookPoint()
{
	if(nullptr == PlayerLookPoint)
		return nullptr;
	return PlayerLookPoint;
}

FVector AHN_Ghost_Drag::Get_TargetLocation()
{
	FVector TargetLocation = FVector::ZeroVector;
	if (Room_A->Get_TargetLocation(TargetLocation))
	{
		return TargetLocation;
	}
	else
	{
		Room_B->Get_TargetLocation(TargetLocation);
		return TargetLocation;
	}
}

FRotator AHN_Ghost_Drag::Get_TargetRotator()
{
	FRotator TargetRotator = FRotator::ZeroRotator;
	if (Room_A->Get_TargetRotator(TargetRotator))
	{
		return TargetRotator;
	}
	else
	{
		Room_B->Get_TargetRotator(TargetRotator);
		return TargetRotator;
	}
}

bool AHN_Ghost_Drag::Set_ReTargetLocation()
{
	if (Room_A->Set_ReTargetLocation())
	{
		return true;
	}

	if (Room_B->Set_ReTargetLocation())
	{
		return true;
	}

	return false;
}

void AHN_Ghost_Drag::Setting_TargetRoom(int32 RandomNumber)
{
	if (RandomNumber == 0)
	{
		Room_A->SetPlayer(true);
		Room_A->SetTargetDoor(true);
		TargetState = ROOM_TYPE::ROOMA;
	}
	else
	{
		Room_B->SetPlayer(true);
		Room_B->SetTargetDoor(true);
		TargetState = ROOM_TYPE::ROOMB;
	}
}

void AHN_Ghost_Drag::Setting_Door(bool IsOpen)
{
	Room_A->Activate_Door(IsOpen);
	Room_B->Activate_Door(IsOpen);
}

void AHN_Ghost_Drag::SetDragBlackBoard(FString BoardName, bool bCheck)
{
	if (GetController())
	{
		Cast<AHN_AI_Drag>(GetController())->SetDragBlackBoard(BoardName, bCheck);
	}
}

void AHN_Ghost_Drag::SetActorIgnore(AHN_Player * targetplayer, bool isIgnore)
{
	Room_A->SetActorIgnore(targetplayer, isIgnore);
	Room_B->SetActorIgnore(targetplayer, isIgnore);
}

void AHN_Ghost_Drag::DestroyDragGhost()
{
	Destroy();
}

void AHN_Ghost_Drag::ClearDrag()
{
	float TimerTime = 5.f;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AHN_Ghost_Drag::DestroyDragGhost, 5.f, false);

	Cast<AHN_AI_Drag>(GetController())->SetDragBlackBoard("Open", false);
	Cast<AHN_AI_Drag>(GetController())->SetDragBlackBoard("Contact", false);

	// Door �� ��� ����
	Room_A->SetTargetDoor(false);
	Room_B->SetTargetDoor(false);

}

void AHN_Ghost_Drag::SpawnPlayerGhost()
{
	// �ش� Ÿ�ٷ��� �ݴ����� �ͽ� ����
	if (TargetState == ROOM_TYPE::ROOMA)
	{
		Room_B->SpawnGhost();
	}
	else if (TargetState == ROOM_TYPE::ROOMB)
	{
		Room_A->SpawnGhost();
	}
}

void AHN_Ghost_Drag::PenaltyPlayerGhost()
{
	// �ش� Ÿ�ٷ��� �ݴ����� �ͽ� ����
	if (TargetState == ROOM_TYPE::ROOMA)
	{
		Room_B->PenaltyGhostPlayer();
		TargetState = ROOM_TYPE::STATE_END;
	}
	else if (TargetState == ROOM_TYPE::ROOMB)
	{
		Room_A->PenaltyGhostPlayer();
		TargetState = ROOM_TYPE::STATE_END;
	}
}
