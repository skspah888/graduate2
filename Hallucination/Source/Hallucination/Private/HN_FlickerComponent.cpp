// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_FlickerComponent.h"
#include "Kismet/KismetMaterialLibrary.h"

// Sets default values for this component's properties
UHN_FlickerComponent::UHN_FlickerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// Defalut Flicker ����
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialObject(TEXT(
		"MaterialInstanceConstant'/Game/Materials/Flicker/Flicker_Inst.Flicker_Inst'"));
	if (MaterialObject.Succeeded())
	{
		FlickerMaterial = MaterialObject.Object;
	}

	FlickerName = "Flicker Value";
	FlickerValue = 0.5f;
}


// Called when the game starts
void UHN_FlickerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	SetFlickerMaterial();
	SetFlickerValue();
}


// Called every frame
void UHN_FlickerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHN_FlickerComponent::SetFlickerMaterial()
{
	if (!FlickerMaterial)
		return;

	TArray<UActorComponent*> LightComponents = GetOwner()->GetComponentsByClass(ULightComponent::StaticClass());
	for (int i = 0; i < LightComponents.Num(); ++i)
	{
		UMaterialInstanceDynamic* MaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), FlickerMaterial);
		Cast<ULightComponent>(LightComponents[i])->SetLightFunctionMaterial(MaterialInstance);
		FlickerMaterialInstances.Add(MaterialInstance);
	}
}

void UHN_FlickerComponent::SetFlickerValue()
{
	for(auto FlickerInst : FlickerMaterialInstances)
		FlickerInst->SetScalarParameterValue(FlickerName, FlickerValue);
}

