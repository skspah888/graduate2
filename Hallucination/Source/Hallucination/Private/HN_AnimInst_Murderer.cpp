// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AnimInst_Murderer.h"
#include "HN_Player.h"
#include "HN_AI_Murderer.h"
#include "HN_Ghost_Murderer.h"

UHN_AnimInst_Murderer::UHN_AnimInst_Murderer()
{
	CurrentPawnSpeed = 0.f;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> ATTACK_MONTAGE(TEXT(
		"AnimMontage'/Game/Characters/Murderer/Murderer_Skeleton_MontageNewAnimMontage.Murderer_Skeleton_MontageNewAnimMontage'"));
	if (ATTACK_MONTAGE.Succeeded())
	{
		AttackMontage = ATTACK_MONTAGE.Object;
	}

}

void UHN_AnimInst_Murderer::NativeUpdateAnimation(float DeltaSeconds)
{
	// 폰의 객체가 유효한지 검사하는 함수 => 제거된 폰 객체를 참조할 수 있기 때문이다.
	auto Pawn = TryGetPawnOwner();

	if (!::IsValid(Pawn)) return;

	CurrentPawnSpeed = Pawn->GetVelocity().Size();
}

void UHN_AnimInst_Murderer::PlayAttackMontage()
{
	Montage_Play(AttackMontage, 0.7f);
	/*if (!Montage_IsPlaying(AttackMontage))
	{
		Montage_Play(AttackMontage, 1.f);
	}*/
}

void UHN_AnimInst_Murderer::AnimNotify_AttackHitCheck()
{
	//// 살인마 공격이 끝났을 때 
	//auto Pawn = TryGetPawnOwner();
	//AHN_Player* TargetObject = Cast<AHN_AI_Murderer>(Pawn->GetController())->GetTarget();
	//if (TargetObject != nullptr)
	//{
	//	print("Hit Player");
	//}

	// 살인마 공격이 끝났을 때 
	auto Pawn = TryGetPawnOwner();
	AHN_Player* TargetObject = Cast<AHN_Ghost_Murderer>(Pawn)->GetTarget();
	if (TargetObject != nullptr)
	{
		TargetObject->CallDeadDelegateFunc();
	}
}