// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Key.h"
#include "HN_Player.h"

AHN_Key::AHN_Key()
{
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_KEY(TEXT("StaticMesh'/Game/Asset/NewAsset/key_fix.key_fix'"));

	if (SM_KEY.Succeeded())
	{
		Mesh->SetStaticMesh(SM_KEY.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> KEY_SOUND(TEXT("SoundCue'/Game/Audio/SoundCue/Key_Sound_Cue.Key_Sound_Cue'"));

	if (KEY_SOUND.Succeeded())
	{
		KeySound = KEY_SOUND.Object;
	}

	eType = ITEM_TYPE::KEY;

	Mesh->ComponentTags.Add(TEXT("outline"));
	SetActorTickEnabled(false);
}

void AHN_Key::BeginPlay()
{
	Super::BeginPlay();
}

void AHN_Key::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Key::GetItem_Implementation(AHN_Player* _player)
{
	_player->SetItem(HAND_TYPE::LEFT, this);
	Speaker->SetSound(KeySound);
	Speaker->Play();
}
