// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Door.h"
#include "HN_Player.h"

// Sets default values
AHN_Door::AHN_Door()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("SCENE"));
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BOX"));
	Door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DOOR"));		//	문 열고 닫히는 부분
	Frame = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FRAME"));	// 문 틀
	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("TRRIGER"));
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));

	RootComponent = Scene;
	Door->SetupAttachment(RootComponent);
	Frame->SetupAttachment(RootComponent);
	BoxCollision->SetupAttachment(Door);
	Trigger->SetupAttachment(RootComponent);

	Scene->Mobility = EComponentMobility::Static;
	Frame->Mobility = EComponentMobility::Static;
	Trigger->Mobility = EComponentMobility::Static;

	//	틀 초기화
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_FRAME(TEXT("StaticMesh'/Game/StarterContent/Props/SM_DoorFrame.SM_DoorFrame'"));

	if (SM_FRAME.Succeeded())
	{
		Frame->SetStaticMesh(SM_FRAME.Object);
	}

	//	문 초기화
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_DOOR(TEXT("StaticMesh'/Game/Asset/Hospital/Meshes/Building/Door/SM_Doorway_01_Door_01.SM_Doorway_01_Door_01'"));

	if (SM_DOOR.Succeeded())
	{
		Door->SetStaticMesh(SM_DOOR.Object);
	}

	//	사운드 초기화
	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_OPEN(TEXT("SoundCue'/Game/Asset/Hospital/Audio/Opening_the_Sliding_Door_Cue.Opening_the_Sliding_Door_Cue'"));

	if (SOUND_OPEN.Succeeded())
	{
		OpenSound = SOUND_OPEN.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_CLOSE(TEXT("SoundCue'/Game/Asset/Hospital/Audio/Door_Closing_Cue.Door_Closing_Cue'"));

	if (SOUND_CLOSE.Succeeded())
	{
		CloseSound = SOUND_CLOSE.Object;
	}

	//	콜리전 크기 설정
	//BoxCollision->SetBoxExtent(FVector(40.0f, 42.0f, 30.0f));	
	BoxCollision->SetRelativeScale3D(FVector(0.3f, 1.5f, 3.25f));
	Trigger->SetRelativeLocation(FVector(0.0f, 0.0f, 100.f));
	Trigger->SetRelativeScale3D(FVector(0.5f, 1.25f, 3.f));

	//	위치조정
	Scene->SetRelativeScale3D(FVector(1.5f, 1.35f, 1.35f));
	BoxCollision->SetRelativeLocation(FVector(0.f, -47.f, 110.f));
	Door->SetRelativeLocation(FVector(0.f, 50.f, 0.f));
	//Door->SetRelativeScale3D(FVector(1.2f, 1.f, 1.f));

	//	콜리전 채널 설정
	BoxCollision->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	Frame->SetCollisionProfileName(TEXT("OverlapOnlyPawn"));
	Door->SetCollisionProfileName(TEXT("OverlapOnlyPawn"));
	Trigger->SetCollisionProfileName(TEXT("HNTrigger"));

	Door->ComponentTags.Add(TEXT("outline"));

	//	변수 초기화
	eState = DOOR_STATE::IDLE;

	CurRotAngle = 50;
	MaxRotAngle = 120;

	bOpen = false;
	bLock = false;
}


// Called when the game starts or when spawned
void AHN_Door::BeginPlay()
{
	Super::BeginPlay();
	
}

void AHN_Door::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	//Trigger->IsOverlappingActor()
}

// Called every frame
void AHN_Door::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//LOG_TEXT(Warning, TEXT("Tick"));

	switch (eState)
	{
	case DOOR_STATE::IDLE:
		Idle();
		break;
	case DOOR_STATE::OPEN:
		Open();
		break;
	case DOOR_STATE::CLOSE:
		Close();
		break;
	default:
		break;
	}
}

void AHN_Door::ChangeState(DOOR_STATE _eNewState)
{
	if (_eNewState == eState)
		return;

	//LOG_TEXT(Warning, TEXT("ChangeState"));

	switch (_eNewState)
	{
	case DOOR_STATE::IDLE:
		break;
	case DOOR_STATE::OPEN:
		Speaker->SetSound(OpenSound);
		break;
	case DOOR_STATE::CLOSE:
		Speaker->SetSound(CloseSound);
		break;
	default:
		break;
	}

	Speaker->Play();
	eState = _eNewState;

	SetActorTickEnabled(true);
}

bool AHN_Door::CanInteract_Implementation()
{
	return !bLock;
}

void AHN_Door::PerformInteract_Implementation()
{
	//LOG_TEXT(Warning, TEXT("Perform"));

	if (bOpen)
	{
		ChangeState(DOOR_STATE::CLOSE);
	}
	else
	{
		ChangeState(DOOR_STATE::OPEN);
	}
}

void AHN_Door::Idle()
{
	//SetActorTickEnabled(false);
}

void AHN_Door::Open()
{
	if (bLock)
	{
		eState = DOOR_STATE::IDLE;
		return;
	}
	else if (FMath::IsNearlyEqual(CurRotAngle, MaxRotAngle, 1.f))
	{
		eState = DOOR_STATE::IDLE;
		bOpen = true;
		return;
	}


	//LOG_TEXT(Warning, TEXT("%f"), UGameplayStatics::GetWorldDeltaSeconds(GetWorld()));

	CurRotAngle += UGameplayStatics::GetWorldDeltaSeconds(GetWorld()) * 70.f;

	Door->SetRelativeLocation(FVector(0.0f, CurRotAngle, 0.0f));
}

void AHN_Door::Close()
{
	if (FMath::IsNearlyEqual(CurRotAngle, 50.f, 1.f))
	{
		eState = DOOR_STATE::IDLE;
		bOpen = false;
		return;
	}


	CurRotAngle -= UGameplayStatics::GetWorldDeltaSeconds(GetWorld()) * 70.f;

	Door->SetRelativeLocation(FVector(0.0f, CurRotAngle, 0.0f));
}


void AHN_Door::SetRenderDepth(bool bValue)
{
	TArray<UActorComponent*> Comps = GetComponentsByTag(UPrimitiveComponent::StaticClass(), TEXT("outline"));
	UPrimitiveComponent* Com = Cast<UPrimitiveComponent>(Comps[0]);
	Com->SetRenderCustomDepth(bValue);
}