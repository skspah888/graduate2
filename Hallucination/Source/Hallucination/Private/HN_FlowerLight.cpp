// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_FlowerLight.h"

// Sets default values
AHN_FlowerLight::AHN_FlowerLight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	LightIntensity = 10000.f;

	PointLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("Point Light"));
	RootComponent = PointLight;
	PointLight->Intensity = LightIntensity;
	PointLight->AttenuationRadius = 500.f;
	PointLight->SetMobility(EComponentMobility::Movable);
	PointLight->bVisible = true;

	DefaultLightColor = PointLight->GetLightColor();
	ChangeLightColor = PointLight->GetLightColor();
}

void AHN_FlowerLight::BeginPlay()
{
	Super::BeginPlay();

	PointLight->SetVisibility(false);
}
/*
void AHN_FlowerLight::PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvent)
{
	//Super::PostEditChangeProperty(PropertyChangedEvent);

	PointLight->Intensity = LightIntensity;
	PointLight->SetLightColor(DefaultLightColor);
}
*/

void AHN_FlowerLight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_FlowerLight::SetCustomLightColor(bool IsChange)
{
	if (IsChange)
		PointLight->SetLightColor(ChangeLightColor);
	else
		PointLight->SetLightColor(DefaultLightColor);
}

void AHN_FlowerLight::SetCustomLightIntensity(float _intensity)
{
	LightIntensity = _intensity;
	PointLight->Intensity = LightIntensity;
}

void AHN_FlowerLight::SetCustomLightVisibility()
{
	PointLight->ToggleVisibility();
}

