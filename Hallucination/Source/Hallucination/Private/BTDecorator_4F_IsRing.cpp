// Fill out your copyright notice in the Description page of Project Settings.


#include "BTDecorator_4F_IsRing.h"
#include "HN_Ghost_Patient.h"
#include "HN_AI_Patient.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTDecorator_4F_IsRing::UBTDecorator_4F_IsRing()
{
	NodeName = TEXT("IsRing");
}

bool UBTDecorator_4F_IsRing::CalculateRawConditionValue(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) const
{
	// 원하는 조건이 달성됐는지 파악
	bool bResult = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	auto ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == ControllingPawn)
		return false;

	bool IsRing = OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Patient::IsRingKey);

	return IsRing;
}
