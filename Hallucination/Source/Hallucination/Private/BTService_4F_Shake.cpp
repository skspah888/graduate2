// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_4F_Shake.h"
#include "HN_Player.h"


UBTService_4F_Shake::UBTService_4F_Shake()
{
	NodeName = TEXT("Shake");
}

void UBTService_4F_Shake::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSecond)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSecond);

	auto Player = Cast<AHN_Player>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (Player == nullptr)
		//LOG_TEXT(Warning, TEXT("No Player"));

	Player->Scamp_Shake();
}
