// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_DragClear.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_AI_Drag.h"
#include "HN_Player.h"
#include "HN_Ghost_Drag.h"

UBTTask_4F_DragClear::UBTTask_4F_DragClear()
{
	NodeName = TEXT("Clear Drag Ghost");
	IsClear = false;
}

EBTNodeResult::Type UBTTask_4F_DragClear::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	if(IsClear)
		return EBTNodeResult::Succeeded;

	auto TargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::TargetKey));
	if (nullptr == TargetPlayer)
		return EBTNodeResult::Failed;

	auto NotTargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::NotTargetKey));
	if (nullptr == NotTargetPlayer)
		return EBTNodeResult::Failed;

	//print("Clear Drag!!");

	// TargetPlayer 상호작용 가능하게
	TargetPlayer->SetPlayerInteract(true);
	NotTargetPlayer->SetPlayerInteract(true);

	// TargetPlayer 원래 모습으로 되돌리기 
	/*FOutputDeviceNull ar;
	TargetPlayer->CallFunctionByNameWithArguments(TEXT("CallFunctionTest"), ar, NULL, true);*/

	// 타이머 설정 [ 5초뒤 파괴 ]
	auto ControllingPawn = Cast<AHN_Ghost_Drag>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;
	ControllingPawn->ClearDrag();

	// 플레이어 귀신 처리
	Cast<AHN_Ghost_Drag>(ControllingPawn)->PenaltyPlayerGhost();

	IsClear = true;

	return EBTNodeResult::Succeeded;
}
