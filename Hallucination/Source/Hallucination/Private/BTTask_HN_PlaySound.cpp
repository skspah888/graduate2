// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_HN_PlaySound.h"
#include "AIController.h"
#include "Sound/SoundCue.h"

UBTTask_HN_PlaySound::UBTTask_HN_PlaySound()
{
	NodeName = TEXT("HN_PlaySound");
}

//	볼륨조절, 감쇠설정같은건 사운드 큐에서 해놓으면 알아서 플레이함

EBTNodeResult::Type UBTTask_HN_PlaySound::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	APawn* Owner = OwnerComp.GetAIOwner()->GetPawn();
	if (nullptr == Owner) return EBTNodeResult::Succeeded;

	AISpeaker = Cast<UAudioComponent>(Owner->GetComponentByClass(UAudioComponent::StaticClass()));

	if (nullptr == AISpeaker)
	{
		//LOG_TEXT(Warning, TEXT("Audio Component is null"));
		return EBTNodeResult::Succeeded;
	}


	//	플레이중이 아닐 때
	if (!AISpeaker->IsPlaying())
	{
		AISpeaker->SetSound(SoundToPlay);
		AISpeaker->Play();
	}
	else
	{
		//	플레이중일 때

		//	새로 들어온 사운드가 이전 것과 다르다
		//	재생 돼야함
		if (AISpeaker->Sound != SoundToPlay)
		{
			AISpeaker->SetSound(SoundToPlay);
			AISpeaker->Play();
		}
	}

	return EBTNodeResult::Succeeded;
}