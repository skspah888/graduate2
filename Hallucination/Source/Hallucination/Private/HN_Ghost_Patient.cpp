// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Ghost_Patient.h"
#include "HN_AI_Patient.h"
#include "HN_AnimInst_Patient.h"
#include "Sound/SoundCue.h"
#include "TimerManager.h"


AHN_Ghost_Patient::AHN_Ghost_Patient()
{
	PrimaryActorTick.bCanEverTick = true;

	Init();

	// AI 설정
	AIControllerClass = AHN_AI_Patient::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	// 스켈레탈 메시
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_PATIENT(TEXT(
		"SkeletalMesh'/Game/Asset/NewAsset/4F_Model/Patient/Pointing.Pointing'"));
	if (SK_PATIENT.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_PATIENT.Object);
	}

	// 캡슐 컴포넌트
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("HNCharacter"));

	//	애니메이션 블루프린트 설정
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> ANIM(TEXT("/Game/Asset/NewAsset/4F_Model/Patient/Patient_AnimBP.Patient_AnimBP_C"));
	if (ANIM.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(ANIM.Class);
	}

	// 사운드 설정
	static ConstructorHelpers::FObjectFinder<USoundCue> CrySoundCueObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Patient/GirlCry_SoundCue.GirlCry_SoundCue'"));
	if (CrySoundCueObject.Succeeded())
	{
		CrySoundCue = CrySoundCueObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> RingSoundCueObject(TEXT(
		"SoundCue'/Game/Audio/SoundCue/Patient/Ring_SoundCue.Ring_SoundCue'"));
	if (RingSoundCueObject.Succeeded())
	{
		RingSoundCue = RingSoundCueObject.Object;
	}

	CurrentState = PATIENT_STATE::STATE_END;
	NextState = PATIENT_STATE::STATE_END;
}

void AHN_Ghost_Patient::BeginPlay()
{
	Super::BeginPlay();
}

void AHN_Ghost_Patient::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
}

void AHN_Ghost_Patient::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_Ghost_Patient::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);
}

void AHN_Ghost_Patient::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	AnimInst = Cast<UHN_AnimInst_Patient>(GetMesh()->GetAnimInstance());
}

void AHN_Ghost_Patient::Acquired_Ring()
{
	Cast<AHN_AI_Patient>(GetController())->Acquired_Ring();
}
void AHN_Ghost_Patient::ChangeAnim_Patient_Implementation() {
	AnimInst->SetIsRing();
}

bool AHN_Ghost_Patient::IsRing()
{
	return Cast<AHN_AI_Patient>(GetController())->IsRing();
}

void AHN_Ghost_Patient::SetRing(bool bCheck)
{
	Cast<AHN_AI_Patient>(GetController())->SetClear(true);
}

bool AHN_Ghost_Patient::CanInteract_Implementation()
{
	return IsRing();
}

void AHN_Ghost_Patient::PerformInteract_Implementation()
{
	// 반지가 있을때 귀신한테 말을 걸었다.
	SetRing(true);
}

void AHN_Ghost_Patient::PlayPatientSound_Implementation(bool isPlay)
{
	isPlay ? Speaker->Play() : Speaker->Stop();
}

void AHN_Ghost_Patient::SetPatientSound(USoundCue * _SoundCue, bool isPlay)
{
	Speaker->SetSound(_SoundCue);
	PlayPatientSound(isPlay);
}

void AHN_Ghost_Patient::SetState_Implementation(PATIENT_STATE eState)
{
	NextState = eState;

	if (CurrentState == NextState)
		return;

	switch (NextState)
	{
	case PATIENT_STATE::CRY:
		if (Speaker->IsPlaying())
			break;
		Speaker->SetSound(CrySoundCue);
		Speaker->Play();
		CurrentState = NextState;
		break;

	case PATIENT_STATE::RING:
		Speaker->SetSound(RingSoundCue);
		Speaker->Play();
		CurrentState = NextState;
		break;

	case PATIENT_STATE::CLEAR:
		Speaker->Stop();
		CurrentState = NextState;
		break;

	case PATIENT_STATE::STATE_END:
		break;

	default:
		break;
	}
}

void AHN_Ghost_Patient::DestoryPatient()
{
	Destroy();
}

void AHN_Ghost_Patient::ClearPatient()
{
	float TimerTime = 5.f;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AHN_Ghost_Patient::DestoryPatient, 5.f, false);

	// bCheck 초기화
	Cast<AHN_AI_Patient>(GetController())->SetPatientClear();
}
