// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_WayPoint.h"
#include "HN_Ghost_Nurse.h"

// Sets default values
AHN_WayPoint::AHN_WayPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Root);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	BoxComponent->SetupAttachment(GetRootComponent());
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AHN_WayPoint::OnActorEnter);
}

// Called when the game starts or when spawned
void AHN_WayPoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHN_WayPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHN_WayPoint::OnActorEnter(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, 
								UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, 
								bool bFromSweep, const FHitResult & SweepResult)
{
	AHN_Ghost_Nurse* Ghost = nullptr;
	if (OtherActor != nullptr)
	{
		Ghost = Cast<AHN_Ghost_Nurse>(OtherActor);
		if (Ghost != nullptr)
		{
			// 목적지 도착 [ 목적지 바꾸고, Idle 상태 진입? ]
			Ghost->SetGoalPoint(true);
			Ghost->NextWayPoint = NextWayPoint;
		}
	}
}

