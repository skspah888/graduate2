// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_SpotLight.h"
#include "Sound/SoundCue.h"
#include "Kismet/KismetMaterialLibrary.h"

// Sets default values
AHN_SpotLight::AHN_SpotLight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// 컴포넌트 초기화
	LightMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Light Mesh"));
	LightParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Light Particle"));;
	LightSpeaker = CreateDefaultSubobject<UAudioComponent>(TEXT("Light Speaker"));
	SpotLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("Spot Light"));;

	// 기본 메쉬 세팅
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshObject(TEXT(
		"StaticMesh'/Game/HorrorEngine/Meshes/Light.Light'"));
	if (StaticMeshObject.Succeeded())
	{
		LightMesh->SetStaticMesh(StaticMeshObject.Object);
	}

	// 기본 사운드 세팅
	static ConstructorHelpers::FObjectFinder<USoundCue> SoundObject(TEXT(
		"SoundCue'/Game/HorrorEngine/Audio/Environment/Bulb_Explosion_Cue.Bulb_Explosion_Cue'"));
	if (SoundObject.Succeeded())
	{
		ExplosionSound = SoundObject.Object;
		LightSpeaker->SetSound(ExplosionSound);
	}

	// 기본 파티클 세팅
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleObject(TEXT(
		"ParticleSystem'/Game/HorrorEngine/Particles/LightExplosion.LightExplosion'"));
	if (ParticleObject.Succeeded())
	{
		LightParticle->SetTemplate(ParticleObject.Object);
	}

	SpotLight->SetRelativeLocation(FVector(0.f, 0.f, -40.f));
	SpotLight->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
	LightParticle->SetRelativeLocation(FVector(0.f, 0.f, -25.f));

	RootComponent = LightMesh;
	SpotLight->SetupAttachment(LightMesh);
	LightParticle->SetupAttachment(LightMesh);
	LightSpeaker->SetupAttachment(LightMesh);

	LightParticle->bAutoActivate = false;
	LightSpeaker->bAutoActivate = false;

	LightParticle->OnSystemFinished.AddDynamic(this, &AHN_SpotLight::OnParticleEnd);

	EmissiveName = "Emissive";
	bIsBreak = false;
}

// Called when the game starts or when spawned
void AHN_SpotLight::BeginPlay()
{
	Super::BeginPlay();
	
	GetStaticMesh();
}

// Called every frame
void AHN_SpotLight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHN_SpotLight::LightExplosion()
{
	// 라이트 파괴
	LightSpeaker->Play();
	LightParticle->SetActive(true, true);
	SpotLight->SetIntensity(0.f);

	for (auto MaterialInst : MaterialInstances)
	{
		Cast<UMaterialInstanceDynamic>(MaterialInst)->SetScalarParameterValue(EmissiveName, 0.f);
		//print(MaterialInst->GetName());
	}

	bIsBreak = true;
}

void AHN_SpotLight::OnParticleEnd(UParticleSystemComponent* PSystem)
{
	//print("HN_PointLight Finish!");
}

void AHN_SpotLight::GetStaticMesh()
{
	TArray<UActorComponent*> StaticMeshComponents = GetComponentsByClass(UStaticMeshComponent::StaticClass());
	for (int i = 0; i < StaticMeshComponents.Num(); ++i)
	{
		TArray<UMaterialInterface*> MaterialInterfaces = Cast<UStaticMeshComponent>(StaticMeshComponents[i])->GetMaterials();
		for (int j = 0; j < MaterialInterfaces.Num(); ++j)
		{
			UMaterialInstanceDynamic* MaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), MaterialInterfaces[j]);
			Cast<UStaticMeshComponent>(StaticMeshComponents[i])->SetMaterial(j, MaterialInstance);
			MaterialInstances.Add(MaterialInstance);
		}
	}
}