// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_1F_DetectPlayer.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Player.h"
#include "HN_Ghost_Murderer.h"
#include "HN_AI_Murderer.h"
#include "EngineUtils.h"
#include <limits>

UBTService_1F_DetectPlayer::UBTService_1F_DetectPlayer()
{
	NodeName = TEXT("Dectect");
	Interval = 0.1f;
}

void UBTService_1F_DetectPlayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto ControllingPawn = Cast<AHN_Ghost_Murderer>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
	{
		//print("Not ControllingPawn");
		return;
	}

	 // 플레이어 검사
	UWorld* CurrentWorld = GetWorld();
	float fCurrentDist = (float)INT_MAX;
	
	for (TActorIterator<AHN_Player> Iter(CurrentWorld); Iter; ++Iter)
	{
		FVector GhostPosition = ControllingPawn->GetActorLocation();
		FVector PlayerPosition = (*Iter)->GetActorLocation();
		float fDist = FVector::DistXY(GhostPosition, PlayerPosition);
		if (fCurrentDist > fDist)
		{
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AHN_AI_Murderer::TargetKey, *Iter);
			fCurrentDist = fDist;
		}
	}
}
