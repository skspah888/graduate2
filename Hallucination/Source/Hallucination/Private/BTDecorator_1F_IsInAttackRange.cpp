// Fill out your copyright notice in the Description page of Project Settings.


#include "BTDecorator_1F_IsInAttackRange.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Ghost_Murderer.h"
#include "HN_Player.h"
#include "HN_AI_Murderer.h"

UBTDecorator_1F_IsInAttackRange::UBTDecorator_1F_IsInAttackRange()
{
	NodeName = TEXT("CanAttack");
}

bool UBTDecorator_1F_IsInAttackRange::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	bool bResult = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	auto ControllingPawn = Cast<AHN_Ghost_Murderer>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
	{
		//print("Not ControllingPawn");
		return false;
	}

	auto Target = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Murderer::TargetKey));
	if (nullptr == Target)
	{
		//print("Not Target");
		return false;
	}

	FVector TargetLocation = Target->GetActorLocation();
	FVector PawnLocation = ControllingPawn->GetActorLocation();

	float fDist = FVector::DistXY(TargetLocation, PawnLocation);
	bResult = (fDist <= ControllingPawn->GetAttackRange());

	if (!bResult)
	{
		ControllingPawn->StartBreathSound();
	}

	return bResult;
}
