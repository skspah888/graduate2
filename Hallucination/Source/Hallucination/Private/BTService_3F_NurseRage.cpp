// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_3F_NurseRage.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Player.h"
#include "HN_Ghost_Nurse.h"
#include "HN_AI_Nurse.h"
#include "EngineUtils.h"

UBTService_3F_NurseRage::UBTService_3F_NurseRage()
{
	NodeName = TEXT("Nurse RageMode");
	Interval = 0.1f;
}

void UBTService_3F_NurseRage::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	//print("Rage");

	for (TActorIterator<AHN_Player> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsObject(AHN_AI_Nurse::TargetKey, *ActorItr);
	}
}