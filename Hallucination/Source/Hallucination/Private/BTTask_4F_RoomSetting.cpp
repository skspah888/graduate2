// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_RoomSetting.h"
#include "EngineUtils.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Ghost_Drag.h"
#include "HN_Player.h"
#include "HN_AI_Drag.h"

UBTTask_4F_RoomSetting::UBTTask_4F_RoomSetting()
{
	NodeName = TEXT("Room Setting");
}

EBTNodeResult::Type UBTTask_4F_RoomSetting::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	UWorld* CurrentWorld = GetWorld();
	int32 iRandom = FMath::RandRange(0, 1);
	int32 iCount = 0;

	// 타겟 플레이어 셋팅 
	for (TActorIterator<AHN_Player> Iter(CurrentWorld); Iter; ++Iter)
	{
		if (iCount == 1)//iRandom)		// 타겟 플레이어
		{
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AHN_AI_Drag::TargetKey, *Iter);
		}
		else						// Not 타겟 플레이어
		{
			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AHN_AI_Drag::NotTargetKey, *Iter);
		}
		++iCount;
	}

	// Room 셋팅
	iRandom = FMath::RandRange(0, 1);
	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	Cast<AHN_Ghost_Drag>(ControllingPawn)->Setting_TargetRoom(iRandom);
	
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Drag::IsRoomSettingKey, true);

	// Door Open
	Cast<AHN_Ghost_Drag>(ControllingPawn)->Setting_Door(true);

	// Player Ghost Spawn
	Cast<AHN_Ghost_Drag>(ControllingPawn)->SpawnPlayerGhost();

	AHN_Player* TargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::TargetKey));
	if (nullptr == TargetPlayer)
		return EBTNodeResult::Failed;

	Cast<AHN_Ghost_Drag>(ControllingPawn)->SetActorIgnore(TargetPlayer, true);

	return EBTNodeResult::Succeeded;
}
