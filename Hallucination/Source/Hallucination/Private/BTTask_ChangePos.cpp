// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_ChangePos.h"
#include "HN_AI_Scamp.h"
#include "HN_Ghost_Scamp.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"
#include "HN_AnimInst_Scamp.h"

#define ROOM_LEFT 270
#define ROOM_TOP -40
#define ROOM_RIGHT -890
#define ROOM_BOTTOM -1110

#define STANDARD_X 700.f
#define STANDARD_Y 700.f

UBTTask_ChangePos::UBTTask_ChangePos()
{
	NodeName = TEXT("ChangePos");

	CurrentType = PATTERN_TYPE::FIRST;
}

EBTNodeResult::Type UBTTask_ChangePos::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto Scamp = Cast<AHN_Ghost_Scamp>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == Scamp)
	{
		//LOG_TEXT(Warning, TEXT("Scamp nullptr"));
		return EBTNodeResult::Failed;
	}

	auto ScampAnim = Cast<UHN_AnimInst_Scamp>(Scamp->GetMesh()->GetAnimInstance());
	if (nullptr == ScampAnim)
	{
		//LOG_TEXT(Warning, TEXT("Scamp Anim Inst nullptr"));
		return EBTNodeResult::Failed;
	}


	switch (CurrentType)
	{
	case PATTERN_TYPE::FIRST:
	{
		Scamp->ActiveActor();
		ScampAnim->SetIsIdle(true);
		float fCenterX = ROOM_RIGHT + (abs(ROOM_LEFT - ROOM_RIGHT) / 2.f);
		float fCenterY = ROOM_BOTTOM + (abs(ROOM_TOP - ROOM_BOTTOM) / 2.f);

		Scamp->SetActorLocation(FVector(fCenterX, fCenterY, 100.f));	//	액터 위치설정
		//OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Scamp::IsClearKey, false);

		return EBTNodeResult::Succeeded;
		break;
	}
	case PATTERN_TYPE::SHAKE:
	{
		ScampAnim->SetIsIdle(false);
		//	플레이어와 먼 곳에서 생성
		FVector vPlayerPos = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
		float fCenterX = ROOM_RIGHT + (abs(ROOM_LEFT - ROOM_RIGHT) / 2.f);
		float fCenterY = ROOM_BOTTOM + (abs(ROOM_TOP - ROOM_BOTTOM) / 2.f);
		//LOG_TEXT(Warning, TEXT("Center : %f, %f"), fCenterX, fCenterY);

		if (vPlayerPos.X >= fCenterX && vPlayerPos.Y >= fCenterY)
		{
			//	왼쪽 위에 있을때
			Scamp->SetActorLocation(FVector(ROOM_RIGHT, ROOM_BOTTOM, 100.f));
		}
		else if (vPlayerPos.X <= fCenterX && vPlayerPos.Y >= fCenterY)
		{
			//	오른쪽 위에 있을때
			Scamp->SetActorLocation(FVector(ROOM_LEFT, ROOM_BOTTOM, 100.f));
		}
		else if (vPlayerPos.X >= fCenterX && vPlayerPos.Y <= fCenterY)
		{
			//	왼쪽 아래에 있을때
			Scamp->SetActorLocation(FVector(ROOM_RIGHT, ROOM_TOP, 100.f));
		}
		else
		{
			//	오른쪽 아래에 있을때
			Scamp->SetActorLocation(FVector(ROOM_LEFT, ROOM_TOP, 100.f));
		}
		Scamp->PlayShakeSound();
		return EBTNodeResult::Succeeded;
		break;
	}
	case PATTERN_TYPE::MOVE:
	{
		//UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetNavigationSystem(Scamp->GetWorld());

		//FVector vPlayerPos = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
		////FVector Origin = Scamp->GetActorLocation();
		//FNavLocation NextPos;

		//while (true)
		//{
		//	NextPos.Location.Z = vPlayerPos.Z;
		//	if (NavSystem->GetRandomPointInNavigableRadius(vPlayerPos, 1500.f, NextPos))
		//	{
		//		if (((vPlayerPos.X - STANDARD_X / 2.f) < NextPos.Location.X) && ( NextPos.Location.X < (vPlayerPos.X + STANDARD_X / 2.f)) && ((vPlayerPos.Y - STANDARD_Y / 2.f) < NextPos.Location.Y) && (NextPos.Location.Y < (vPlayerPos.Y + STANDARD_Y / 2.f)))
		//		{
		//			//	플레이어와 귀신이 가깝다
		//			LOG_TEXT(Error, TEXT("Player Pos : x %f y %f / Scamp Pos : x %f y %f z %f"), vPlayerPos.X, vPlayerPos.Y, NextPos.Location.X, NextPos.Location.Y, NextPos.Location.Z);
		//			LOG_TEXT(Error, TEXT("Failed"));
		//		}
		//		else
		//		{
		//			//	플레이어와 귀신이 머니까 귀신 위치를 다음 위치로 세팅한다
		//			OwnerComp.GetBlackboardComponent()->SetValueAsVector(AHN_AI_Scamp::NextPosKey, NextPos.Location);
		//			Scamp->SetActorLocation(NextPos.Location);
		//			LOG_TEXT(Error, TEXT("Player Pos : x %f y %f / Scamp Pos : x %f y %f z %f"), vPlayerPos.X, vPlayerPos.Y, NextPos.Location.X, NextPos.Location.Y, NextPos.Location.Z);
		//			LOG_TEXT(Error, TEXT("Succeeded"));
		//			return EBTNodeResult::Succeeded;
		//		}
		//	}
		//}

		//-------------------------------------------------------------------------------
		ScampAnim->SetIsIdle(false);
		FVector vPlayerPos = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
		FVector vRandomPos = FVector::ZeroVector;


		while (true)
		{
			vRandomPos.X = FMath::FRandRange(0.f, (fabsf(ROOM_LEFT) + fabsf(ROOM_RIGHT))) - fabsf(ROOM_RIGHT);
			vRandomPos.Y = -(FMath::FRandRange(fabsf(ROOM_TOP), fabsf(ROOM_BOTTOM)));
			vRandomPos.Z = vPlayerPos.Z;

			//LOG_TEXT(Error, TEXT("Scamp Pos : x %f y %f z %f"), vRandomPos.X, vRandomPos.Y, vRandomPos.Z);

			if (((vPlayerPos.X - STANDARD_X / 2.f) + 3000 < vRandomPos.X + 3000) && (vRandomPos.X + 3000 < (vPlayerPos.X + STANDARD_X / 2.f) + 3000)
				&& ((vPlayerPos.Y - STANDARD_Y / 2.f) + 3000 < vRandomPos.Y + 3000) && (vRandomPos.Y + 3000 < (vPlayerPos.Y + STANDARD_Y / 2.f) + 3000))
			{
				//	가까우면
				//LOG_TEXT(Error, TEXT("Failed"));
			}
			else
			{
				//	플레이어와 가깝지 않으면
				Scamp->SetActorLocation(vRandomPos);
				Scamp->PlayNotShakeSound();
				return EBTNodeResult::Succeeded;
			}
		}

		//LOG_TEXT(Error, TEXT("GetRandomPointInNavigableRadius Failed"));
		return EBTNodeResult::Failed;
		break;
	}
	default:
		break;
	}

	return EBTNodeResult::Succeeded;
}
