// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_VolumeControl.h"
#include "HN_AI_Patient.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Ghost_Patient.h"
#include "HN_Player.h"

UBTTask_4F_VolumeControl::UBTTask_4F_VolumeControl()
{
	NodeName = TEXT("Patient State Change");
}

// 비헤이비어 트리의 태스크 실행 시 이 함수 실행
EBTNodeResult::Type UBTTask_4F_VolumeControl::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	// 이 AI에 연결된 Pawn 불러오기
	auto ControllingPawn = Cast<AHN_Ghost_Patient>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;

	bool isContact = OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Patient::IsContactKey);
	bool isClear = OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Patient::IsClearKey);

	if (isContact)		// 플레이어 접촉 상태 : 반지 사운드
	{
		if (isClear)	// Scamp 클리어 후
		{
			auto TargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Patient::TargetKey));
			if (nullptr == TargetPlayer)
				return EBTNodeResult::Failed;

			bool bCheck = TargetPlayer->CheckItem("Ring");
			if (bCheck)	// 반지가 있음
			{
				ControllingPawn->SetState(PATIENT_STATE::CLEAR);	// 반지가 있으면 Clear
			}
			else
			{
				ControllingPawn->SetState(PATIENT_STATE::CRY);	// 반지가 없음
			}
		}
		else			// Scamp 클리어 전
		{
			ControllingPawn->SetState(PATIENT_STATE::RING);		// 반지가 없으면 Ring
		}
	}
	else				// 플레이어 접촉 상태 X : 우는 사운드
	{
		ControllingPawn->SetState(PATIENT_STATE::CRY);
	}

	return EBTNodeResult::Succeeded;
}
