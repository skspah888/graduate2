// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_GateButton.h"
#include "HN_Gate.h"

AHN_GateButton::AHN_GateButton()
{
	ButtonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BUTTON"));
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_BUTTONBASE(TEXT("StaticMesh'/Game/HorrorEngine/Meshes/ButtonBase.ButtonBase'"));

	if (SM_BUTTONBASE.Succeeded())
	{
		Mesh->SetStaticMesh(SM_BUTTONBASE.Object);
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_BUTTON(TEXT("StaticMesh'/Game/HorrorEngine/Meshes/Button.Button'"));

	if (SM_BUTTON.Succeeded())
	{
		ButtonMesh->SetStaticMesh(SM_BUTTON.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_ON(TEXT("SoundCue'/Game/HorrorEngine/Audio/Button/ButtonOn_Cue.ButtonOn_Cue'"));

	if (SOUND_ON.Succeeded())
	{
		ButtonOnSound = SOUND_ON.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_OFF(TEXT("SoundCue'/Game/HorrorEngine/Audio/Button/ButtonOff_Cue.ButtonOff_Cue'"));

	if (SOUND_OFF.Succeeded())
	{
		ButtonOffSound = SOUND_OFF.Object;
	}

	ButtonMesh->SetupAttachment(RootComponent);
	ButtonMesh->SetRelativeLocation(FVector(0.f, 0.8f, 5.0f));
	ButtonMesh->ComponentTags.Add(TEXT("outline"));

	Gate = NULL;

	SetActorTickEnabled(false);
}

void AHN_GateButton::BeginPlay()
{
	Super::BeginPlay();

	UMaterialInstanceDynamic* pInstance = ButtonMesh->CreateDynamicMaterialInstance(0, ButtonMesh->GetMaterial(0));
	pInstance->SetScalarParameterValue(TEXT("Emissive"), 1.f);
}

void AHN_GateButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_GateButton::ActiveOn_Implementation()
{
	bActive = true;

	if(Gate != NULL)
		Gate->Open();

	UMaterialInstanceDynamic* pInstance = ButtonMesh->CreateDynamicMaterialInstance(0, ButtonMesh->GetMaterial(0));
	pInstance->SetScalarParameterValue(TEXT("Emissive"), 0.f);

	ButtonMesh->SetRelativeLocation(FVector(0.f, 0.5f, 5.0f));

	Speaker->Stop();
	Speaker->SetSound(ButtonOnSound);
	Speaker->Play();
}

void AHN_GateButton::ActiveOff_Implementation()
{
	bActive = false;

	if (Gate != NULL)
		Gate->Stop();

	UMaterialInstanceDynamic* pInstance = ButtonMesh->CreateDynamicMaterialInstance(0, ButtonMesh->GetMaterial(0));
	pInstance->SetScalarParameterValue(TEXT("Emissive"), 1.f);

	ButtonMesh->SetRelativeLocation(FVector(0.f, 0.8f, 5.0f));

	Speaker->Stop();
	Speaker->SetSound(ButtonOffSound);
	Speaker->Play();
}
