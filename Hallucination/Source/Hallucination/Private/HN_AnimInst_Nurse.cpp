// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AnimInst_Nurse.h"
#include "HN_Player.h"
#include "HN_AI_Nurse.h"
#include "HN_Ghost_Nurse.h"


UHN_AnimInst_Nurse::UHN_AnimInst_Nurse()
{
	eState = ENurseState::IDLE;
	CurrentPawnSpeed = 0.f;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> ATTACK_MONTAGE(TEXT(
		"AnimMontage'/Game/Characters/Nurse/nurse_Skeleton_Montage.nurse_Skeleton_Montage'"));
	if (ATTACK_MONTAGE.Succeeded())
	{
		AttackMontage = ATTACK_MONTAGE.Object;
	}

}

void UHN_AnimInst_Nurse::NativeUpdateAnimation(float DeltaSeconds)
{
	// 폰의 객체가 유효한지 검사하는 함수 => 제거된 폰 객체를 참조할 수 있기 때문이다.
	auto Pawn = TryGetPawnOwner();

	if (!::IsValid(Pawn)) return;

	CurrentPawnSpeed = Pawn->GetVelocity().Size();
}

void UHN_AnimInst_Nurse::SetState(ENurseState NextState)
{
	eState = NextState;
}

uint32 UHN_AnimInst_Nurse::GetState()
{
	return (uint32)eState;
}

void UHN_AnimInst_Nurse::PlayAttackMontage()
{
	Montage_Play(AttackMontage, 1.f);
	/*if (!Montage_IsPlaying(AttackMontage))
	{
		Montage_Play(AttackMontage, 1.f);
	}*/
}

bool UHN_AnimInst_Nurse::AnimNotify_AttackHitCheck_Validate()
{
	return true;
}

void UHN_AnimInst_Nurse::AnimNotify_AttackHitCheck_Implementation()
{
	//// 간호사 공격이 끝났을 때 
	//auto Pawn = TryGetPawnOwner();
	//AHN_Player* TargetObject = Cast<AHN_AI_Nurse>(Pawn->GetController())->GetTarget();  
	//if (TargetObject != nullptr)
	//{
	//	print("Hit Player");
	//}

	// 간호사 공격이 끝났을 때 
	auto Pawn = TryGetPawnOwner();
	AHN_Player* TargetObject = Cast<AHN_Ghost_Nurse>(Pawn)->GetTarget();
	if (TargetObject != nullptr)
	{
		TargetObject->CallDeadDelegateFunc();
	}
}
