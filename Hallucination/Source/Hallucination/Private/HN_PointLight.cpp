// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_PointLight.h"
#include "Sound/SoundCue.h"
#include "Kismet/KismetMaterialLibrary.h"

// Sets default values
AHN_PointLight::AHN_PointLight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// 컴포넌트 초기화
	LightMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Light Mesh"));
	PointLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("Point Light"));;
	LightParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Light Particle"));;
	LightSpeaker = CreateDefaultSubobject<UAudioComponent>(TEXT("Light Speaker"));

	// 기본 메쉬 세팅
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshObject(TEXT(
		"StaticMesh'/Game/HorrorEngine/Meshes/Light.Light'"));
	if (StaticMeshObject.Succeeded())
	{
		LightMesh->SetStaticMesh(StaticMeshObject.Object);
	}
	
	// 기본 사운드 세팅
	static ConstructorHelpers::FObjectFinder<USoundCue> SoundObject(TEXT(
		"SoundCue'/Game/HorrorEngine/Audio/Environment/Bulb_Explosion_Cue.Bulb_Explosion_Cue'"));
	if (SoundObject.Succeeded())
	{
		ExplosionSound = SoundObject.Object;
		LightSpeaker->SetSound(ExplosionSound);
	}

	// 기본 파티클 세팅
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleObject(TEXT(
		"ParticleSystem'/Game/HorrorEngine/Particles/LightExplosion.LightExplosion'"));
	if (ParticleObject.Succeeded())
	{
		LightParticle->SetTemplate(ParticleObject.Object);
	}

	PointLight->SetRelativeLocation(FVector(0.f, 0.f, -40.f));
	LightParticle->SetRelativeLocation(FVector(0.f, 0.f, -25.f));

	RootComponent = LightMesh;
	PointLight->SetupAttachment(LightMesh);
	LightParticle->SetupAttachment(LightMesh);
	LightSpeaker->SetupAttachment(LightMesh);

	LightParticle->bAutoActivate = false;
	LightSpeaker->bAutoActivate = false;

	LightParticle->OnSystemFinished.AddDynamic(this, &AHN_PointLight::OnParticleEnd);

	ParameterName = "Emissive";
}

// Called when the game starts or when spawned
void AHN_PointLight::BeginPlay()
{
	Super::BeginPlay();

	GetStaticMesh();
}

// Called every frame
void AHN_PointLight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_PointLight::LightExplosion()
{
	// 라이트 파괴
	LightSpeaker->Play();
	LightParticle->SetActive(true, true);
	PointLight->SetIntensity(0.f);

	for (auto MaterialInst : MaterialInstances)
	{
		Cast<UMaterialInstanceDynamic>(MaterialInst)->SetScalarParameterValue(ParameterName, 0.f);
		//print(MaterialInst->GetName());
	}
}

void AHN_PointLight::OnParticleEnd(UParticleSystemComponent* PSystem)
{
	//print("HN_PointLight Finish!");
}

void AHN_PointLight::GetStaticMesh()
{
	TArray<UActorComponent*> StaticMeshComponents = GetComponentsByClass(UStaticMeshComponent::StaticClass());
	for (int i = 0; i < StaticMeshComponents.Num(); ++i)
	{
		TArray<UMaterialInterface*> MaterialInterfaces = Cast<UStaticMeshComponent>(StaticMeshComponents[i])->GetMaterials();
		for (int j = 0; j < MaterialInterfaces.Num(); ++j)
		{
			UMaterialInstanceDynamic* MaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), MaterialInterfaces[j]);
			Cast<UStaticMeshComponent>(StaticMeshComponents[i])->SetMaterial(j, MaterialInstance);
			MaterialInstances.Add(MaterialInstance);
		}
	}
}

