// Fill out your copyright notice in the Description page of Project Settings.


#include "BTDecorator_3F_IsInAttackRange.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Ghost_Nurse.h"
#include "HN_Player.h"
#include "HN_AI_Nurse.h"

UBTDecorator_3F_IsInAttackRange::UBTDecorator_3F_IsInAttackRange()
{
	NodeName = TEXT("CanAttack");
}

bool UBTDecorator_3F_IsInAttackRange::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	bool bResult = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	auto ControllingPawn = Cast<AHN_Ghost_Nurse>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
	{
		//print("Not ControllingPawn");
		return false;
	}

	auto Target = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Nurse::TargetKey));
	if (nullptr == Target)
	{
		//print("Not Target");
		return false;
	}

	FVector TargetLocation = Target->GetActorLocation();
	TargetLocation.Z = 0.f;

	FVector PawnLocation = ControllingPawn->GetActorLocation();
	PawnLocation.Z = 0.f;

	float fDist = FVector::Dist(TargetLocation, PawnLocation);
	bResult = (fDist <= ControllingPawn->GetAttackRange());
	return bResult;

	//return OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Nurse::IsAttackKey);
}
