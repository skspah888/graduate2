// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Radio.h"

// Sets default values
AHN_Radio::AHN_Radio()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_RADIO(TEXT("StaticMesh'/Game/HorrorEngine/Meshes/Radio.Radio'"));

	if (SM_RADIO.Succeeded())
	{
		Mesh->SetStaticMesh(SM_RADIO.Object);
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_EMISSIVE(TEXT("Texture2D'/Game/HorrorEngine/Textures/RadioEmissive.RadioEmissive'"));

	if (TEX_EMISSIVE.Succeeded())
	{
		EmissiveTex = TEX_EMISSIVE.Object;
	}

	CurrentTime = 0.f;
	CurrentDuration = 0.f;
	Switching = false;

	Mesh->ComponentTags.Add(TEXT("outline"));
}

// Called when the game starts or when spawned
void AHN_Radio::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHN_Radio::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!Switching)
	{
		float NewTime = CurrentTime + DeltaTime;
		
		if(NewTime >= CurrentDuration)
		{
			CurrentTime = 0.f;
		}
		else
		{
			CurrentTime = NewTime;
		}
	}
}

void AHN_Radio::ActiveOn_Implementation()
{
	bActive = true;

	UMaterialInstanceDynamic* pMtrlInstance = Mesh->CreateDynamicMaterialInstance(0, Mesh->GetMaterial(0));
	pMtrlInstance->SetTextureParameterValue(TEXT("EmissiveTexture"), EmissiveTex);

	pMtrlInstance->SetScalarParameterValue(TEXT("Emissive"), 20.f);
}

void AHN_Radio::ActiveOff_Implementation()
{
	bActive = false;

	UMaterialInstanceDynamic* pMtrlInstance = Mesh->CreateDynamicMaterialInstance(0, Mesh->GetMaterial(0));
	pMtrlInstance->SetScalarParameterValue(TEXT("Emissive"), 0.f);
}