// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_Curtain.h"

AHN_Curtain::AHN_Curtain()
{
	PoleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("POLE"));
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_CURTAIN(TEXT("StaticMesh'/Game/CustomMesh/cclose.cclose'"));

	if (SM_CURTAIN.Succeeded())
	{
		Mesh->SetStaticMesh(SM_CURTAIN.Object);
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_POLE(TEXT("StaticMesh'/Game/CustomMesh/curtain_bong.curtain_bong'"));

	if (SM_POLE.Succeeded())
	{
		PoleMesh->SetStaticMesh(SM_POLE.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundBase> SOUND_CURTAIN(TEXT("SoundCue'/Game/Audio/SoundCue/Curtain_Cue.Curtain_Cue'"));

	if (SOUND_CURTAIN.Succeeded())
	{
		CurtainSound = SOUND_CURTAIN.Object;
	}


	fCurtainScale = 0.2f;

	RootComponent = PoleMesh;
	PoleMesh->SetWorldScale3D(FVector(1.35f, 1.35f, 1.35f));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetRelativeLocation(FVector(-98.f, 0.0f, -87.0));
	Mesh->SetRelativeScale3D(FVector(fCurtainScale, 1.f, 0.8f));
	Mesh->Mobility = EComponentMobility::Movable;

	Mesh->ComponentTags.Add(TEXT("outline"));

	Speaker->SetRelativeLocation(FVector(0.f, 0.f, 0.f));

	bChange = false;
}

void AHN_Curtain::BeginPlay()
{
	Super::BeginPlay();
}

void AHN_Curtain::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bChange)
	{
		if (bActive)
		{
			//	커튼 열기
			if (FMath::IsNearlyEqual(fCurtainScale, 0.8f, 0.01f))
			{
				bChange = false;
				return;
			}

			fCurtainScale += DeltaTime * 0.5f;
			Mesh->SetRelativeScale3D(FVector(fCurtainScale, 1.f, 0.8f));

		}
		else
		{
			//	커튼 닫기
			if (FMath::IsNearlyEqual(fCurtainScale, 0.2f, 0.01f))
			{
				bChange = false;
				return;
			}

			fCurtainScale -= DeltaTime * 0.5f;
			Mesh->SetRelativeScale3D(FVector(fCurtainScale, 1.f, 0.8f));
		}
	}
}

void AHN_Curtain::ActiveOn_Implementation()
{
	bActive = true;
	Speaker->Stop();
	Speaker->SetSound(CurtainSound);
	Speaker->Play();

	bChange = true;
}

void AHN_Curtain::ActiveOff_Implementation()
{
	bActive = false;

	Speaker->Stop();
	Speaker->SetSound(CurtainSound);
	Speaker->Play();

	bChange = true;
}
