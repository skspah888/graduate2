// Fill out your copyright notice in the Description page of Project Settings.

#include "HN_Gate.h"

#define OPEN_Z 340.f

// Sets default values
AHN_Gate::AHN_Gate()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DOOR"));		//	문 열고 닫히는 부분
	Speaker = CreateDefaultSubobject<UAudioComponent>(TEXT("SPEAKER"));

	RootComponent = Speaker;
	Door->SetupAttachment(RootComponent);

	//	문 초기화
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_DOOR(TEXT("StaticMesh'/Game/Asset/Hospital/Meshes/Interior_Elements/Firefighting/SM_Fire_Cabinet_02_Door.SM_Fire_Cabinet_02_Door'"));

	if (SM_DOOR.Succeeded())
	{
		Door->SetStaticMesh(SM_DOOR.Object);
	}

	Door->SetRelativeScale3D(FVector(8.5f, 1.0f, 4.0f));
	Door->SetRelativeLocation(FVector(0.f, 45.f, 0.f));

	bOpen = false;
	fValue = 0.f;
}

// Called when the game starts or when spawned
void AHN_Gate::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickEnabled(false);
}

// Called every frame
void AHN_Gate::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (fValue >= OPEN_Z)
	{
		SetActorTickEnabled(false);
		bOpen = true;
		return;
	}

	fValue += 0.2f;

	Door->SetRelativeLocation(FVector(0.f, 45.f, fValue));
}

void AHN_Gate::Open()
{
	SetActorTickEnabled(true);
}

void AHN_Gate::Stop()
{
	SetActorTickEnabled(false);
}

