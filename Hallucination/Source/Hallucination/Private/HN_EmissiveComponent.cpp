// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_EmissiveComponent.h"
#include <Kismet/KismetMaterialLibrary.h>

// Sets default values for this component's properties
UHN_EmissiveComponent::UHN_EmissiveComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// 디폴트값 셋팅
	static ConstructorHelpers::FObjectFinder<UCurveFloat> Curve(TEXT(
		"CurveFloat'/Game/Materials/Dissolve2/DissolveCurve.DissolveCurve'"));
	if (Curve.Succeeded())
	{
		EmissiveCurve = Curve.Object;
	}

	EmissiveOriginPower = 15.f;
	EmissiveParameterName = "Power glow";
	IsEmissiveFlicking = false;

	AccumulatedTime = 0.f;
	EmissiveFrequencyMin = 1.f;
	EmissiveFrequencyMax = 1.f;
	EmissiveFrequency = 1.f;
}


// Called when the game starts
void UHN_EmissiveComponent::BeginPlay()
{
	Super::BeginPlay();

	GetStaticMeshes();
	GetSkeletalMeshes();
	SetEmissivePower();
	EmissiveCurrentPower = EmissiveOriginPower;
	EmissiveFrequency = FMath::RandRange(EmissiveFrequencyMin, EmissiveFrequencyMax);
}


// Called every frame
void UHN_EmissiveComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!IsEmissiveFlicking)
		return;

	AccumulatedTime += DeltaTime;

	if (AccumulatedTime > EmissiveFrequency)
	{
		AccumulatedTime = 0.f;
		EmissiveFrequency = FMath::RandRange(EmissiveFrequencyMin, EmissiveFrequencyMax);
	}

	EmissiveCurrentPower = EmissiveOriginPower * Cast<UCurveFloat>(EmissiveCurve)->GetFloatValue(AccumulatedTime / EmissiveFrequency);

	UpdateEmissivePower(EmissiveCurrentPower);
}

void UHN_EmissiveComponent::GetStaticMeshes()
{
	TArray<UActorComponent*> StaticMeshComponents = GetOwner()->GetComponentsByClass(UStaticMeshComponent::StaticClass());
	for (int i = 0; i < StaticMeshComponents.Num(); ++i)
	{
		TArray<UMaterialInterface*> MaterialInterfaces = Cast<UStaticMeshComponent>(StaticMeshComponents[i])->GetMaterials();
		for (int j = 0; j < MaterialInterfaces.Num(); ++j)
		{
			UMaterialInstanceDynamic* MaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), MaterialInterfaces[j]);
			Cast<UStaticMeshComponent>(StaticMeshComponents[i])->SetMaterial(j, MaterialInstance);
			EmissiveMaterials.Add(MaterialInstance);
		}
	}
}

void UHN_EmissiveComponent::GetSkeletalMeshes()
{
	TArray<UActorComponent*> SkeletalMeshComponents = GetOwner()->GetComponentsByClass(USkeletalMeshComponent::StaticClass());
	for (int i = 0; i < SkeletalMeshComponents.Num(); ++i)
	{
		TArray<UMaterialInterface*> MaterialInterfaces = Cast<USkeletalMeshComponent>(SkeletalMeshComponents[i])->GetMaterials();
		for (int j = 0; j < MaterialInterfaces.Num(); ++j)
		{
			UMaterialInstanceDynamic* MaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), MaterialInterfaces[j]);
			Cast<USkeletalMeshComponent>(SkeletalMeshComponents[i])->SetMaterial(j, MaterialInstance);
			EmissiveMaterials.Add(MaterialInstance);
		}
	}
}

void UHN_EmissiveComponent::SetEmissivePower()
{
	for (auto EmissiveMaterial : EmissiveMaterials)
	{
		Cast<UMaterialInstanceDynamic>(EmissiveMaterial)->SetScalarParameterValue(EmissiveParameterName, EmissiveOriginPower);
	}
}

void UHN_EmissiveComponent::UpdateEmissivePower(float _EmissivePower)
{
	for (auto EmissiveMaterial : EmissiveMaterials)
	{
		Cast<UMaterialInstanceDynamic>(EmissiveMaterial)->SetScalarParameterValue(EmissiveParameterName, _EmissivePower);
	}
}

