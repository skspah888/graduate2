// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_3F_NursePatrol.h"
#include "HN_Ghost_Nurse.h"
#include "HN_AI_Nurse.h"
#include "HN_AnimInst_Nurse.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTask_3F_NursePatrol::UBTTask_3F_NursePatrol()
{
	bNotifyTick = false;
	NodeName = TEXT("Patrol WayPoint");
}

EBTNodeResult::Type UBTTask_3F_NursePatrol::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	/*auto ControllingPawn = Cast<AHN_Ghost_Nurse>(OwnerComp.GetAIOwner()->GetPawn());
	ControllingPawn->SetGoalPoint(false);
	ControllingPawn->SetNurseState(ENurseState::RUN);

	return EBTNodeResult::InProgress;*/

	auto ControllingPawn = Cast<AHN_Ghost_Nurse>(OwnerComp.GetAIOwner()->GetPawn());

	FVector NextLocation = Cast<AHN_AI_Nurse>(OwnerComp.GetAIOwner())->GetNextLocation();
	OwnerComp.GetBlackboardComponent()->SetValueAsVector(AHN_AI_Nurse::PatrolPosKey, NextLocation);
	return EBTNodeResult::Succeeded;
}

void UBTTask_3F_NursePatrol::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);

	Cast<AHN_AI_Nurse>(OwnerComp.GetAIOwner())->PatrolNurseGhost();

	auto ControllingPawn = Cast<AHN_Ghost_Nurse>(OwnerComp.GetAIOwner()->GetPawn());

	if (ControllingPawn->GetGoalPoint())
	{
		ControllingPawn->SetNurseState(ENurseState::IDLE);
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
