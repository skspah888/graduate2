// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_AnimInst_Scamp.h"


UHN_AnimInst_Scamp::UHN_AnimInst_Scamp()
{
	IsIdle = true;
}

void UHN_AnimInst_Scamp::SetIsIdle(bool _IsIdle)
{
	IsIdle = _IsIdle;
}
