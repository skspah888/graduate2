// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_DragPenalty.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_AI_Drag.h"
#include "HN_Player.h"
#include "HN_Ghost_Drag.h"

UBTTask_4F_DragPenalty::UBTTask_4F_DragPenalty()
{
	NodeName = TEXT("DragGhost Penalty");
	IsPenalty = false;
}

EBTNodeResult::Type UBTTask_4F_DragPenalty::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	if (IsPenalty)
		return EBTNodeResult::Succeeded;

	auto TargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::TargetKey));
	if (nullptr == TargetPlayer)
		return EBTNodeResult::Failed;

	auto NotTargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Drag::NotTargetKey));
	if (nullptr == NotTargetPlayer)
		return EBTNodeResult::Failed;


	// TargetPlayer or NotTargetPlayer에게 효과 부여하기
	auto ControllingPawn = Cast<AHN_Ghost_Drag>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;
	Cast<AHN_Ghost_Drag>(ControllingPawn)->PenaltyPlayerGhost();
	//print("Penalty Drag!!");

	// 블랙보드 변수 초기화
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Drag::IsOpenDoorKey, false);
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Drag::IsContactKey, false);

	IsPenalty = true;


	return EBTNodeResult::Succeeded;
}
