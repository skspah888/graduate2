// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_4F_PatientClear.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_AI_Patient.h"
#include "HN_Ghost_Patient.h"
#include "HN_4F_DragTriggerVolume.h"
#include "HN_Player.h"
#include "Engine/TargetPoint.h"
#include "EngineUtils.h"

UBTTask_4F_PatientClear::UBTTask_4F_PatientClear()
{
	NodeName = TEXT("Patient Ghost Clear");

	//static ConstructorHelpers::FObjectFinder<UBlueprint> TriggerBlueprint(TEXT(
	//	"Blueprint'/Game/Blueprints/BP_4F_DragTriggerVolume'")); //.BP_4F_DragTriggerVolume'"));
	//if (TriggerBlueprint.Object) {
	//	PlayerBlueprintType = (UClass*)TriggerBlueprint.Object->GeneratedClass;
	//}
}

EBTNodeResult::Type UBTTask_4F_PatientClear::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	// 타겟 플레이어 찾기 ( 탐지된 플레이어 )
	auto TargetPlayer = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Patient::TargetKey));
	if (nullptr == TargetPlayer)
		return EBTNodeResult::Failed;

	// 플레이어랑 접촉 상태, 플레이어한테 반지가 있는지 검사 ( bCheck 변경 )
	bool IsClear = OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Patient::IsClearKey);

	if (IsClear)
	{
		bool bCheck = TargetPlayer->CheckItem("Ring");
		if (bCheck)
		{
			// 애니메이션 변경
			Cast<AHN_Ghost_Patient>(OwnerComp.GetAIOwner()->GetPawn())->ChangeAnim_Patient();

			// 트리거 생성
			FVector SpawnPosition = FVector(2440.f, -2080.f, 40.f);
			for (TActorIterator<ATargetPoint> Iter(GetWorld()); Iter; ++Iter)
			{
				if ((*Iter)->GetName() == FString(TEXT("DragTriggerSpawnPosition")))
				{
					SpawnPosition = Cast<ATargetPoint>(*Iter)->GetActorLocation();
					break;
				}
			}

			FTransform SpawnLocation;
			SpawnLocation.SetLocation(SpawnPosition);
			GetWorld()->SpawnActor<AHN_4F_DragTriggerVolume>(AHN_4F_DragTriggerVolume::StaticClass(), SpawnLocation);

			// 아이템 사용
			TargetPlayer->UseItem("Ring");

			// 타이머 설정 [ 5초뒤 파괴 ]
			auto ControllingPawn = Cast<AHN_Ghost_Patient>(OwnerComp.GetAIOwner()->GetPawn());
			if (nullptr == ControllingPawn)
				return EBTNodeResult::Failed;
			ControllingPawn->ClearPatient();
		}
		
		bool IsRing = OwnerComp.GetBlackboardComponent()->GetValueAsBool(AHN_AI_Patient::IsRingKey);
		if (IsRing)
		{
			// 애니메이션 변경
			Cast<AHN_Ghost_Patient>(OwnerComp.GetAIOwner()->GetPawn())->ChangeAnim_Patient();

			// 트리거 생성
			FTransform SpawnLocation;

			SpawnLocation.SetLocation(FVector(2440.f, -2080.f, 10.f));

			GetWorld()->SpawnActor<AHN_4F_DragTriggerVolume>(AHN_4F_DragTriggerVolume::StaticClass(), SpawnLocation);

			// 아이템 사용
			TargetPlayer->UseItem("Ring");

			// 타이머 설정 [ 5초뒤 파괴 ]
			auto ControllingPawn = Cast<AHN_Ghost_Patient>(OwnerComp.GetAIOwner()->GetPawn());
			if (nullptr == ControllingPawn)
				return EBTNodeResult::Failed;
			ControllingPawn->ClearPatient();
		}
	}

	return EBTNodeResult::Succeeded;
}
