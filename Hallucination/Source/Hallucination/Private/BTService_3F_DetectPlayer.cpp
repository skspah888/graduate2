// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_3F_DetectPlayer.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Player.h"
#include "HN_Ghost_Nurse.h"
#include "HN_AI_Nurse.h"

UBTService_3F_DetectPlayer::UBTService_3F_DetectPlayer()
{
	NodeName = TEXT("Dectect");
	Interval = 0.f;	
}

void UBTService_3F_DetectPlayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto ControllingPawn = Cast<AHN_Ghost_Nurse>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
	{
		//print("Not ControllingPawn");
		return;
	}

	auto Target = Cast<AHN_Player>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AHN_AI_Nurse::TargetKey));
	if (nullptr == Target)
	{
		//print("Not Target");
		return;
	}

	bool bResult = (Target->GetDistanceTo(ControllingPawn) <= ControllingPawn->GetAttackRange());
	OwnerComp.GetBlackboardComponent()->SetValueAsBool(AHN_AI_Nurse::IsAttackKey, bResult);

	//print(FString::SanitizeFloat(Target->GetDistanceTo(ControllingPawn)) + "   /  " + FString::SanitizeFloat(ControllingPawn->GetAttackRange()));
}
