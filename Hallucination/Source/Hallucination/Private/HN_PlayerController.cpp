// Fill out your copyright notice in the Description page of Project Settings.


#include "HN_PlayerController.h"
#include "HN_Player.h"
#include "EngineUtils.h"

AHN_PlayerController::AHN_PlayerController()
{
	RootComponent->bAbsoluteRotation = true;
}

void AHN_PlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AHN_PlayerController::OnPossess(APawn * aPawn)
{
	Super::OnPossess(aPawn);
}

void AHN_PlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(GameInputMode);	// 게임 플레이에서만 입력 받음
}

void AHN_PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction(TEXT("Character_Change"), EInputEvent::IE_Pressed, this, &AHN_PlayerController::Character_Change);
}
void AHN_PlayerController::Character_Change()
{
	UWorld* CurrentWorld = GetWorld();

	for (TActorIterator<AHN_Player> Iter(CurrentWorld); Iter; ++Iter)
	{
		if (!(*Iter)->IsPlayerControlled())
		{
			OnPossess(*Iter);
			return;
		}
	}
}

void AHN_PlayerController::SetPlayerControllerInput(bool bCheck)
{
	// SetIgnoreLookInput(bCheck);
	// SetIgnoreMoveInput(bCheck);
}
