// Fill out your copyright notice in the Description page of Project Settings.

#include "HN_AI_Nurse.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISense.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HN_Player.h"
#include "HN_Ghost_Nurse.h"
#include "HN_WayPoint.h"
#include "EngineUtils.h"

const FName AHN_AI_Nurse::IsRageKey(TEXT("IsRage"));
const FName AHN_AI_Nurse::TargetKey(TEXT("TargetObject"));
const FName AHN_AI_Nurse::IsAttackKey(TEXT("IsAttack"));
const FName AHN_AI_Nurse::HomePosKey(TEXT("HomePos"));
const FName AHN_AI_Nurse::PatrolPosKey(TEXT("PatrolPos"));

AHN_AI_Nurse::AHN_AI_Nurse()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObject(TEXT(
		"BlackboardData'/Game/AI/3F/BB_3F.BB_3F'"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;		// 블랙보드 셋팅
	}

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObject(TEXT(
		"BehaviorTree'/Game/AI/3F/BT_3F_Nurse.BT_3F_Nurse'"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;		// 비헤이비어트리 셋팅
	}

	// AI Perception [ Sight ] 
	SetPerceptionComponent(*CreateOptionalDefaultSubobject<UAIPerceptionComponent>(TEXT("AI Perception")));
	SightConfig = CreateOptionalDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));

	AISightRadius = 750.f;
	AISightAge = 1.0f;
	AILoseSightRadius = AISightRadius + 50.0f;
	AIFieldOfView = 75.f;

	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightAge);

	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;

	GetPerceptionComponent()->ConfigureSense(*SightConfig);
	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());

	GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AHN_AI_Nurse::OnTargetUpdate);
	GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &AHN_AI_Nurse::OnPerceptionUpdate);

	// 팀 ID 설정
	SetGenericTeamId(FGenericTeamId(1));

	IsRage = false;
}

void AHN_AI_Nurse::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHN_AI_Nurse::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	UseBlackboard(BBAsset, Blackboard);

	// 블랙보드 사용
	//GetBlackboardComponent()->SetValueAsFloat(DelayTimeKey, RandTime);
}

void AHN_AI_Nurse::OnUnPossess()
{
	Super::OnUnPossess();
}

void AHN_AI_Nurse::Initialize_SightSense()
{
	// AI Perception [ Sight ] 
	SetPerceptionComponent(*CreateOptionalDefaultSubobject<UAIPerceptionComponent>(TEXT("AI Perception")));
	SightConfig = CreateOptionalDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));

	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightAge);

	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;

	GetPerceptionComponent()->ConfigureSense(*SightConfig);
	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
}

AHN_Player* AHN_AI_Nurse::GetTarget()
{
	return Cast<AHN_Player>(GetBlackboardComponent()->GetValueAsObject(TargetKey));
}

bool AHN_AI_Nurse::GetRage()
{
	return GetBlackboardComponent()->GetValueAsBool(IsRageKey);
}

void AHN_AI_Nurse::OnTargetUpdate(AActor* Actor, FAIStimulus Stimulus)
{
	if (IsRage)
		return;
	//print("OnTargetUpdate");

	/*FActorPerceptionBlueprintInfo Info;
	GetAIPerceptionComponent()->GetActorsPerception(Actor, Info);
	if (Stimulus.WasSuccessfullySensed())
		print("True");

	else
		print("False");*/
}

void AHN_AI_Nurse::OnPerceptionUpdate(const TArray<AActor*>& DetectedPawns)
{
	if (IsRage)
		return;

	for (size_t i = 0; i < DetectedPawns.Num(); i++)
	{
		if (Cast<AHN_Player>(DetectedPawns[i]))
		{
			FActorPerceptionBlueprintInfo Info;
			GetAIPerceptionComponent()->GetActorsPerception(DetectedPawns[i], Info);

			if (Info.LastSensedStimuli.Num() > 0) {
				const FAIStimulus Stimulus = Info.LastSensedStimuli[0];

				if (Stimulus.WasSuccessfullySensed()) {
					GetBlackboardComponent()->SetValueAsObject(TargetKey, DetectedPawns[i]);
					Cast<AHN_Player>(DetectedPawns[i])->Damage(10);

					AHN_Ghost_Nurse* ControllerPawn = Cast<AHN_Ghost_Nurse>(GetPawn());
					if (ControllerPawn)
					{
						ControllerPawn->StartLaughSound();
					}
				}
				else {
					GetBlackboardComponent()->SetValueAsObject(TargetKey, nullptr);
					AHN_Ghost_Nurse* ControllerPawn = Cast<AHN_Ghost_Nurse>(GetPawn());
					if (ControllerPawn)
					{
						ControllerPawn->StartWhisperSound();
					}
				}
			}
		}
	}
}

void AHN_AI_Nurse::TestNurseRageMode()
{
	IsRage = !GetBlackboardComponent()->GetValueAsBool(IsRageKey);
	GetBlackboardComponent()->SetValueAsBool(IsRageKey, IsRage);

	if (IsRage)
	{
		//GetBlackboardComponent()->SetValueAsObject(TargetKey, GetWorld()->GetFirstPlayerController()->GetPawn());
		GetBlackboardComponent()->SetValueAsObject(TargetKey, Cast<AHN_Ghost_Nurse>(GetPawn())->RagePoint);
	}

	AHN_Ghost_Nurse* ControllerPawn = Cast<AHN_Ghost_Nurse>(GetPawn());
	if (ControllerPawn)
	{
		ControllerPawn->StartPlayerDeadSound();
	}
}

void AHN_AI_Nurse::PatrolNurseGhost()
{
	// WayPoint 따라서 Nurse 이동
	AHN_Ghost_Nurse* ControllerPawn = Cast<AHN_Ghost_Nurse>(GetPawn());
	if (ControllerPawn->GetGoalPoint())
		return;

	if (ControllerPawn->NextWayPoint != nullptr)
	{
		/*EPathFollowingRequestResult::Type ResultType = MoveToActor(ControllerPawn->NextWayPoint, 10.f);

		switch (ResultType)
		{
		case EPathFollowingRequestResult::Failed:
			print("Failed");
			break;
		case EPathFollowingRequestResult::AlreadyAtGoal:
			print("AlreadyAtGoal");
			break;
		case EPathFollowingRequestResult::RequestSuccessful:
			print("RequestSuccessful");
			break;
		default:
			break;
		}*/

		MoveToActor(ControllerPawn->NextWayPoint, 50.f);
	}
}

void AHN_AI_Nurse::ChaseTarget()
{
	// Target 추적
	AActor* Target = Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(TargetKey));
	if (!Target)
		return;

	MoveToActor(Target, 1.f);
}

FVector AHN_AI_Nurse::GetNextLocation()
{
	// WayPoint 따라서 Nurse 이동
	AHN_Ghost_Nurse* ControllerPawn = Cast<AHN_Ghost_Nurse>(GetPawn());

	FVector Location = ControllerPawn->GetActorLocation();
	if (ControllerPawn->NextWayPoint != nullptr)
	{
		FVector NextLocation = ControllerPawn->NextWayPoint->GetActorLocation();
		NextLocation.Z = Location.Z;

		return NextLocation;
	}

	return Location;
}

void AHN_AI_Nurse::ExcuteBehaviorTree()
{
	// 비헤이비어 트리 사용
	RunBehaviorTree(BTAsset);
}
